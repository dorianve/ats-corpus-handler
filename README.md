# ATS Corpus Handler (ATSCH for short)

A python package to manage corpora aimed at Automatic Text Summarization (ATS) as well as the semi-automatic evaluation process (with ROUGE). 
Typically, a corpus for ATS has several Original Texts and reference summaries (gold standard)

ATSCH is programmed for my master thesis (written in French), that investigates the effect on using an automatic coreference resolution tool on the quality (in the aspect of coherence) of a summary. It includes a coreference resolver script, that relies on the output of [COFR](https://github.com/boberle/cofr) to modify sentence in order to resolve the coreference. The algorithme is rule based and therefore is specific for French.

Note : The Centroid implementation (with and without coreference) reffered to in the thesis in NOT included in this online repository, given that the code it is built on was developed during an internship and is therefore not mine to publish. The code for those was, instead, provided to the jury in an annexe file containing all the code of this repository, and the summarizers (in the atsch/summarizers directory, with our baseline summarizers)