#!/usr/bin/env python
from setuptools import setup

setup(
   name='atsch',
   version='0.0.4',
   author='Dorian Venderick',
   author_email='dorian.venderick@student.uclouvain.be',
   packages=['atsch'],
   scripts=[],
   url='https://gitlab.com/dorianve/ats-corpus-handler',
   license='LICENSE.txt',
   description='A set of python classes to manage corpora aimed at Automatic Text Summarization (ATS) as well as the '
               'semi-automatic evaluation process (with ROUGE). Typically, a corpus for ATS has several Original '
               'Texts and reference summaries (gold standard)',
   long_description=open('README.md').read(),
   install_requires=[
       "spacy>=3.0.0",
       "lexical-diversity",
       "numpy",
       "pandas",
       "rouge",
       "tqdm"
   ]
)
