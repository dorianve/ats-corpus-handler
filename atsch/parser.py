import json
from spacy.tokens import Doc, Span
from spacy import Vocab
from typing import Any

CONTRACTED_ARTICLES = ["du", "des", "au", "aux"]
UNCONTRACTED_ARTICLES = [['de', 'le'], ['de', 'les'], ['à', 'le'], ['à', 'les']]


def is_flat_list(test_list: list[Any]) -> bool:
    """Tests if a non empty list has 1 dimension (True) or more (False)

    Args:
        test_list: A non-empty list

    Returns: True (flat list) or False (multi dimensional list)

    """
    return bool(type(test_list[0]) != list)


def tokens2list(doc: Doc) -> list[str]:
    """Forms a list of sentences and tokens from a spacy Doc.
    """
    return [token.text for token in doc]


def spaces2list(doc: Doc) -> list[bool]:
    """Generates a list of booleans to show where spaces are to be placed
    after tokens.
    """
    return [bool(token.whitespace_) for token in doc]


def pos2list(doc: Doc) -> list[str]:
    """Extracts the list of POS tags.
    """
    return [token.pos_ for token in doc]


def lemmas2list(doc: Doc) -> list[str]:
    """Makes a list of lemmas.
    """
    return [token.lemma_ for token in doc]


def sents2list(doc: Doc) -> list[list[str]]:
    """Forms a list of sentences and tokens from a spacy Doc.
    """
    return [[token.text for token in sent] for sent in doc.sents]


def sentstart2list(doc: Doc) -> list[list[bool]]:
    """Makes list of sentence starts"""
    return [token.is_sent_start for token in doc]


def nested2sentstart(list2d: list[list[Any]]) -> list[bool]:
    """Creates a list of lists of booleans, where 1st element of sublist is True (sentence start)

    Args:
        list2d: a 2d list

    Returns:
        a list of lists of booleans, where 1st element of sublist is True (sentence start)
    """
    nw_l = []
    for sublist in list2d:
        nw_l.append(True)
        for _ in range(len(sublist) - 1):
            nw_l.append(False)
    return nw_l


def nested2flat(nested_lst: list[list[Any]]) -> list[Any]:
    """Transforms an 2d list into a flat list.
    Args:
        sents: a 2d list (eg [[0],[1],[2],[2]]

    Returns: a flat list (eg. [0,1,2,3])
    """
    return [item for lst in nested_lst for item in lst]


def flat2nested(flatlist: list[Any], sub_start: list[bool]) -> list[list[Any]]:
    """Turns a flat list into a 2d list, with sub lists

    Args:
        flatlist: a list to transform into a 2d list
        sub_start: a boolean list where True is at the index where to start a sublist

    Returns: A 2d list
    """
    nw_2d_list = []
    for item, needs_split in zip(flatlist, sub_start):
        if needs_split:
            nw_2d_list.append([])
        nw_2d_list[-1].append(item)
    return nw_2d_list


def contracted2full(tokens: list[str], align: list[list[Any]] = None) -> tuple[list[str], list[bool], list[Any]]:
    """Transforms French contracted articles into their full form.
    For French, it can be useful to transform the contracted articles "du", "des", "au" and "aux"
    into their non-contracted (ungrammatical) forms "de le", "de les", "à la", "à les"
    (eg. for use with corefence algorithme)

    Args:
        tokens: a list of tokens, where articles ARE contracted.
        align: A list of lists that have to be aligned with the full forms of the articles

    Returns:
        - the transformed list, with contracted forms replaced by non-contracted forms
        - a bool. list where item is true when it corresponds to a non-contracted acticle
    """
    nw_doc = []  # Store the tokens as strings
    is_uncontracted = []  # Store True where the corresponding token is an uncontracted article, False otherwise
    ca = CONTRACTED_ARTICLES
    ua = UNCONTRACTED_ARTICLES

    if align is None:
        align = []
    aligned = [[] for _ in align]

    for i, tok in enumerate(tokens):
        if tok in ca:
            index = ca.index(tok)
            nw_doc.extend(ua[index])  # add the 2 uncontracted articles
            is_uncontracted.extend([True, False])  # add True for 1st part and False for 2nd part of article
            for align_li, aligned_li in zip(align, aligned):
                aligned_li.extend([align_li[i], align_li[i]])  # same value for the 2 parts of uncontracted art
        else:
            nw_doc.append(tok)
            is_uncontracted.append(False)
            for align_li, aligned_li in zip(align, aligned):
                aligned_li.append(align_li[i])  # copy value for lists to be aligned
    return nw_doc, is_uncontracted, aligned


def full2contracted(tokens: list[str], contraction: list[bool], align: list[list[Any]] = None) \
        -> tuple[list[str], list[list[Any]]]:
    """Transforms French non-contracted articles into the (grammatical) contracted forms.
    eg. after coreference resolution.
    Args:
        tokens:  a list corresponding to a sentencized and tokenized text, where articles ARE NOT contracted.
        contraction: a bool. list where item is true when it corresponds to a non-contracted acticle
        align: A list of lists that have to be aligned with the contracted forms of the articles

    Returns:
        - the transformed list, with non-contracted forms replaced by contracted forms

    """
    assert (len(tokens) == len(contraction))
    if align is None:
        align = []
    else:
        for li in align:
            assert (len(li) == len(tokens))
    aligned = [[] for _ in align]

    nw_doc = []
    ca = CONTRACTED_ARTICLES
    ua = UNCONTRACTED_ARTICLES

    i = 0
    while i <= len(tokens) - 1:
        if contraction[i]:
            nw_doc.append(ca[ua.index([tokens[i], tokens[i + 1]])])
            i += 1
        else:
            nw_doc.append(tokens[i])
        # Then align the other lists, if present.
        for align_li, aligned_li in zip(align, aligned):
            aligned_li.append(align_li[i])  # copy value for lists to be aligned
        i += 1
    return nw_doc, aligned


def clone_shape_list(og_list: list) -> list:
    """Clones the shape of a list and returns a new list, where the elements have been replaced by "_".
    (eg. [["aa"],["bb"]] -> [[_],[_]]) This can be useful to work with COFR (coreference resolution tool)

    Args:
        og_list:

    Returns:

    """
    return [clone_shape_list(item) if type(item) is list else "_" for item in og_list]


def doc2dict(doc: Doc,
             doc_key: str = '',
             doc_type: str = '',
             author: str = '',
             other: list[tuple[Any, Any]] = None) -> dict:
    """Takes a spacy Doc and generates a json dictionnary.
    The resulting dictonnay is compatible to work with COFR, a coreference resolution tool in French.

    Args:
        author: The ID of the author af the document.
        doc: The spaCy Doc from which to make the dict
        doc_key: The name of the document
        doc_type: The type of document (eg. 'original', 'reference', 'generated_summary')
        other: A list of tuples containing optional additional keys and values to add to the dict

    Returns: a dictionnary
    """
    nw_dict = {
        "doc_key": doc.user_data["doc_key"] if "doc_key" in doc.user_data else doc_key,
        "doc_type": doc.user_data["doc_type"] if "doc_type" in doc.user_data else doc_type,
        "clusters": [],  # cluster is user with COFR and is left empty
        "author": doc.user_data["author"] if "author" in doc.user_data else author,
        "sentences": sents2list(doc),
        "spaces": spaces2list(doc),
        "pos": pos2list(doc) if doc[0].pos else [],
    }
    nw_dict["speakers"] = clone_shape_list(nw_dict["sentences"])
    #  If there are some additional values to add, add them
    if other:
        for key, value in other:
            nw_dict[key] = value
    return nw_dict

def dict2doc(doc: dict, vocab: Vocab) -> Doc:
    # The first token might contains the utf8 BOM "\ufeff", which is not required here
    doc["sentences"][0][0] = doc["sentences"][0][0].strip("\ufeff")
    # The list of all the keys spaCy accepts when creating a new Doc
    doc_keys = ["vocab", "sentences", "spaces", "tags", "pos", "morphs", "lemmas", "heads", "deps", "sent_start", "ents"]
    # All the other keys available in doc will be added to Doc.user_data
    user_data = {key: value for key, value in doc.items() if key not in doc_keys}
    return Doc(vocab,
               words=nested2flat(doc["sentences"]),
               spaces=doc.get("spaces"),
               tags=doc.get("tags"),
               pos=doc.get("pos"),
               morphs=doc.get("morphs"),
               lemmas=doc.get("lemmas"),
               heads=doc.get("heads"),
               deps=doc.get("deps"),
               sent_starts=nested2sentstart(doc["sentences"]),
               ents=doc.get("ents"),
               user_data=user_data)


def spans2doc(spans: list[Span]) -> Doc:
    """Makes the summary in doc._.summary as an independent Doc

    Args:
        doc: a Doc that has a summary in

    Returns:
        A new Doc, containing solely the summary

    """
    return Doc.from_docs([span.as_doc() for span in spans])


def vectors2bytes(doc: Doc):
    pass


def bytes2vectors(doc: Doc):
    # TODO : not clear how to handle vectors on the disk.
    # doc.vocab.vectors = spacy.vocab.Vectors(data = None, keys=[words])
    # doc.vocab.vectors.from_bytes(...)
    pass


def save_trf_embeddings_one(self, doc: Doc, path: str) -> Doc:
    # Todo: rename the function
    # Todo: this should go in the parser instead
    return doc

def set_trf_embedding_one(self, doc: Doc, path: str) -> Doc:
    # Todo
    # 1 Check if doc already has trf_data (not empty TransformerData)
    # 2 Check if local trf_data is stored
        # 2.1 restore it
        # 2.2 if disabled, enable Transformer in the pipeline and get the data back
    return doc