from atsch import Corpus
from atsch.summarizers.centroid import CentroidSummarizer
from atsch.summarizers.centroid_corefed_selection import CentroidSummarizerCorefedSelection
from atsch.summarizers.random_baseline import Random
from atsch.summarizers.leadn_baseline import LeadN
import spacy

def main():
    corpus = Corpus()
    nlp = spacy.load("fr_dep_news_trf")
    nlp.add_pipe("tensor2attr")
    disabled_pipes = nlp.select_pipes(enable=["transformer", "tensor2attr"])
    summarizers = [CentroidSummarizer(n=5, similarity_threshold=0.9, tf_idf_threshold= 0.3),
                   CentroidSummarizerCorefedSelection(n=5, similarity_threshold=0.9, tf_idf_threshold=0.3),
                   LeadN(n=5),
                   Random(n=5)
                   ]

    for summarizer in summarizers:
        corpus.summarize(summarizer=summarizer,
                         nlp=nlp)
    # Restore all the pipes because they will be needed for coreference resolution
    disabled_pipes.restore()
    for summarizer in summarizers:
        corpus.summarize(summarizer=summarizer,
                         nlp=nlp,
                         resolve_coref=True,
                         no_coref_dont_keep=True,
                         resolution_window=0,
                         replace_first_mention=True)


if __name__ == "__main__":
    main()