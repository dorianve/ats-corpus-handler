import os
import json
from tqdm import tqdm
from atsch import parser
from atsch.coref_resolver import CorefResolver
import spacy
from pathlib import Path

nlp = spacy.load("fr_dep_news_trf")
input_dir = "originals"
output_dir = "corefed_originals"
Path(output_dir).mkdir(parents=True, exist_ok=True)

for file in tqdm(os.listdir(input_dir)):
    with open(os.path.join(input_dir, file), "r") as json_file:
        dict_doc = json.load(json_file)

    doc = parser.dict2doc(dict_doc, nlp.vocab)
    doc._.nlp = nlp
    for _ , pipe in nlp.pipeline:
        pipe(doc)
    resolver = CorefResolver(doc, target_is_summary=False, resolution_window=0, replace_first_mention=False)
    corefed_doc = resolver()
    dic_doc = parser.doc2dict(corefed_doc, doc.user_data["doc_key"], doc_type="original_corefed")
    dic_doc["is_uncontracted_article"] = resolver.target_is_uncontracted_article
    dic_doc["coref_modification"] = resolver.target_is_modification
    dic_doc["coref_chains"] = resolver.target_corefchains
