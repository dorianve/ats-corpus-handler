"""
Just a little utility script to make a unique dataframe out of all the generated fataframes in
corpus/stats
"""
import os
import pandas as pd

def main():
    """
    cwdir needs to be the stats folder of the corpus
    Returns:

    """
    df1 = pd.DataFrame()
    for dirpath, dirnames, filenames in os.walk("."):
        for filename in filenames:
            df2 = pd.read_feather(os.path.join(dirpath, filename))
            df1 = pd.concat([df1, df2], ignore_index=True)

    df1.to_feather("full_auto_eval_df.ftr")
    df1.to_excel("full_auto_eval_df.xlsx")

if __name__ == "__main__":
    os.chdir("/mnt/sharedhome/dorian/Documents/Documents/Etudes/2019-2020-tal/Memoire/Corpus/txt/Corpus_RPM2_resumes/preprocessed_corpus/stats")
    main()
