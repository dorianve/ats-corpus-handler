import datetime

import numpy as np
import pandas as pd
from hashlib import sha256
from lexical_diversity import lex_div as ld
from spacy.tokens import Doc
from spacy.language import Language
from statistics import mean, pstdev


def _get_sha256(doc: Doc) -> str:
    """Hashes the text with sha256 to get a shorter representation.

    Args:
        doc: The spaCy Doc to process

    Returns:
        The sha256 string representing the text
    """
    return sha256(doc.text.encode('utf-8')).hexdigest()


def _get_model_meta(nlp: Language) -> dict:
    keys2keep = ['lang', 'name', 'version', 'spacy_version', 'author']
    return {key: nlp.meta[key] for key in keys2keep}


class StatFactory:
    def __init__(self):
        self.data = pd.DataFrame()

    def add_doc(self,
                doc: Doc,
                nlp: Language,
                name: str,
                doc_type: str = np.NAN,
                path: str = np.NAN,
                genre: str = np.NAN,
                date: datetime = np.NAN) -> pd.DataFrame:
        """Adds statistics elements of a document to self.data

        Args:
            doc: the document to analyse
            nlp: the spaCy language model used to generate the spaCy doc
            name: A name to give to the doc (eg. the name of the file on the disk)
            doc_type: the type of document to add (eg. Original Document, Reference Summary or Generated Summary)
            path: the path to the json/txt file on the drive
            genre: the genre of the document (eg. news, law, sport, entertainment, social, ...)
            date: date when the document was written

        Returns:
            the updated Pandas DataFrame, populated with collected stat. data.
        """
        # Update self.data
        nbr_tok_no_punct_sent = [sum(not tok.is_punct for tok in sent) for sent in doc.sents]
        nbr_chars_per_sent = [len(sent.text) for sent in doc.sents]
        tokens = [tok.text for tok in doc if not tok.is_punct]
        data = {
            # Doc metas specified by user
            "doc_key": name,
            "doc_path": path,
            "doc_type": doc_type,
            "text_genre": genre,
            "date": date,
            "time_added": datetime.datetime.now(),
            # Processed from Doc and nlp
            "text_sha256sum": _get_sha256(doc),
            "spacy_model": _get_model_meta(nlp),
            "vector_dimensions": doc.vector.shape[0] if doc.has_vector else np.NAN,
            "nbr_chars_w_spaces": len(doc.text),
            "nbr_chars_wo_spaces": len("".join(doc.text.split())),
            "nbr_tokens_w_punct": len(doc),
            "nbr_tokens_wo_punct": len([tok for tok in doc if not tok.is_punct]),
            "nbr_types_wo_punct": len({tok.text.lower() for tok in doc if not tok.is_punct}),
            "nbr_lemmas_wo_punct": len({tok.lemma for tok in doc if not tok.is_punct}),
            "nbr_sentences": len(nbr_tok_no_punct_sent),
            "nbr_token_wo_punct_per_sent": nbr_tok_no_punct_sent,
            "avg_nbr_token_per_sent": round(mean(nbr_tok_no_punct_sent), 2),
            "std_nbr_token_per_sent": round(pstdev(nbr_tok_no_punct_sent), 2),
            "nbr_chars_per_sent_spaces_punct": nbr_chars_per_sent,
            "avg_nbr_chars_per_sent_spaces_punct": round(mean(nbr_chars_per_sent), 2),
            "std_nbr_chars_per_sent_spaces_punct": round(pstdev(nbr_chars_per_sent), 2),
            # Lexical diversity
            "SIMPLETTR": ld.ttr(tokens),
            "ROOTTTR": ld.root_ttr(tokens),
            "LOGTTR": ld.log_ttr(tokens),
            "MASSTTR": ld.maas_ttr(tokens),
            "MSTTR": ld.msttr(tokens),
            "MATTR": ld.mattr(tokens),
            "HDD": ld.hdd(tokens),
            "MTLD": ld.mtld(tokens)
        }
        self.data = self.data.append(data, ignore_index=True)
        return self.data

    def add_coref_chains(self,
                         doc : Doc) -> pd.DataFrame:
        data_column = [
            "doc_key",  # The name of the original doc
            "chain_str",  # The list of all the mentions in that chain
            "chain_size",  # The number of mentions in that chain
            "PRON_count",  # The number of mentions that are pronouns
            "GP_NOUN_count",  # The number of mentions that are a Nominal Group (our a noun)
            "VERB_count",  # The number of mentions that are verbs
            # Add other POS if relevant (see Boberle's paper for coreference relations)
            "nested_count",  # The number of mentions nested in other mentions (except single mentions)
            "indexes",  # The indexes of the mentions in the original text
            "head",  # The first noun (or noun phrase) in the list
            "head_gender",
            "head_number",
            "3Heads",  # The first 3 nouns (or noun phrases in the list)
            "rw_sents",  # The number of sentences elligible for reformulation, with that chai, in the document
            "mention_distances",
            "avg_mentions_distance",  # The avg distance (in term of tokens) between 2 mentions
        ]
        raise NotImplementedError

    def to_disk(self, path):
        raise NotImplementedError
