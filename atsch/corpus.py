from __future__ import annotations

import datetime
import random

import numpy as np
import atsch.parser as parser
from atsch.stat_factory import StatFactory
from atsch.coref_resolver import CorefResolver
import os
from spacy.tokens import Doc
from spacy.language import Language
from tqdm import tqdm
from typing import List, Any, Tuple, Type, Union, Callable
from warnings import warn
from pathlib import Path
import pandas as pd
from datetime import datetime
import json
from threading import Thread, Lock
from rouge import Rouge

# Check if spaCy needed custom extensions already exist, otherwise create them
if not Doc.has_extension("summary"):
    Doc.set_extension("summary", default=[])
if not Doc.has_extension("summary_stats"):
    Doc.set_extension("summary_stats", default=pd.DataFrame())
if not Doc.has_extension("nlp"):
    Doc.set_extension("nlp", default=None)


class Corpus:
    """An ATS corpus is a collection of original texts, reference summaries
	and extracted summaries.
    """

    def __init__(self,
                 docs_dir: str = "original",
                 ref_sums_dir: str = "reference",
                 generated_sums_dir: str = "generated",
                 generated_stats_dir: str = "stats",
                 vectors_store: str = "vectors"
                 ) -> None:
        """
        Args:
            - docs_dir: str = "original", The folder where the original docs are located
            - ref_sums_dir: str = "reference", The folder where the reference summaries docs are located
            - generated_summs_dir: str = "generated", The folder where the generated summaries will be located
            - generated_stats_dir: str = "stats"
            - vectors_store: str = "vectors", The folder in which you'd like to store the embeddings

        """
        # Make sure the directories exist, ore create them
        Path(docs_dir).mkdir(parents=True, exist_ok=True)
        Path(ref_sums_dir).mkdir(parents=True, exist_ok=True)
        Path(generated_sums_dir).mkdir(parents=True, exist_ok=True)
        Path(generated_stats_dir).mkdir(parents=True, exist_ok=True)
        Path(vectors_store).mkdir(parents=True, exist_ok=True)

        self.docs = docs_dir
        self.ref_sums = ref_sums_dir
        self.generated_sums = generated_sums_dir
        self.stats = generated_stats_dir
        self.vectors = vectors_store

    def get_list_docs(self):
        return os.listdir(self.docs)

    def get_list_ref_sums(self):
        return os.listdir(self.ref_sums)

    def gen_forms_data(self,
                       summarization_ids: list[str],
                       target_summary_size: int = 5,
                       target_number: int = 50,
                       min_nbr_sents: int = 10,
                       max_nbr_sents: int = 35,
                       to_disk: bool = True
                       ) -> tuple[list, dict]:
        json_form_data = []
        json_doc_summaries_rel = {}
        orig_docs = self.get_list_docs()
        random.shuffle(orig_docs)
        for doc_filename in tqdm(orig_docs):
            data = {"doc_key": "",
                    "text": "",
                    "generated_summaries": []}
            # try to find all generated summaries with one of the given summarization_ids,
            filename = doc_filename.removesuffix(".json")
            json_doc_summaries_rel[filename] = []
            for i in range(1, 1000):
                path_check = os.path.join(self.generated_sums, filename + "_GS{:0>2}.json".format(i))
                if not os.path.exists(path_check):
                    # If there is no summary with the current ID, there will not be a following one (except if
                    # data was deleted by user), so break
                    break
                summary_data = {"sum_key": "", "text": ""}
                with open(path_check, "r") as json_file:
                    summary = json.load(json_file)
                if summary.get("nbr_sents_orig") < min_nbr_sents or\
                    summary.get("nbr_sents_orig") > max_nbr_sents:
                    # Check the number of sentences in the original document, if the
                    print(f"Doc {filename} has {summary.get('nbr_sents_orig')}  sents and was excluded from the forms.")
                    break
                if len(summary["sentences"]) != target_summary_size:
                    continue
                if summary.get("summarization_process") not in summarization_ids:
                    continue
                summary_data["sum_key"] = summary.get("doc_key")
                summary_text = "\n".join(
                    " ".join(sent) for sent in summary.get('sentences')
                )
                # Make sure the summary is not exactly the same as one before, if any
                if summary_text.lower() in [summary.get("text").lower() for summary in data.get("generated_summaries")]:
                    continue
                summary_data["text"] = summary_text
                data["generated_summaries"].append(summary_data)
                json_doc_summaries_rel[filename].append(summary_data["sum_key"])
            if len(data["generated_summaries"]) < len(summarization_ids):
                # If not enough summaries were collected, pass to another doc
                continue
            # add the doc to the data
            with open(os.path.join(self.docs, doc_filename), "r") as json_file:
                json_doc = json.load(json_file)
            data["doc_key"] = json_doc.get("doc_key")
            data["text"] = "\n".join(" ".join(sent) for sent in json_doc.get('sentences'))
            # add the data tp json_form_data
            json_form_data.append(data)
            if len(json_form_data) >= target_number:
                # If we've reached the number of doc we want to gather the make our forms, exit
                break
        if len(json_form_data) != target_number:
            warn(f"The total number of documents added is {len(json_form_data)} whereas {target_number} were asked.")

        # Save the json_files to disk
        if to_disk:
            with open("forms_data.json", "w") as json_file:
                json.dump(json_form_data, json_file)
            with open("forms_meta_data.json", "w") as json_file:
                json.dump(json_doc_summaries_rel, json_file)
        return json_form_data, json_doc_summaries_rel

    def summarize(self,
                  summarizer: Callable,
                  nlp: Language,
                  summarization_id: str = None,
                  to_disk: bool = True,
                  evaluate: bool = True,
                  gen_stat: bool = True,
                  resolve_coref: bool = False,
                  resolution_window: int = 0,
                  replace_first_mention: bool = True,
                  no_coref_dont_keep: bool = True,
                  date: datetime = None,
                  n_batch: int = 10,
                  n_threads: int = 10,
                  ) -> None:
        """Summarize every original document in the corpus.
        This method is multi-thread.

        Side note: Can't use spaCy's built in nlp.pipe with our json files

        Args:
            summarizer: An instanciated summarizer (example of working summarizer with atsch).
            nlp: A pre-loaded spaCy Language stat model to generate spaCy Doc's
            summarization_id: The name to give to the summarization process
            to_disk: True = data will be save to disk, False = dry run
            evaluate: True = evaluate
            gen_stat: True = generate basic statistics
            resolve_coref: True = the summary needs to have its coreference resolved
            resolution_window: in N, will check the N previous sentence to see if a mention is already referenced by
                a nominal mention there, and do not modify the text in that case. 0 means only the already visited
                tokens of a candidate sentence are taken into account, <0 means no window is set, replace mentions
                wherever possible.
            replace_first_mention: If true, replaces the 1st mention in the text by the 1st nominal mention of its chain
                no matter the pos of the mention.
            no_coref_dont_keep: if True, this method will return None instead of the doc, in case no modification
                has been applied to it
            date: the date of the summarization process
            n_batch: Number of documents to process per thread
            n_threads: Number of threads to use
        """
        if not to_disk:
            warn("The Generated Summaries will NOT be saved, you are just doing a dry run")
        if not summarization_id:
            # Set a default summarizer ID if none is specified
            date = date or datetime.now()
            summarization_id = "-".join([summarizer.name, date.strftime("%Y%m%d-%H:%M:%S")])
        print(f"Starting {summarization_id}")
        # List all documents to summarize
        doc_files = [filename for filename in self.get_list_docs()]
        # Create a thread Lock, to avoid race conditions
        th_lock = Lock()
        # Create a progress bar, with the number of files to summarize
        pbar = tqdm(total=len(doc_files))

        def inner_thread_summarize():
            # Acquire lock to avoid race conditions
            th_lock.acquire()
            # See if you can take all files specified in batch, otherwise just what is left
            in_batch = n_batch if n_batch <= len(doc_files) else len(doc_files)
            # take the number of files
            current_files = doc_files[:in_batch]
            # delete those elements from the full list of files to process
            del doc_files[:in_batch]
            th_lock.release()
            # open the files in order
            for filename in current_files:
                with open(os.path.join(self.docs, filename), 'r') as json_file:
                    json_doc = json.load(json_file)
                doc = parser.dict2doc(json_doc, nlp.vocab)
                # Process the doc with every pipe of the pipeline
                # Only one thread can use the nlp language at a time
                th_lock.acquire()
                for _, pipe in nlp.pipeline:
                    pipe(doc)
                th_lock.release()
                doc._.nlp = nlp
                self.summarize_one(doc=doc,
                                   summarizer=summarizer,
                                   summarization_id=summarization_id,
                                   to_disk=to_disk,
                                   evaluate=evaluate,
                                   gen_stat=gen_stat,
                                   resolve_coref=resolve_coref,
                                   resolution_window=resolution_window,
                                   replace_first_mention=replace_first_mention,
                                   no_coref_dont_keep=no_coref_dont_keep,
                                   date=date)
                # Update progress bar
                th_lock.acquire()
                pbar.update()
                th_lock.release()

        while doc_files:
            n_threads = n_threads if n_threads >= 0 else 1
            threads = []
            for _ in range(n_threads):
                threads.append(Thread(target=inner_thread_summarize))
                threads[-1].start()
            for thread in threads:
                thread.join()
        pbar.close()

    def summarize_one(self, doc: Doc,
                      summarizer: Callable,
                      summarization_id: str = None,
                      to_disk: bool = True,
                      evaluate: bool = True,
                      gen_stat: bool = True,
                      resolve_coref: bool = False,
                      resolution_window: int = 0,
                      replace_first_mention: bool = True,
                      no_coref_dont_keep: bool = True,
                      date: datetime = datetime.now()
                      ) -> Union[Doc, None]:
        """Summarizes one document with one specified and instanciated summarizer

        Args:
            doc: The original document to summarize
            summarizer: The instanciated summarizer (must have a name)
            summarization_id: The name of the evaluation process
            to_disk: a boolean: True (default) if you want to save to disk, False otherwise
            evaluate: a boolean: True (default) if you want to evaluate the summary as well, when generated
            gen_stat: a boolean: True (default) if you want to generate basic statistics
            resolve_coref: True = the summary needs to have its coreference resolved
            resolution_window: in N, will check the N previous sentence to see if a mention is already referenced by
                a nominal mention there, and do not modify the text in that case. 0 means only the already visited
                tokens of a candidate sentence are taken into account, <0 means no window is set, replace mentions
                wherever possible.
            replace_first_mention: If true, replaces the 1st mention in the text by the 1st nominal mention of its chain
                no matter the pos of the mention.
            no_coref_dont_keep: if True, this method will return None instead of the doc, in case no modification
                has been applied to it
            date: A date of the summarization

        Returns:
            The Doc, with summary stored in Doc._.summary, and optionnal other information in Doc._.user_data
            (evaluation, for instance)

        """
        if not summarization_id:
            # Set a default summarizer ID if none is specified
            summarization_id = "-".join([summarizer.name, date.strftime("%Y%m%d-%H:%M:%S")])

        # Generate the summary with the summarizer
        doc = summarizer(doc)
        # Choose a name for the summary
        summary_name = self._gen_summary_name(doc)
        doc.user_data["summary_name"] = summary_name

        # Optional: Resolve the coreference
        if resolve_coref:
            doc = self.resolve_summary_coreference(doc=doc,
                                                   resolution_window=resolution_window,
                                                   replace_first_mention=replace_first_mention,
                                                   no_coref_dont_keep=True
                                                   )
            #  doc is None if no modification was applied to the summary and no_coref_dont_keep is True
            if no_coref_dont_keep and not doc:
                return

        summarization_parameters = summarizer.__dict__.copy()
        summarization_parameters["coreference_params"] = {
                                                          "resolve_coref": resolve_coref,
                                                          "resol_window": resolution_window,
                                                          "replace_first_mention": replace_first_mention}

        # Optional: Generate stats about that summary
        if gen_stat:
            # Generated meta data will be added to doc._.summary_stats
            doc = self.gen_summary_stats(doc, summarization_id, summary_name, summarization_parameters, date)

        # Optional: evaluate the summary
        if evaluate:
            # Generated meta data will be added to doc._.summary_stats
            doc = self.evaluate_one(doc)

        # Optional: Save the summary on the disk
        if to_disk:
            # 1) save the summary as a json file,
            with open(os.path.join(self.generated_sums, summary_name + ".json"), "w") as json_file:
                other = [("summarization_process", summarization_id),
                         ("date_time", date.strftime("%Y%m%d-%H:%M:%S")),
                         ("extracted_sents_ids", [sent._.orig_sent_i
                                                  for sent in doc._.summary]),
                         ("nbr_sents_orig", len(list(doc.sents)))]
                if doc.user_data.get("coref_resolver"):
                    other.append(("coref_nodes", doc.user_data["coref_resolver"].stats_visited_nodes))
                    other.append(("coref_is_modification", doc.user_data["coref_resolver"].target_is_modification))
                    other.append(("nbr_modified_sentence", doc.user_data["coref_resolver"].stats_nbr_of_modified_sentences))
                other.extend(list(summarizer.__dict__.items()))  # Add all other parameters of the summarizer
                # Write the json file
                json.dump(parser.doc2dict(parser.spans2doc(doc._.summary),
                                          summary_name,
                                          "extractive_summary",
                                          other=other),
                          json_file)
            # 2) save the summary stats and eval in ftr file (name = summary_name)
            if not doc._.summary_stats.empty:
                # Make sure the directory to store the Datafram exists
                Path(os.path.join(self.stats, summarization_id)).mkdir(parents=True, exist_ok=True)
                doc._.summary_stats.to_feather(os.path.join(self.stats, summarization_id, summary_name) + ".ftr")
        return doc

    def _gen_summary_name(self, doc: Doc) -> str:
        """Generates a propper name for the summary, that will be used as filename
        for storing the summary (and in stats)

        Takes the doc_key of the original doc, check if another summary already exists for
        that document, and add the count as summary id

        eg. for doc "T01_C1_D01", 1st summary = "T01_C1_D01_GS01"

        T = Theme, C = Cluster, D = Document, GS = Generated summary

        Args:
            doc: a Doc that has Doc._.user_data["doc_key"] set.

        Returns:
            The name to assign to the summary
        """
        assert doc.user_data.get("doc_key")
        gs_nbr = 1
        for dirpath, dirnames, filenames in os.walk(self.generated_sums):
            for filename in filenames:
                if doc.user_data['doc_key'] in filename:
                    gs_nbr += 1
        return doc.user_data['doc_key'] + "_GS{:0>2}".format(gs_nbr)

    def get_reference_summaries(self, doc_key) -> list:
        """A a list of reference summaries that can be used to evaluate the Doc named doc_key

        Args:
            doc_key: The key of the doc, named like T01_C1_D01 (T = theme, C = cluster, D= document)
            group_by: (optional) Criteria to find the reference summaries
        Returns:
            The list of filenames of the reference summary
        """
        theme, _, _ = doc_key.split("_")
        return [filename for filename in os.listdir(self.ref_sums) if theme in filename]

    def resolve_summary_coreference(self, doc: Doc,
                                    resolution_window: int = 1,
                                    replace_first_mention: bool= True,
                                    no_coref_dont_keep: bool = True,) -> Union[Doc, None]:
        """ Resolve the summary in doc._.summary

        Args:
            doc: The doc to resolve the coreference in (or doc._.summary)
            resolution_window: in N, will check the N previous sentence to see if a mention is already referenced by
                a nominal mention there, and do not modify the text in that case. 0 means only the already visited
                tokens of a candidate sentence are taken into account, <0 means no window is set, replace mentions
                wherever possible.
            replace_first_mention: If true, replaces the 1st mention in the text by the 1st nominal mention of its chain
                no matter the pos of the mention.
            no_coref_dont_keep: if True, this method will return None instead of the doc, in case no modification
                has been applied to it
        Returns:
            Doc: the doc, only if some modifications were made (if no_coref_dont_keep is set)
            None if not modification were made and no_coref_dont_keep is set to True.
        """
        resolver = CorefResolver(doc=doc,
                                 target_is_summary=True,
                                 resolution_window=resolution_window,
                                 replace_first_mention=replace_first_mention
                                 )
        # Resolve coreference and make a list of sentences that will replace doc._.summary
        corefed_doc = resolver()
        corefed_sents = [sent for sent in corefed_doc.sents]
        # If nothing is modified in the corefed summary, then just exit
        if [tok.text.lower() for sent in doc._.summary for tok in sent] == [tok.text.lower() for tok in corefed_doc]:
            return
        # update original sentence index if summary
        for orgi_sum_sent, corefed_sum_sent in zip(doc._.summary, corefed_sents):
            corefed_sum_sent._.orig_sent_i = orgi_sum_sent._.orig_sent_i
        # Now replace doc._.summary by the corefed_summary
        doc._.summary = corefed_sents
        # Attach resolver to doc
        doc.user_data["coref_resolver"] = resolver
        return doc

    def evaluate_one(self, doc: Doc) -> Doc:
        """ Evaluate the generated summary unde doc._.summary (list[Span]) with rouge-1, rouge-2 and rouge-l,
        each time with recall, precision and f score.

        Args:
            doc: The doc containin the summary to evaluate under doc._.summary (list[Span])
        Returns:
            doc: The same Doc as in input, with doc._.summary_stats augmented with the evaluation results.
        """
        assert doc._.summary
        # Get the Theme ID of the original document, every reference summary with the same theme are to be
        #  considered as reference summaries of the original doc
        theme, _, _ = doc.user_data['doc_key'].split("_")
        # Prepare the dictionnary that will hold all the computed data to be added to the DataFrame doc._.summary_stats
        data = {"ref_sum": [],
                "rouge_1_r": [],
                "rouge_1_p": [],
                "rouge_1_f": [],
                "rouge_2_r": [],
                "rouge_2_p": [],
                "rouge_2_f": [],
                "rouge_l_r": [],
                "rouge_l_p": [],
                "rouge_l_f": []
                }
        # Make a doc out of the spans of doc._.summary
        doc_summary = parser.spans2doc(doc._.summary)
        for filename in self.get_list_ref_sums():
            #
            if theme in filename:
                with open(os.path.join(self.ref_sums, filename), "r") as reference:
                    doc_reference = parser.dict2doc(json.load(reference), doc._.nlp.vocab)
                # 2) evaluate summary
                # Each token is sepearated by a space
                reference_sum = " ".join(token.text for token in doc_reference)
                generated_sum = " ".join(token.text for token in doc_summary)
                rouge_eval = Rouge().get_scores(generated_sum, reference_sum)
                # rouge results are in a signle dictionnary in a single list, just get the dict
                rouge_eval = rouge_eval[0]
                # populate the lists with the results
                data["rouge_1_r"].append(rouge_eval.get("rouge-1").get("r"))
                data["rouge_1_p"].append(rouge_eval.get("rouge-1").get("p"))
                data["rouge_1_f"].append(rouge_eval.get("rouge-1").get("f"))
                data["rouge_2_r"].append(rouge_eval.get("rouge-2").get("r"))
                data["rouge_2_p"].append(rouge_eval.get("rouge-2").get("p"))
                data["rouge_2_f"].append(rouge_eval.get("rouge-2").get("f"))
                data["rouge_l_r"].append(rouge_eval.get("rouge-l").get("r"))
                data["rouge_l_p"].append(rouge_eval.get("rouge-l").get("p"))
                data["rouge_l_f"].append(rouge_eval.get("rouge-l").get("f"))
                data["ref_sum"].append(doc_reference.user_data["doc_key"])
        # update DataFrame (one row = one reference summary)
        doc._.summary_stats = pd.concat([doc._.summary_stats] * len(data["rouge_1_r"]), ignore_index=True)
        # Add the evaluation results held in 'data' on axis 1 (columns)
        doc._.summary_stats = pd.concat([doc._.summary_stats, pd.DataFrame(data)], axis=1)
        return doc

    def gen_summary_stats(self,
                          doc: Doc,
                          summarization_id: str,
                          summary_name: str,
                          summarization_parameters: dict = np.NAN,
                          date: datetime = np.NAN
                          ) -> Doc:
        """Generates a DataFrame with basic meta data for statistical analysis,
        as well as some data more specific to summarization

        Args:
            doc: a Doc that contains a summary under Doc._.summary
            summarization_id: The id of summarization process
            date: an optional datetime information

        Returns:
            The original Doc, with stats about the summary stored under doc._.summary_stats
        """
        assert doc._.summary
        # Make a Doc out of the Spans extracted as summary (list[Span])
        summary_doc = parser.spans2doc(doc._.summary)

        # Prepare few meta data needed by StatFactory
        summary_doc.user_data["doc_key"] = [summary_name]
        summary_doc._.nlp = doc._.nlp

        # Generate basic stats about the summary
        summary_stats = StatFactory().add_doc(summary_doc,
                                              doc._.nlp,
                                              summary_name,
                                              "extractive_summary",
                                              date=date)
        # Add specific stats to summarization
        summary_stats["compression_sent"] = [round((len(doc._.summary) / len(list(doc.sents))) * 100, 2)]
        summary_stats["compression_tok"] = [round((len(summary_doc) / len(doc)) * 100, 2)]
        summary_stats["compression_char"] = [round((len(summary_doc.text) / len(doc.text)) * 100, 2)]
        summary_stats["summarizer_parameters"] = [summarization_parameters]
        summary_stats["summarization_id"] = [summarization_id]
        summary_stats["orig_doc_key"] = [doc.user_data["doc_key"]]
        if doc._.summary[0]._.orig_sent_i < 0:
            warn("It looks that the ID of the extracted sentence are not referenced (negative values"
                 "for Span._.orig_sent_i")
        summary_stats["extracted_sentences"] = [[sent._.orig_sent_i for sent in doc._.summary]]
        if doc.user_data.get("coref_resolver"):
            summary_stats["coref_visited_nodes"] = [doc.user_data["coref_resolver"].stats_visited_nodes]
            summary_stats["coref_modified_sentences"] = [doc.user_data["coref_resolver"].stats_nbr_of_modified_sentences]
        doc._.summary_stats = pd.concat([doc._.summary_stats, summary_stats], axis=1)
        return doc
