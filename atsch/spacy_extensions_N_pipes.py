""" All the custom spacy extensions and pipes that are needed for atsch to process documents are declared in this file.
Custom extensions will be automatically declared by atsch when importing the module.
"""
import warnings

import pandas as pd
import numpy as np
from spacy.tokens import Doc, Token, Span
from spacy import Language

# Check if spaCy needed custom extensions already exist, otherwise create them
if not Doc.has_extension("summary"):
    """ Where a summary can be stored by a summarizer. The summary will be a list of Spans, corresponding to sentences
    The Spans are not necessarily taken directly from the original Doc, but can be attached to another Doc, 
    for instance for when a summary goes through coreference resolution and must make some modification.
    """
    Doc.set_extension("summary", default=[])

if not Doc.has_extension("summary_stats"):
    Doc.set_extension("summary_stats", default=pd.DataFrame())

if not Doc.has_extension("nlp"):
    Doc.set_extension("nlp", default=None)

if not Doc.has_extension("get_coref_chains"):
    '''Store mentions of coreference in a ragged list of Spans 
    # Example :
    #
    #         j=0   j=1   ...  j=n
    # i = 0 [[Span, Span, ..., Span]
    # i = 1  [Span, Span, ..., Span]
    # ...            ...
    # i = m  [Span, Span, ..., Span]]
    #
    # Each line is a coreference chain, aka. a real world entity
    # each Span is a mention of that chain
    # i = the ID of the chain (=of the entity),
    # j = the ID of the mention in the chain
    # m = the number of chains
    # n = the number of mentions in a chain

    >>>doc._.coref_chains
    [[Span, Span, Span], [Span, Span, Span, Span] ...]
    '''
    Doc.set_extension("_coref_chains", default=[])

    def getter_setter(doc: Doc, ignore_singletons: bool = True) -> list:
        """ Sets doc._._coref_chains, without singletons if ignore_sinletons is True
        Or simply returns the list if already computed"""
        # Generate the list chains (Spans) if it's empty
        if not doc._._coref_chains:
            for cofr_chain in doc.user_data.get("coref_chains", []):
                chain_spans = []
                if ignore_singletons and len(cofr_chain) == 1:
                    # Skip this iteration to ignore this singleton
                    continue
                for cofr_mention in cofr_chain:
                    k, l = cofr_mention
                    # l is inclusive with COFR, but python is exclusive for 2nd index when slicing lists.
                    # so, we need to add 1 to that index
                    l += 1
                    # Create the Span from the Doc, and add it to current chain
                    chain_spans.append(doc[k:l])
                # Add the current chain to
                doc._._coref_chains.append(chain_spans)
        return doc._._coref_chains
    Doc.set_extension("get_coref_chains", method=getter_setter)

if not Doc.has_extension("get_coref_doc_rep"):
    '''The Doc with increased representation of real world entities, 
    where every mentions is replaced by all the mentions of the same chain

    eg.
    >>> doc = nlp("Ceci est un exemple. Il est parlant!")
    >>> doc.user_data["coref_chains"] = [[[0,0],[2,3], [5,5]]]
    >>> print(doc._.coref_doc_rep)
    Ceci un exemple Il est Ceci un exemple Il. Ceci un exemple Il est parlant!

    >>> type(doc._.coref_doc_rep)
    Doc
    '''
    # set the "hidden" extension, where the data will actually be stored
    Doc.set_extension("_coref_doc_rep", default=None)
    # The function that gets (or sets if already computed) the attribute
    def getter_setter(doc: Doc) -> Doc:
        warnings.warn("doc._.get_coref_doc_rep() is deprecated and will be removed in future versions.")
        if not doc._._coref_doc_rep:
            # Prepare a list to store Docs. Each Doc is made from a sentence from the original Doc,
            # modified for coreference (selection purpose only)
            coref_doc_rep: list[Doc] = []
            # Prepare a queue of sentences
            sents_queue = [[token for token in sent] for sent in doc.sents]
            while sents_queue:
                new_sent: list[Span] = []
                # Make a second queue, from the 1st element of sents_queue
                tokens_queue = sents_queue.pop(0)  # Get the 1st list
                while tokens_queue:
                    end_slice = 1  # Keep track of where to cut the Doc (end of biggest mention for Token[0])
                    # Check first Token for mention
                    if tokens_queue[0]._.coref_spans:
                        # If there are more than 1 corresponding Span, it means that
                        # the token is present in more than 1 mention
                        #
                        #   eg. "ta" in "le corbeau de [[ta]* tante]**"
                        #       where [ta]* might refere to [[toi], [tu], [Jean]]
                        #       and [ta tante]** to [[Germaine], [la sœur de ta mère], [la fille de ta grand mère]]

                        # Sort Span mentions the token[0] is part of, based on the size of each mention
                        # (so, that we start by solving the smaller (aka. inner) mention first
                        # and collect the chain ids (unique value to make sure we only have a the same chain once)
                        chains_ids = set()
                        for span in sorted(tokens_queue[0]._.coref_spans, key=lambda span_x: span_x.end):
                            chain_id, _ = span._.mention_id
                            # Add the corresponding chain to the set
                            chains_ids.add(chain_id)
                            # Update end slice. Logicaly, the last update will give the furthest index the token
                            # is part of, so we know where to slice the token queue later on
                            # (slicing is taken care of after all conditions are checked)
                            end_slice = len(span)

                        # Append new sent with every single chain (= entity) this particylar Token is part of
                        new_sent.extend(span for chain_id in chains_ids for span in doc._.get_coref_chains()[chain_id])
                    else:
                        # If the Token has no mention, just copy it
                        # Add the current token as a Span
                        # Take directly from doc, so that you slice Spans automatically
                        start = tokens_queue[0].i
                        end = start + 1
                        new_sent.append(doc[start:end])
                    # Delete the mention / Token from the queue, since already processed
                    del tokens_queue[0:end_slice]
                # The whole sentence is now processed and the modified version stored in new_sent
                # as a list of Spans. But the tokens are still linked to the original Doc, so any
                # modification on them could cause inconsistance in the original Doc as well.
                # Therefore, make a Doc out of the Spans and crush new_sent

                new_sent: Doc = Doc.from_docs([span.as_doc() for span in new_sent])
                # Set sent_start straight
                for tok in new_sent:
                    # only the first Token is sent_start
                    tok.is_sent_start = tok.i == 0
                # Finally, append this new Doc to coref_doc_rep
                coref_doc_rep.append(new_sent)
            # Merge all the Docs to make a single Doc
            doc._._coref_doc_rep = Doc.from_docs(coref_doc_rep)
            # We could keep article state of contraction in new doc, but there is no need
            # for just a representation as this one.
        return doc._._coref_doc_rep
    Doc.set_extension("get_coref_doc_rep", method=getter_setter)

if not Span.has_extension("mention_id"):
    ''' The mention ID in the coreference chain, meaning a tuple (i, j)
    cf. above for meaning of i and j

    >>>Span._.mention_id
    (i, j)
    '''
    def getter(span: Span) -> tuple:
        assert span.doc._.get_coref_chains()
        for i in range(len(span.doc._.get_coref_chains())):
            for j in range(len(span.doc._.get_coref_chains()[i])):
                if span == span.doc._.get_coref_chains()[i][j]:
                    return i, j
        return ()
    Span.set_extension("mention_id", getter=getter)

if not Span.has_extension("orig_sent_i"):
    """ The place of the extracted sentence in the original text.
    Default is -1, the attribute is not set."""
    Span.set_extension("orig_sent_i", default=-1)

if not Token.has_extension("mention_id"):
    ''' The mention ID in the coreference chain the token is part of, meaning a tuple (i, j), 
    or empty list if is not part of a mention; i is the id of the chain, j the id of the span in the chain
    >>> token._.mention_id
    [(0,0), (1,0)]
    '''
    def getter(tok: Token) -> list[tuple[int, int]]:
        """ The list is ordered by the length of Span, cf. Token._.coref_spans"""
        return [span._.mention_id for span in tok._.coref_spans]
    Token.set_extension("mention_id", getter=getter)

if not Token.has_extension("coref_spans"):
    ''' Store list of coref Spans the Token is part of. 
    The list is ordered by length of Spans in term of tokens(smallest to biggest) to guarantee
    that the first element correspond to the one the is encountered first in the text

    >>>Token._.coref_spans
    [Span, ... Span]
    '''
    def getter(tok: Token) -> list[Span]:
        """ The list is ordered by length of Span """
        span_list = [span for chain in tok.doc._.get_coref_chains() for span in chain if tok in span]
        return sorted(span_list, key=lambda span: len(span))
    Token.set_extension("coref_spans", getter=getter)

if not Token.has_extension("coref_chains"):
    ''' Get all the chains the Token is part of a mention of.
    >>>Token._.coref_chains
    [[Spans,... Span][Span, ..., Span]]
    '''
    def getter(tok: Token) -> list[list[Span]]:
        """ Ordered by length of Span, cf. Token._.coref_spans"""
        return [tok.doc._.get_coref_chains()[i] for i, _ in tok._.mention_id]
    Token.set_extension("coref_chains", getter=getter)

if not Token.has_extension("sent_i"):
    ''' Store the ID of the sentence in the document the Token belongs to

    >>> doc[0]._.sent_i
    0
    '''
    # The number of the sentence in a list of sentence
    getter = lambda token: list(token.doc.sents).index(token.sent)
    Token.set_extension("sent_i", getter=getter)

if not Token.has_extension("is_uncontracted_article"):
    ''' Bool telling if the Token is an uncontracter article ("de les" instead of "des, ...), for French
    (a list of booleans under doc.user_data["is_uncontracted_article"] must be set first,
    eg = doc_tokens = ["le", "collier", "de", "le", "chat"]
        doc.user_data["is_uncontracted_article"] = [False, False, True, False, False]

    >>> doc[2:5]
    de le chat
    >>>doc[2].is_uncontracted_article
    True
    '''
    Token.set_extension("_is_uncontracted_article", default=None)
    # If the list is under user_data, just create a "shortcut", or False as default value

    def setter(tok: Token, is_uncontracted: bool):
        tok._._is_uncontracted_article = is_uncontracted

    def getter(tok: Token):
        if tok._._is_uncontracted_article:
            return tok._._is_uncontracted_article
        is_uncontracted = False
        if tok.doc.user_data.get("is_uncontracted_article"):
            is_uncontracted = tok.doc.user_data.get("is_uncontracted_article")[tok.i]
        return is_uncontracted
    Token.set_extension("is_uncontracted_article", getter=getter, setter=setter)

if not Token.has_extension("is_modification"):
    ''' Boolean value to show if a Token corresponds to a modification to the original doc
    '''
    Token.set_extension("is_modification", default=False)



# Use the @ character to register the pipe element under the name 'tensor2attr'.
@Language.factory('tensor2attr')
class Tensor2Attr:
    """This script was provided there
    https://applied-language-technology.readthedocs.io/en/latest/notebooks/part_iii/04_embeddings_continued.html#contextual-word-embeddings-from-transformers
    """
    def __init__(self, name, nlp):
        # We do not really do anything with this class, so we
        # simply move on using 'pass' when the object is created.
        pass

    # The __call__() method is called whenever some other object
    # is passed to an object representing this class.
    # We use the variable 'doc' to refer to any object received.
    def __call__(self, doc):

        self.add_attributes(doc)
        return doc

    # Next, we define the 'add_attributes' method that will modify
    # the incoming Doc object by calling a series of methods.
    def add_attributes(self, doc):
        # spaCy Doc objects have an attribute named 'user_hooks',
        # which allows customising the default attributes of a
        # Doc object, such as 'vector'. We use the 'user_hooks'
        # attribute to replace the attribute 'vector' with the
        # Transformer output, which is retrieved using the
        # 'doc_tensor' method defined below.
        doc.user_hooks['vector'] = self.doc_tensor

        # We then perform the same for both Spans and Tokens that
        # are contained within the Doc object.
        doc.user_span_hooks['vector'] = self.span_tensor
        doc.user_token_hooks['vector'] = self.token_tensor

        # We also replace the 'similarity' method, because the
        # default 'similarity' method looks at the default 'vector'
        # attribute, which is empty! We must first replace the
        # vectors using the 'user_hooks' attribute.
        doc.user_hooks['similarity'] = self.get_similarity
        doc.user_span_hooks['similarity'] = self.get_similarity
        doc.user_token_hooks['similarity'] = self.get_similarity

        # Now that doc has vector, 'has_vector' method must return True
        doc.user_hooks['has_vector'] = self.has_vector
        doc.user_span_hooks['has_vector'] = self.has_vector
        doc.user_token_hooks['has_vector'] = self.has_vector

    def has_vector(self, doc):
        return any(doc.vector)

    # Define a method that takes a Doc object as input and returns
    # Transformer output for the entire Doc.
    def doc_tensor(self, doc):
        # Return Transformer output for the entire Doc. As noted
        # above, this is the last item under the attribute 'tensor'.
        # Average the output along axis 0 to handle batched outputs.
        return doc._.trf_data.tensors[-1].mean(axis=0)

    # Define a method that takes a Span as input and returns the Transformer
    # output.
    def span_tensor(self, span):
        # Get alignment information for Span. This is achieved by using
        # the 'doc' attribute of Span that refers to the Doc that contains
        # this Span. We then use the 'start' and 'end' attributes of a Span
        # to retrieve the alignment information. Finally, we flatten the
        # resulting array to use it for indexing.
        tensor_ix = span.doc._.trf_data.align[span.start: span.end].data.flatten()

        # Fetch Transformer output shape from the final dimension of the output.
        # We do this here to maintain compatibility with different Transformers,
        # which may output tensors of different shape.
        out_dim = span.doc._.trf_data.tensors[0].shape[-1]

        # Get Token tensors under tensors[0]. Reshape batched outputs so that
        # each "row" in the matrix corresponds to a single token. This is needed
        # for matching alignment information under 'tensor_ix' to the Transformer
        # output.
        tensor = span.doc._.trf_data.tensors[0].reshape(-1, out_dim)[tensor_ix]

        # Average vectors along axis 0 ("columns"). This yields a 768-dimensional
        # vector for each spaCy Span.
        return tensor.mean(axis=0)

    # Define a function that takes a Token as input and returns the Transformer
    # output.
    def token_tensor(self, token):
        # Get alignment information for Token; flatten array for indexing.
        # Again, we use the 'doc' attribute of a Token to get the parent Doc,
        # which contains the Transformer output.
        tensor_ix = token.doc._.trf_data.align[token.i].data.flatten()

        # Fetch Transformer output shape from the final dimension of the output.
        # We do this here to maintain compatibility with different Transformers,
        # which may output tensors of different shape.
        out_dim = token.doc._.trf_data.tensors[0].shape[-1]

        # Get Token tensors under tensors[0]. Reshape batched outputs so that
        # each "row" in the matrix corresponds to a single token. This is needed
        # for matching alignment information under 'tensor_ix' to the Transformer
        # output.
        tensor = token.doc._.trf_data.tensors[0].reshape(-1, out_dim)[tensor_ix]

        # Average vectors along axis 0 (columns). This yields a 768-dimensional
        # vector for each spaCy Token.
        return tensor.mean(axis=0)

    # Define a function for calculating cosine similarity between vectors
    def get_similarity(self, doc1, doc2):
        # Calculate and return cosine similarity
        return np.dot(doc1.vector, doc2.vector) / (doc1.vector_norm * doc2.vector_norm)