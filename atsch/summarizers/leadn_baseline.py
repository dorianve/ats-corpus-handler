"""
LEAD-N is a baseline summarizer, that takes the N first sentences in the document to make a summary
"""
from spacy.tokens import Doc, Span

# Check if summary extension is already set
if not Doc.has_extension("summary"):
    Doc.set_extension("summary", default=[])


class LeadN:

    def __init__(self,
                 n: int = 3):
        self.name = "LeadN"
        self.n = n

    def __call__(self, doc: Doc, n: int = None) -> Doc:
        n = self.n if not n else n
        return self.summarize(doc, n)

    def summarize(self, doc: Doc, n: int = None) -> Doc:
        n = self.n if not n else n
        doc._.summary = list(doc.sents)[0:n]
        # put the index of extracted sentence
        for sentence in doc._.summary:
            sentence._.orig_sent_i = sentence[0]._.sent_i
        return doc
