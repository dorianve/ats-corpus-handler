"""
Random is a baseline summarizer, where N random sentences are extracted from a document
"""
from spacy.tokens import Doc, Span
import random

# Check if summary extension is already set
if not Doc.has_extension("summary"):
    Doc.set_extension("summary", default=[])


class Random:

    def __init__(self,
                 n: int = 5):
        self.name = "Random"
        self.n = n

    def __call__(self, doc: Doc) -> Doc:
        return self.summarize(doc)

    def summarize(self, doc: Doc) -> Doc:
        l = list(doc.sents)
        random.shuffle(l)
        doc._.summary = l[:self.n]
        # Set the id of the sentence
        for span_sent in doc._.summary:
            span_sent._.orig_sent_i = span_sent[0]._.sent_i
        return doc

