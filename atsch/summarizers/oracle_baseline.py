# TODO WIP

"""
Oracle is a baseline summarizer, where N sentences are selected based on their impact of ROUGE-1 and Rouge-2 scores
"""
from spacy.tokens import Doc, Span
import statistics
import warnings

# Check if summary extension is already set
if not Doc.has_extension("summary"):
    Doc.set_extension("summary", default=[])


class Oracle:

    def __init__(self,
                 n: int = 5):
        self.name = "Oracle"
        self.n = n

    def __call__(self, doc: Doc) -> Doc:
        return self.summarize(doc)

    def summarize(self, doc: Doc, ref_summaries) -> Doc:
        raise NotImplementedError
        summary = []
        sentences = list(doc.sents)
        if self.n >= len(sentences):
            warnings.warn("The length of the document is shorter than that set for the target summary")
        while len(summary) != self.n or len(summary) <= len(sentences):
            highest_score = 0
            highest_index = 0
            for i, sent in enumerate(sentences):
                # evaluate sentence against ref summaries
                # TODO: will have to take into account ROUGE 1, ROUGE 2 or custom evaluation metric?
                # Optimizing for ROUGE-1 might be enough : Durrett et al (2016) observation :
                #  " We found that optimizing for ROUGE-1 actually resulted
                #  in a model with better performance on both ROUGE-1 and
                #  ROUGE-2"
                scores = [evaluate(sent, ref_summary) for ref_summary in ref_summaries]
                avg_score = statistics.mean(scores)
                if avg_score > highest_score:
                    highest_score = avg_score
                    highest_index = i
            summary.append(sentences.pop(highest_index))
        doc._.summary = summary
        # indicate the id of the extracted sentences
        for sentence in doc._.summary:
            sentence._.orig_sent_i = sentence[0]._.sent_i
        return doc
