"""Rules to transform a Doc with coreference chains under doc.user_data["coref_chains"].
The coref_chains are in the format of prediction of COFR https://github.com/boberle/cofr

The rules here are not meant to match every case but to cover basic usage.
It's not exhaustive.

The chains under doc.user_data["coref_chains"] are in a list as follows:
- outer list    [
- chain 0           [
- mentions*             [k,l],[k,l],[k,l]
                    ],
- chain 1...        [   [k,l],[...] ...]
                ]
* For mentions, k = index of the first Token of the mention, l = index of last token (in a flat list)

They are now automatically processed by custum extensions set to Doc, Token and Span elements
(cf. spacy_extensions_N_pipres.py)

The rules of the class CorefResolver consist of nodes in a directed Graph, the diagram of which is available
as a xml file to open with https://www.diagrams.net/ (previously https://draw.io) and a pdf export
in the gitlab page of atsch, under the "diragram" folder.
"""
import warnings
from collections import deque
from typing import NoReturn, Union
from spacy.tokens import Doc, Span, Token

# Names of the custom extensions set in atsch.spacy_extensions_N_pipes
# useful here:
# - Doc._.get_coref_chains() -> list[list[Span]]:
#           Each span is a mention of a chain (the inner list)
# - Doc._.get_coref_doc_rep() -> Doc:
#           A new Doc with every mention in sentences replaced by the
#           whole chain (selection purposes)
# - Span._.mention_id -> tuple[int,int] = (i,j):
#           where i is the chains id and j the mention id in that chain
#           eg. Doc._.coref_chains[i,j] = Span
# - Token._.coref_spans -> list[Span]:
#           A list of Spans = the mentions this token is part of (often just 1)
# - Token._.sent_i -> int:
#           The id of the sentence in the Doc
# - Token._.is_uncontracted_article -> bool:
#           if the token is uncontracted (helps when need to re-contract the article)
# - Token._.is_modification -> bool:
#           True if a Token is a modification to the original text.


class CorefResolver:

    def __init__(self,
                 doc: Doc,
                 target_is_summary: bool = False,
                 resolution_window: int = 1,
                 replace_first_mention=True):
        """ Prepare the document for resolution of coreference

        Args:
            doc: a spacy Doc, the summary of which (under doc._.summary) or which needs to be
                resolved.
            target_is_summary: (bool) True means that the summary is the target, otherwise it's
                the Doc itself
            resolution_window: The amount of sentences before a Token N being invastigated for resolution
                that will be taking into account to ignore the resolution.
                1 means that, if Token N is part of a coreference chain, a nominal mention of which
                has already been encountered in the current sentence or in the 1 previous one,
                the Token will NOT be candidate for resolution and will simply be added as such in the
                new document. You can choose to increase that window, or decrease it.
                0 means that only nominal mentions in the same sentence as Token N are taken into account.
                -1 (or any minus value) means that every Token is candidate for resolution.
            replace_first_mention:
                True means that every 1st mention in the target text will be replaced by the first
                nominal mention of its chain in the original text, except when another rule supersedes
                this parameter.
        """
        # Warn if the pipe does not have all the elements needed by CorefResolver
        if doc.has_extension("nlp") and not {
            "morphologizer",
            "parser",
            "lemmatizer",
        }.intersection(doc._.nlp.pipe_names):
            warnings.warn("The spacy Pipe does not have all the elements needed for CorefResolver to work "
                          "propperly. Expect inconsistence or errors.")
        self.doc = doc  # Original doc that will never be directly modified (except custom extensions)
        # Following are basic features needed
        self.target_tokens: list[Union[Token, None]] = []  # 'NONE' when the index is not a token from orig doc.
        self.target_words: list[str] = []
        self.target_spaces: list[bool] = []
        self.target_sent_starts: list[bool] = []
        self.target_pos: list[str] = []
        self.target_morphs: list[str] = []
        self.target_lemmas: list[str] = []
        # Note : spaCy parser data such as 'heads' and 'deps' are not going to be copied, because they
        #   are very highly likely to break with the modified text.
        # Following are necessary to reformulate the text later
        self.target_is_uncontracted_article: list[bool] = []
        # Following are (ohter) custom Token extensions to keep a track of
        self.target_sent_i: list[Union[int, None]] = []  # The sentence number in the original file
        self.target_mention_ids: list[Union[list[tuple[int, int]], None]] = []  # IDs of the mentions the tokens are part of
        # Following is the statistical information to keep
        self.target_is_modification: list[bool] = []  # The token at position i is a modification compared to doc
        self.target_corefchains = [[] for _ in doc.user_data["coref_chains"]]  # Empty lists to fill when aligning new token position
        self.stats_nbr_of_modified_sentences = 0
        self.stats_coref_chains_text: list[list[list[str]]] = \
            [[span.text for span in chain] for chain in doc._.get_coref_chains()]  # List of mentions in textual form
        self.stats_encountered_np_chains_in_sent: list[set[int]] = []
        #       Each sentence of target doc has set of encountered mentions here
        #       Only chains referenced to by a noun phrase (NP) are counted
        self.stats_visited_nodes: list[list[str]] = []  # The nodes every Token has come through to be added
        self.tmp_token_visited_nodes: list[str] = []  # Keep track of visited node will they are being visited
        self.tmp_current_sent_is_modified = False
        self.tmp_tokens_queue: deque[Token] = deque()  # All the Tokens in that queue will be of the same sentence.
        self.tmp_sentences_queue: deque[Span] = deque(self.doc._.summary if target_is_summary else self.doc.sents)  # All the sentence to process
        # Parameters
        self.param_resolution_window = resolution_window
        self.param_replace_1st_mention = replace_first_mention

    def __call__(self):
        return self.resolve_coreference()

    def resolve_coreference(self) -> Doc:
        """ Main CorefResolver method, and the only one a user should ever need to use to resolve coreference.
        It takes the original doc and returns a new Doc that has been processed to resolve coreference.

        Returns:
            A Doc, that is a copy of the original doc, but where coreference has been resolved.
        """
        # Iterate over sentences
        while self.tmp_sentences_queue:
            # Make the queue of tokens
            self.tmp_tokens_queue.extend([token for token in self.tmp_sentences_queue.popleft()])
            # Mark 1st Token of sentence as sentence start
            self.target_sent_starts.append(True)
            # Prepare set for recording which chains are refered to in the sentence with a Noun Phrase.
            #  Refered chains are being added to the set when a Token is added with self._add_token
            self.stats_encountered_np_chains_in_sent.append(set())
            # Iterate over tokens of sentence M
            while self.tmp_tokens_queue:
                token = self.tmp_tokens_queue.popleft()
                # Make the Token N go through the graph, starting at node 1
                self._01_token_is_mention(token)
                # reset tmp token visited node list
                self.tmp_token_visited_nodes = []
            if self.tmp_current_sent_is_modified:
                self.stats_nbr_of_modified_sentences += 1
            # Reset value of current_sent_is_modified
            self.tmp_current_sent_is_modified = False
        # Everything is processed, align coreference chain clusters
        self._align_new_coref_chains_ids()
        # Make a doc out of every added tokens and return it
        corefed_doc = Doc(self.doc.vocab,
                          words=self.target_words,
                          spaces=self.target_spaces,
                          pos=self.target_pos,
                          morphs=self.target_morphs,
                          lemmas=self.target_lemmas,
                          sent_starts=self.target_sent_starts)
        # Add the coreference chains in user_data, as well as "is_uncontracted" list
        corefed_doc.user_data["coref_chains"] = self.target_corefchains
        corefed_doc.user_data["is_uncontracted_article"] = self.target_is_uncontracted_article
        # Add is_modification and is_uncontracted article to the tokens
        for token, is_modif, is_uncontracted in zip(corefed_doc, self.target_is_modification, self.target_is_uncontracted_article):
            token._.is_modification = is_modif
            token._.is_uncontracted_article = is_uncontracted
        return corefed_doc

    def _align_new_coref_chains_ids(self):
        i = 0
        while i < len(self.target_tokens):
            orig_token = self.target_tokens[i]
            if orig_token._.coref_spans:
                for mention_span in orig_token._.coref_spans:
                    mention_start = i
                    mention_end = i + ((mention_span.end - 1) - mention_span.start)
                    chain_id, _ = orig_token._.mention_id[0]
                    self.target_corefchains[chain_id].append([mention_start, mention_end])
                    if mention_span == orig_token._.coref_spans[-1]:
                        # Increment i with the number of tokens in the last span
                        i += len(mention_span)
            else:
                i += 1


    def _add_token(self, token: Token, is_modification: bool = False) -> NoReturn:
        """Adds a Token to the target text.
        Use a temporary sentence formed by a list of tokens (and strings) you are using for coreference
        and then iterate over each item and only then add them with this function.
        (because sometimes you need to go back in the sentence, but here you add the Token for good !)

        Args:
            token: The Token that needs to be added to the coreferenced document
            is_modification: True if the Token differs from the original document.
                eg. If it's a completely new Token, or a Token extracted from another sentence, ...
        """
        node_id = "A"
        self.tmp_token_visited_nodes.append(node_id)
        # update the status of modification of the current sentence (only in case of modification)
        if is_modification:
            self.tmp_current_sent_is_modified = True
        # Add all infos to all the lists in self
        # First, check sentence boundaries status
        is_sent_start = len(self.target_sent_starts) > len(self.target_tokens) and self.target_sent_starts[-1]
        # If the word is a DET or a PRON, check what casing it needs to have (title or lower)
        if token.pos_ in ["DET", "PRON"]:
            word_cased = token.text.title() if is_sent_start else token.text.lower()
        else:
            word_cased = token.text
        self.target_tokens.append(token)
        self.target_words.append(word_cased)
        self.target_spaces.append(bool(token.whitespace_))
        if not is_sent_start:
            # Only append a value in case Token N is not sent start, otherwise it should already have been set
            self.target_sent_starts.append(False)
        self.target_pos.append(token.pos_)
        self.target_morphs.append(str(token.morph))
        self.target_lemmas.append(token.lemma_)
        self.target_is_uncontracted_article.append(token._.is_uncontracted_article)
        #   Value will be modified when necessary afterwards
        self.target_sent_i.append(token._.sent_i if token.doc == self.doc else None)  # sentence in the original file
        self.target_mention_ids.append(token._.mention_id if token.doc == self.doc and token._.mention_id else None)
        self.target_is_modification.append(is_modification)
        self.stats_visited_nodes.append(self.tmp_token_visited_nodes)
        # Add the chain the Token is a mention of to the set of already met chains in the current sentence
        if token._.coref_spans and {"NOUN", "PROPN"}.intersection({tok.pos_ for tok in token._.coref_spans[0]}):
            self.stats_encountered_np_chains_in_sent[-1].add(token._.mention_id[0][0])
        # If the token we just added is a popped token, change some values according to it
        if token.doc.user_data.get("misc_popped_token_data"):
            popped = token.doc.user_data.get("misc_popped_token_data")
            self.target_tokens[-1] = popped["orig_token"]
            self.target_sent_i[-1] = popped["sent_i"]
            self.target_mention_ids[-1] = popped["mention_ids"]
            self.target_is_modification[-1] = popped["is_modification"]
            self.stats_visited_nodes[-1] = popped["visited_nodes"]

    def _pop_token(self, index: int = -1) -> Token:
        """ Remove a token at index `index` in all the lists

        Args:
            index: The index of the already added token to remove from the list

        Returns: The token that has been removed
        """
        node_id = "POP"
        tmp_doc = Doc(self.doc.vocab,
                      words=[self.target_words.pop(index)],
                      spaces=[self.target_spaces.pop(index)],
                      pos=[self.target_pos.pop(index)],
                      lemmas=[self.target_lemmas.pop(index)],
                      morphs=[self.target_morphs.pop(index)],
                      sent_starts=[self.target_sent_starts]  # DO NOT pop here (cf. bottom of function)
                      )
        tmp_doc[0]._.is_uncontracted_article = self.target_is_uncontracted_article.pop(index)
        visited_nodes = self.stats_visited_nodes.pop(index)[:]
        visited_nodes.append(node_id)
        misc_popped_token_data = {
            "orig_token": self.target_tokens.pop(index),
            "sent_i": self.target_sent_i.pop(index),
            "mention_ids": self.target_mention_ids.pop(index),
            "is_modification": self.target_is_modification.pop(index),
            "visited_nodes": visited_nodes
        }
        tmp_doc.user_data["misc_popped_token_data"] = misc_popped_token_data
        # Only pop sent start if the Token is Not sent start, otherwise, let the Token
        #  that will replace if become sentence start
        if not self.target_sent_starts[index]:
            self.target_sent_starts.pop(index)
        return tmp_doc[0]

    def _add_mention_instead(self, token: Token) -> NoReturn:
        """ Add a nominal mention refered by the token, instead of the token"""
        replacement_mention: Span = self._get_replacement_noun_mention(token)
        assert replacement_mention
        for new_token in replacement_mention:
            self._add_token(new_token, is_modification=True)

    def _get_replacement_noun_mention(self, token: Token) -> Union[Span, None]:
        """ Get the 1st nominal mention of the Chain O"""
        for mention_span in token._.coref_chains[0]:
            for mention_token in mention_span:
                if mention_token.pos_ in ["NOUN", "PROPN"]:
                    # Return The first nominal mention that is found
                    return mention_span
        # If no nominal mention has been found, return NONE
        return None

    def _resolve_rule_abis(self, token: Token) -> NoReturn:
        """Replace Mention P by 1st nominale mention in original texte

        Args:
            token: Token N, for which a resolution is required
        """
        node_id = "Abis"
        self.tmp_token_visited_nodes.append(node_id)
        for candidate_mention in token._.coref_chains[0]:
            for candidate_token in candidate_mention:
                if candidate_token.pos_ in ["NOUN", "PROPN"]:
                    # Candidate Mention has a NOUN (or "PROPN"), let's loop over the candidate mention again
                    # and add its Tokens to the target document
                    for candidate_token in candidate_mention:
                        is_modification = token._.coref_spans[0] != candidate_mention
                        self._add_token(candidate_token, is_modification=is_modification)
                    # Dequeue mention P from the queue, as it's not needed in the target document,
                    # since replaced by candidate mention
                    # Just check that the queue is not empty first
                    while self.tmp_tokens_queue:
                        # Span.end is not the index of the last token composing it, but
                        # the last token + 1 (token offset)
                        if self.tmp_tokens_queue[0].i >= token._.coref_spans[0].end:
                            break
                        self.tmp_tokens_queue.popleft()
                    # Stop the loops and exit
                    return
        # In case nothing was found, add every token of mention P to the text
        # starting with token N
        self._add_token(token, is_modification=False)
        # Just check that the queue is not empty first
        while self.tmp_tokens_queue:
            if self.tmp_tokens_queue[0].i >= token._.coref_spans[0].end:
                break
            self._add_token(self.tmp_tokens_queue.popleft(), is_modification=False)

    def _resolve_rule_b(self, token: Token) -> NoReturn:
        """ Replace mention P, starting with a demonstrative determiner ("ce", "ces", "cette"),
        by another nominal mention of Chain O not starting with a demonstrative determiner.

        eg. input: « Ces derniers étaient parvenu, ... »
        eg. output: « Les guérilleros étaient parvenu »

        Args:
            token: Token N, for which a resolution is required
        """
        node_id = "B"
        self.tmp_token_visited_nodes.append(node_id)
        # Get len of the whole mention P
        mention_p = token._.coref_spans[0]
        # if exists, replace mention by another nominal mention not starting with a demonstrative determiner
        found_match = False
        for candidate_mention in token._.coref_chains[0]:
            if candidate_mention[0].morph.to_dict().get("PronType") != "Dem" \
                    and "NOUN" in [tok.pos_ for tok in candidate_mention]:
                # Match, put candidate mention instead of mention p
                for tok in candidate_mention:
                    self._add_token(tok, is_modification=True)
                # Now we can pop mention P from the queue of tokens
                while self.tmp_tokens_queue:
                    # Span.end = last token offset (so token.i + 1)
                    if self.tmp_tokens_queue[0].i >= token._.coref_spans[0].end:
                        break
                    self.tmp_tokens_queue.popleft()
                # exit loop and function
                return
        # if no match in the loop, just add Token N as such
        self._add_token(token, is_modification=False)

    def _resolve_rule_c(self, token: Token) -> NoReturn:
        """ Replace possessive determiners by their real world entity
        and rearange the sentence

        eg. input: « Son grand manteau jaune est chaud. »
        eg. output: « Le grand manteau jaune de Sophie est chaud. »

        Args:
            token: Token N, for which a resolution is required
        """
        node_id = "C"
        self.tmp_token_visited_nodes.append(node_id)
        # We need an equivalence table to be able to replace the possessive det. by a definite one.
        poss_det_table = {
            "son": {
                "Masc": "le",
                "Vowel": "l'"
            },
            "sa": {
                "Fem": "la",
                "Vowel": "l'"
            },
            "ses": {
                "Fem": "les",
                "Masc": "les",
                "Vowel": "l'"
            },
            "leur": {
                "Fem": "la",
                "Masc": "le",
                "Vowel": "l'"
            },
            "leurs": {
                "Fem": "les",
                "Masc": "les",
                "Vowel": "les"
            }}
        # If the possessive determiner is not in 3r Person and therefore not
        #  in the dict above  (eg. "mon", "ton", ...) just add the token and exit function.
        art = None
        if token.text.lower() not in poss_det_table.keys():
            self._add_token(token, is_modification=False)
            # exit the function
            return
        # else Add definite determiner instead of Token N
        # if the 1st character of the following token is a vowel, add the corresponding def. article
        if len(self.doc) > token.i:
            if self.doc[token.i + 1].text[0].lower() in "aeiou":
                art = poss_det_table.get(token.text.lower()).get("Vowel")
            # else, find the gender of the noun after the det in the queue of Tokens
            # (same sentence), to make sure you get the right def det.
            else:
                for tok in self.tmp_tokens_queue:
                    if tok.pos_ == "NOUN":
                        # Check the gender of the noun
                        gender = tok.morph.to_dict().get("Gender")
                        # Choose the article depending on the gender of the noun
                        art = poss_det_table.get(token.text.lower()).get(gender)
                        break
        # If for some reason no article was found, simply add token N unmodified and exit function
        if not art:
            self._add_token(token, is_modification=False)
            return
        # Make sure the new article has right spacing (no space only after "l'")
        space = art != "l'"
        # If art is "le" or "les", and it is precede by "de" or à, then they must be flagged as uncontracted article
        if self.target_words and art in ["le", "les"] and self.target_words[-1].lower() in ["de", "à"]:
            self.target_is_uncontracted_article[-1] = True
        # Make sure the new article has the right cass
        if token.is_title:
            art = art.title()
        # actually create a new Token
        definite_art = Doc(self.doc.vocab,
                           words=[art],
                           spaces=[space],
                           sent_starts=[token.is_sent_start],
                           pos=["DET"])[0]
        # Add the new art
        self._add_token(definite_art, is_modification=True)
        # Check if Token N -1 (if any) is "de", and flag it as uncontracted article
        # looking at index -2, because we just added an article between Token N -1 and Token N
        if len(self.target_words) >= 2 and self.target_words[-2].lower() == "de":
            self.target_is_uncontracted_article[-2] = True
        # Add next tokens up to AUX, VERB, SCONJ, PUNCT or end of the subtree of the Noun
        while self.tmp_tokens_queue:
            # Add up to the NOUN following the det
            tok = self.tmp_tokens_queue[0]
            if tok.pos_ == "NOUN":
                # Iterate over the subtree of the Noun to find the last acceptable
                #  Token to add before adding the mention of Token N
                subtree = list(tok.subtree)
                # Find the index of tok in its subtree, only iterate from there,
                # To ensure tok and subtree[0] are the same
                subtree = subtree[subtree.index(tok):]
                assert self.tmp_tokens_queue[0] == subtree[0]
                for i, sub_tok in enumerate(subtree):
                    assert self.tmp_tokens_queue[0] == subtree[i]
                    if (
                        subtree[-1] != sub_tok  # If there are still other tokens in the subtree
                        and sub_tok.text in ",-"
                        and subtree[i + 1].pos_ not in ["PUNCT","AUX", "VERB", "PRON", "SCONJ"]
                    ):
                        # Add the comma (or hyphen) if not followed by a stopping POS
                        self._add_token(self.tmp_tokens_queue.popleft(), is_modification=False)
                        continue
                    if sub_tok.pos_ in ["PUNCT", "AUX", "VERB", "PRON", "SCONJ"]:
                        # Stop adding the tokens just before one of the above POS
                        # Including other PUNCT as "," and "-" (cf. if condition just above)
                        break
                    self._add_token(self.tmp_tokens_queue.popleft(), is_modification=False)
                # Stop adding the Tokens of the tmp tokens queue
                break
            self._add_token(self.tmp_tokens_queue.popleft(), is_modification=False)
        # insert "de" at current position
        self._add_adp_de(token)
        # Finally, add the nominal mention
        self._add_mention_instead(token)

    def _resolve_rule_d(self, token: Token) -> NoReturn:
        """ Resolve coreference of Token N in sentence M, when Token N is a pronoun (PRON)
        subjet (nsubj) preceded by the particle "-t" and sentence M is NOT an interrogative sentence,
        or does not contain a question after Token N

        eg. input: « ... a-t-[elle] indiqué ... »
        eg. output: « ... a indiqué [Ingrid Betancourt] ... »

        Args:
            token: Token N, for which a resolution is required
        """
        node_id = "D"
        self.tmp_token_visited_nodes.append(node_id)
        # This rule applies only for cases with euphoric "-t", which means there must be a VERB
        # and that verb is somewhere after Token N in the queue, or was there right before -t and is already added.
        # Should that not be the case, raise assertion error
        # Exclude Token N-1 "-t", already added in the lists
        self._pop_token(-1)
        # Check if the VERB is already added (otherwise it will be an AUX)
        if self.target_pos[-1] == "VERB":
            # Sentence must look like "... indique-t-il"
            # So just add the nominal mention at current position
            self._add_mention_instead(token)
            # Leave the function
            return
        elif self.target_pos[-1] == "AUX":
            # Make sure there is a VERB somewhere after the current position
            for tok in self.tmp_tokens_queue:
                if tok.pos_ == "VERB":
                    # Add all the Tokens in queue until the VERB (inlcuded).
                    while self.tmp_tokens_queue:
                        new_token = self.tmp_tokens_queue.popleft()
                        self._add_token(new_token, is_modification=False)
                        if new_token.pos_ == "VERB":
                            break
                    # Last element is now the VERB, make sure it has spacing
                    self.target_spaces[-1] = True
                    # Now add the mention after the VERB
                    self._add_mention_instead(token)
                    # Exit the function, job is done
                    return
                # If no verb is found, just add the Token
        self._add_token(token)

    def _resolve_rule_e(self, token: Token) -> NoReturn:
        """ Replace Token N (3rd P. nsubj PRON) by a nominal mention

        eg. input: « Il faut qu'il vienne, ... »
        eg. output: « Il faut qu'Ivan vienne, ... »

        Args:
            token: Token N, for which a resolution is required
        """
        node_id = "E"
        self.tmp_token_visited_nodes.append(node_id)
        # Simply replace Token N (PRON, 3rd p, nsubj) by a nominal mention
        self._add_mention_instead(token)

    def _resolve_rule_e_bis(self, token: Token) -> NoReturn:
        """ Uncontract SCONJ ("qu'"-> "que", "lorsqu'" -> "lorsque")

        eg. input: « Je pense qu'il viendra. »
        eg. output: « Je pense que Jean viendra. »

        Args:
            token: Token N, for which a resolution is required
        """
        node_id = "Ebis"
        self.tmp_token_visited_nodes.append(node_id)
        # replace Token N-1, logically the last Token to having been added in self.target_tokens,
        # which is a SCONJ that is contracted, by its decontracted form, which happens to be its lemma.
        old_token_1 = self._pop_token()
        new_token_1 = Doc(vocab=self.doc.vocab,
                          words=[old_token_1.lemma_],
                          lemmas=[old_token_1.lemma_],
                          spaces=[True],
                          pos=[old_token_1.pos_],
                          morphs=[str(old_token_1.morph)],
                          sent_starts=[old_token_1.is_sent_start])[0]
        self._add_token(new_token_1, is_modification=True)
        # Then resolve Token N with rule E
        self._resolve_rule_e(token)

    def _resolve_rule_f(self, token: Token) -> NoReturn:
        """
        eg. input: « A-t-il eu froid ? »
        eg. output: « Jean a-t-il eu froid ? »
        """
        node_id = "F"
        self.tmp_token_visited_nodes.append(node_id)
        tmp_token_queue = []
        pattern_found = False
        # Find where to insert the nominal mention, before a VERB (or AUX), in a window of 2 words max
        for _ in range(2, 0, -1):
            # remove Tokens previously added to new document until you find a VERB or an AUX (included),
            #  and put them in a queue, to add them again later
            if not self.target_tokens:
                break
            tmp_token = self._pop_token()
            tmp_token_queue.append(tmp_token)
            if tmp_token.pos_ in ["AUX", "VERB"]:
                pattern_found = True
                break
        if pattern_found:
            # Insert mention before VERB (or AUX)
            self._add_mention_instead(token)
            # Add again all the tokens after the insert (need to go backward again)
            while tmp_token_queue:
                self._add_token(tmp_token_queue.pop(), is_modification=False)
            # Add Token N itself (the pronoun)
            self._add_token(token, is_modification=False)
        else:
            self._add_token(token)

    def _resolve_rule_g(self, token: Token) -> NoReturn:
        """
        eg. input: « Stéphane l'avait humilié »
        eg. output: « Stéphane avait humilié Kevin »
        """
        node_id = "G"
        self.tmp_token_visited_nodes.append(node_id)
        # Don't include Token N
        # Add all tokens until the VERB (likely = include just the VERB ;) )
        # Since Token N is not in the queue of Tokens anymore (because currently analysed) use the queue
        while self.tmp_tokens_queue:
            other_token = self.tmp_tokens_queue.popleft()
            # Add the token
            self._add_token(other_token)
            if other_token.pos_ == "VERB":
                break
        # Add the nominal mention
        self._add_mention_instead(token)

    def _resolve_rule_h(self, token: Token) -> NoReturn:
        """
        eg. input: « Il [lui] a offert des fleurs. »
        eg. output: « Il a offert des fleurs à [Jean]. »
        """
        node_id = "H"
        self.tmp_token_visited_nodes.append(node_id)
        # Do not add Token N (iobj personnal Pron)
        # Find NOUN also obj within a window of 5 max
        found_obj = None
        window = min(len(self.tmp_tokens_queue), 5)
        for _, tok in zip(range(window), self.tmp_tokens_queue):
            if tok.pos_ in ["NOUN", "PROPN"] and tok.dep_ == "obj":
                found_obj = tok
                break
        if not found_obj:
            # If no match was found, send Token to 02_bis, to replace it
            #  only if it's the first mention of Chain 0 in the new doc o
            #  and it's different from the 1st mention of Chain O in the
            #  original doc
            self._02bis_is_mention_p_1st_orig_doc(token)
            # exit the function, node 02bis is in charge now
            return
        obj_subtree = list(found_obj.subtree)
        end_obj_i = obj_subtree[-1].i if obj_subtree else found_obj.i
        while self.tmp_tokens_queue:
            if self.tmp_tokens_queue[0].i > end_obj_i:
                break
            # add every token until the end of the obj subtree
            self._add_token(self.tmp_tokens_queue.popleft(), is_modification=False)
        # add ADP "à"
        self._add_adp_a(token)
        # Add nominal mention
        self._add_mention_instead(token)

    def _resolve_rule_i(self, token: Token) -> NoReturn:
        """
        eg. input: « Je le [leur] ai donné »
        eg. output: « Je l'ai donné à les enfants »

        Note: « à les » is marked as uncontracted and can be transformed
            to « aux » easily afterwards
        """
        node_id = "I"
        self.tmp_token_visited_nodes.append(node_id)
        # The sentence must contain a VERB, otherwise just add Token N without modification.
        if "VERB" not in [tok.pos_ for tok in self.tmp_tokens_queue]:
            self._add_token(token, is_modification=False)
            return
        # Token N (personnal PRON will be exluded in the new doc)
        # If token N-1 is "le" and Token N + 1 starts with a vowel, replace "le" with "l'"
        if self.target_tokens[-1].text.lower() == "le" and self.tmp_tokens_queue[0].text[0].lower() in "aeiou":
            self.target_words[-1] = "l'"
            self.target_spaces[-1] = False
            self.target_is_modification[-1] = True
        # Add every token in the queue until VERB (included)
        while self.tmp_tokens_queue:
            tok = self.tmp_tokens_queue.popleft()
            self._add_token(tok, is_modification=False)
            if tok.pos_ == "VERB":
                break
        # add ADP "à"
        self._add_adp_a(token)
        # Add nominal mention
        self._add_mention_instead(token)

    def _add_adp_a(self, token: Token):
        """ Adds "à" at current position, and marks it as a modification
        Args:
            token: The Token is needed to know what mention will actually follow "à"
        """
        tmp_doc = Doc(self.doc.vocab,
                      words=["à"],
                      lemmas=["à"],
                      spaces=[True],
                      pos=["ADP"],
                      sent_starts=[False],
                     deps=["case"])
        self._add_token(tmp_doc[0], is_modification=True)
        # mark "à" as uncontracted if nominal mention starts with DET "le" or "les"
        if self._get_replacement_noun_mention(token)[0].text.lower() in ["le", "les"]:
            self.target_is_uncontracted_article[-1] = True

    def _add_adp_de(self, token):

        tmp_doc = Doc(self.doc.vocab,
                      words=["de"],
                      lemmas=["de"],
                      spaces=[True],
                      sent_starts=[False],
                      pos=["ADP"],
                      deps=["case"])
        self._add_token(tmp_doc[0], is_modification=True)
        if self._get_replacement_noun_mention(token)[0].text.lower() in ["le", "les"]:
            # If the candidate mention starts with "le" or "les", then "de" must be
            #  an uncontracted article
            self.target_is_uncontracted_article[-1] = True

    def _01_token_is_mention(self, token: Token) -> NoReturn:
        node_id = "01"
        self.tmp_token_visited_nodes.append(node_id)
        if token._.coref_spans:
            # If the Token is part of a mention of an entity, go to node "02"
            self._02_coref_chain_has_noun(token)
        else:
            # If the Token is not a mention in a chain, do action of node "A"
            self._add_token(token, is_modification=False)

    def _02_coref_chain_has_noun(self, token: Token) -> NoReturn:
        node_id = "02"
        self.tmp_token_visited_nodes.append(node_id)
        # Chain O, which is the only one being processed in the current configuration,
        # is guaranteed to be at token._.coref_chains[0] (if the chain is not empty).
        # This guarantee comes from the extension Token._.coref_spans, on which ._.coref_chains rely on,
        # that sorts the mentions a Token is part of according to their length, so that the first item
        # is always the shortest mention Token N is part of.
        chainO_pos = {tok.pos_ for span in token._.coref_chains[0] for tok in span}
        if any(pos in ["NOUN", "PROPN"] for pos in chainO_pos):
            # Go to node 3
            self._03_chain_has_noun_mention_nearby(token)
        else:
            # Simple Rule A (Add Token)
            self._add_token(token, is_modification=False)

    def _02bis_is_mention_p_1st_orig_doc(self, token: Token) -> NoReturn:
        node_id = "02bis"
        self.tmp_token_visited_nodes.append(node_id)
        if token._.coref_spans[0]._.mention_id[0] \
                not in {mention for sent in self.stats_encountered_np_chains_in_sent for mention in sent} \
                and self.param_replace_1st_mention:
            # Mention P is the 1st mention encountered in this document for chain O
            # Replace the whole mention by the very 1st nominal mention in chain O
            self._resolve_rule_abis(token)
        else:
            # Mention P is NOT the 1st mention encountered in the new document
            # Simply add Token N as is
            self._add_token(token, is_modification=False)

    def _03_chain_has_noun_mention_nearby(self, token: Token) -> NoReturn:
        node_id = "03"
        self.tmp_token_visited_nodes.append(node_id)
        chainO_id, _ = token._.mention_id[0]
        if self.param_resolution_window >= 0:
            # Only take into account positive values, meaning coreference is resolved except if
            # chain O has already been encountered in the current sentence or in x previous ones.
            # If window resol was set to less than 0, then resolve coreference nevermind if
            # a mention to the same chain is already present in current or any previous sentence.
            window = self.param_resolution_window + 1  # +1 because backward iteration on list
            # Add once again 1, because otherwise last element not included in the loop
            bw_window = - (min(window, len(self.stats_encountered_np_chains_in_sent)) + 1)
            for i in range(-1, bw_window, -1):
                # Check if the chain is already referenced to by a nominal mention
                if chainO_id in self.stats_encountered_np_chains_in_sent[i]:
                    self._add_token(token, is_modification=False)
                    # break loop and exit function
                    return
        # Go to node 4 if no condition is met (if there is no window set, or no mention
        # of chain O has been encountered in the window)
        self._04_token_is_pron(token)

    def _04_token_is_pron(self, token: Token) -> NoReturn:
        node_id = "04"
        self.tmp_token_visited_nodes.append(node_id)
        if token.pos_ == "PRON":
            self._05_token_pron_nsubj_3rd(token)
        else:
            self._18_token_is_det(token)

    def _05_token_pron_nsubj_3rd(self, token: Token) -> NoReturn:
        node_id = "05"
        self.tmp_token_visited_nodes.append(node_id)
        if token.pos_ == "PRON" and token.dep_ == "nsubj":
            # Note : spacing is important in a document, otherwise parser is not sure what
            #   is the function of, for instance, "-il" in the sentence and mark it as dep
            #   ("unclassified dependent" instead of "nsubj")
            #   shortcut to if not 3rd person add_token
            if token.morph.to_dict().get("Person") != '3':
                self._add_token(token, is_modification=False)
            else:
                self._06_sentence_m_has_question(token)
        else:
            self._12_token_pron_obj_3rd(token)

    def _06_sentence_m_has_question(self, token: Token) -> NoReturn:
        node_id = "06"
        self.tmp_token_visited_nodes.append(node_id)
        # Check all the next tokens of that sentence for a question mark
        # Tokens of the current sentence are available in self.tmp_tokens_queue
        for next_token in self.tmp_tokens_queue:
            if next_token.text == "?":
                # The sentence looks like it is, or include, a question mark
                self._11_sentence_m_has_inversion(token)
                return
        # Sentence is not a question
        self._07_token_n_1_is_t(token)


    def _07_token_n_1_is_t(self, token: Token) -> NoReturn:
        node_id = "07"
        self.tmp_token_visited_nodes.append(node_id)
        # Avoid error in case Token N is the first elem of document (although very unlikely)
        if token.i >= 1:
            if token.doc[token.i -1].text.lower() == "-t":
                self._resolve_rule_d(token)
            else:
                self._08_token_is_sconj(token)
        else:
            # Just add the Token if it's the 1st Token of the Doc
            self._add_token(token, is_modification=False)

    def _08_token_is_sconj(self, token: Token) -> NoReturn:
        node_id = "08"
        self.tmp_token_visited_nodes.append(node_id)
        # Is Token N-1 a Sconj (que, qu', lorsque, ...)?
        # Logically, Token N-1 is the last token added to self.target_tokens
        if self.target_tokens and self.target_tokens[-1].pos_ == "SCONJ":
                self._09_is_n_1_sconj_contracted(token)
        else:
            self._resolve_rule_e(token)

    def _09_is_n_1_sconj_contracted(self, token: Token) -> NoReturn:
        node_id = "09"
        self.tmp_token_visited_nodes.append(node_id)
        # Is Token N-1 a contracted Sconj (qu', lorsqu')?
        # Logically, Token N-1 is the last token added to self.target_tokens
        if self.target_tokens[-1].text.lower() in ["qu'", "lorsqu'"]:
            self._10_is_mention_1st_char_vowel(token)
        else:
            self._resolve_rule_e(token)

    def _10_is_mention_1st_char_vowel(self, token: Token) -> NoReturn:
        node_id = "10"
        self.tmp_token_visited_nodes.append(node_id)
        # If the mention is a vowell, the preceding SCONJ does not need to be decontracted
        # "qu'il ..." -> "qu'Ivan ..."
        if self._get_replacement_noun_mention(token).text[0].lower() in "aeiou":
            self._resolve_rule_e(token)
        else:
            # "qu'il ..." -> "que Benoit ..."
            self._resolve_rule_e_bis(token)

    def _11_sentence_m_has_inversion(self, token: Token) -> NoReturn:
        node_id = "11"
        self.tmp_token_visited_nodes.append(node_id)
        # Iterate backwards in a window of 2 tokens before Token N.
        # Find if there is a Verb or AUX before Token N (Pron nsubj 3rd Person)
        bw_window_i = - (min(len(self.target_tokens), 2) + 1)  # +1 because backward loop
        for i in range(-1, bw_window_i, -1):
            if self.target_tokens[i].pos_ in ["AUX", "VERB"]:
                self._resolve_rule_f(token)
                return
        # If no VERB (or AUX) has been found in that window, try node 8
        self._08_token_is_sconj(token)

    def _12_token_pron_obj_3rd(self, token: Token) -> NoReturn:
        node_id = "12"
        self.tmp_token_visited_nodes.append(node_id)
        if token.dep_ == "obj":
            self._13_token_followed_bt_verb(token)
        else:
            self._14_token_pron_iobj_3rd(token)

    def _13_token_followed_bt_verb(self, token: Token) -> NoReturn:
        node_id = "13"
        self.tmp_token_visited_nodes.append(node_id)
        if self.doc[token.i +1].pos_ in ["VERB", "AUX"]:
            self._resolve_rule_g(token)
        else:
            self._02bis_is_mention_p_1st_orig_doc(token)

    def _14_token_pron_iobj_3rd(self, token: Token) -> NoReturn:
        node_id = "14"
        self.tmp_token_visited_nodes.append(node_id)
        # Check if the pronoun is indirect object
        if token.dep_ == "iobj":
            self._15_token_n_1_nsubj(token)
        else:
            self._02bis_is_mention_p_1st_orig_doc(token)

    def _15_token_n_1_nsubj(self, token: Token) -> NoReturn:
        node_id = "15"
        self.tmp_token_visited_nodes.append(node_id)
        if self.doc[token.i - 1].dep_ == "nsubj":
            self._16_sentence_m_has_noun_further(token)
        else:
            self._17_token_n_1_pron_obj_3rd(token)

    def _16_sentence_m_has_noun_further(self, token: Token) -> NoReturn:
        node_id = "16"
        self.tmp_token_visited_nodes.append(node_id)
        for next_token in self.tmp_tokens_queue:
            if next_token.pos_ == "NOUN" and next_token.dep_ == "obj":
                self._resolve_rule_h(token)
                return
        self._02bis_is_mention_p_1st_orig_doc(token)

    def _17_token_n_1_pron_obj_3rd(self, token: Token) -> NoReturn:
        node_id = "17"
        self.tmp_token_visited_nodes.append(node_id)
        if self.doc[token.i -1].pos_ == "PRON" \
                and self.doc[token.i - 1].dep_ == "obj" \
                and self.doc[token.i - 1].morph.to_dict().get("Person") == '3':
            self._resolve_rule_i(token)
        else:
            self._02bis_is_mention_p_1st_orig_doc(token)

    def _18_token_is_det(self, token: Token) -> NoReturn:
        node_id = "18"
        self.tmp_token_visited_nodes.append(node_id)
        if token.pos_ == "DET":
            self._19_token_is_det_poss(token)
        else:
            self._02bis_is_mention_p_1st_orig_doc(token)

    def _19_token_is_det_poss(self, token: Token) -> NoReturn:
        node_id = "19"
        self.tmp_token_visited_nodes.append(node_id)
        if token.morph.to_dict().get("Poss") == "Yes":
            self._resolve_rule_c(token)
        else:
            self._20_token_is_det_demo(token)

    def _20_token_is_det_demo(self, token: Token) -> NoReturn:
        node_id = "20"
        self.tmp_token_visited_nodes.append(node_id)
        if token.morph.to_dict().get("PronType") == "Dem":
            self._21_mention_lgt_token(token)
        else:
            self._02bis_is_mention_p_1st_orig_doc(token)

    def _21_mention_lgt_token(self, token: Token) -> NoReturn:
        node_id = "21"
        self.tmp_token_visited_nodes.append(node_id)
        if len(token._.coref_chains[0]) > 1:
            self._resolve_rule_b(token)
        else:
            self._02bis_is_mention_p_1st_orig_doc(token)