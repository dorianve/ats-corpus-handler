import spacy
from spacy import Language
from spacy.tokens import Doc
from tqdm import tqdm
import atsch
from atsch import parser
import json
import os

@Language.component('to_uncontracted_articles')
def to_uncontracted_articles(doc: Doc) -> Doc:
    tokens = parser.tokens2list(doc)
    spaces = parser.spaces2list(doc)
    uncontracted_doc, is_uncontracted, aligned = parser.contracted2full(tokens, align=[spaces])
    doc = Doc(doc.vocab, words=uncontracted_doc, spaces=aligned[0], user_data=doc.user_data)
    doc.user_data["is_uncontracted_article"] = is_uncontracted or []
    return doc


nlp = spacy.load("fr_dep_news_trf")
input_dir = "txt_files"
output = "json_files"

nlp.add_pipe("to_uncontracted_articles", first=True)


for filename in tqdm(os.listdir(input_dir)):
    with open(os.path.join(input_dir, filename), "r") as txt_file:
        txt = ""
        for line in txt_file.readlines():
            line = line.strip("\n")
            line = line.strip()
            # Only add the line if non empty
            if line:
                # Add space before line if text is not empty
                if txt:
                    txt += " "
                txt += line
        doc = nlp(txt)
        doc.user_data["doc_key"] = filename
        dict_doc = atsch.parser.doc2dict(doc, other=[("is_uncontracted_article", doc.user_data["is_uncontracted_article"])])
    filename = filename.strip(".txt")
    with open(os.path.join(output, filename + ".json"), "w") as json_file:
        json.dump(dict_doc, json_file)
