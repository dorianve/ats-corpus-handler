""" 
"""

import json
import warnings
from tqdm import tqdm

count_modify = 0
print("loading model")
nlp = fr_dep_news_trf.load()
print("loaded")
for file in tqdm(os.listdir()):
    with open(file, "r") as json_file:
        json_doc = json.load(json_file)
    doc = parser.dict2doc(json_doc, nlp.vocab)
    correct_sents = [[tok.text for tok in sent] for sent in doc.sents]
    correct_len = len(list(doc.sents))
    for tok in doc:
        if tok.is_sent_start not in [True, False]:
            print("!!!!! Sent start is not clear")
            print(f"Value: {tok.is_sent_start} (should be True or False")
    has_len_changed = False
    for pipe_name, pipe in nlp.pipeline:
        pipe(doc)
        result_sents = [[tok.text for tok in sent] for sent in doc.sents]
        if correct_sents != result_sents and not has_len_changed:
            has_len_changed = True
            print(f"Doc segmentation changed with '{pipe_name}' !!!!")
            print(doc.user_data["doc_key"])
            print(f"expected len {correct_len}, got len {len(list(doc.sents))}")
            count_modify += 1

