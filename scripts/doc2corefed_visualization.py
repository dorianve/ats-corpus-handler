import atsch
from atsch import parser
from atsch.coref_resolver import CorefResolver
import os
import spacy
from spacy.tokens import Doc
import json
from tqdm import tqdm
import numpy as np
import pandas as pd


def create_html_representation(resolver: CorefResolver, corefed_doc: Doc, original_doc: Doc) -> str:
    # Sort modifications by sentences (list[list[Tokens]])
    modifications = []
    for is_modification, sent_start in zip(resolver.target_is_modification, resolver.target_sent_starts):
        if sent_start:
            modifications.append([])
        modifications[-1].append(is_modification)
    # Prepare the html file
    html_rep = ""
    html_rep += f'<h1>{doc.user_data.get("doc_key")}</h1>\n'
    html_corefed = f"<h2>{doc.user_data.get('doc_key')} : Avec coréférence </h2>\n"
    # Start the table for modified sentences, with first row
    html_corefed += "<table>\
                        <tr>\
                            <th>ID</th>\
                            <th>Phrases</th>\
                        </tr>\n"
    current_sent = ""
    sent_id = 0
    last_mention_span = None
    for token, is_modification, nodes in zip(corefed_doc, resolver.target_is_modification,
                                             resolver.stats_visited_nodes):
        # Check if it is a new sentence
        if token.is_sent_start:
            # First, add the previously processed sentence if it exists (current_sent)
            if current_sent:
                # Next sent is incremented
                sent_id += 1
                # If there is already a sentence in new sent, it is the prev sent
                html_corefed += current_sent
                # Close the line in the table
                html_corefed += "</td>\n</tr>\n"
                # reset the new sentence
                current_sent = ""
            # Start the row
            html_corefed += "<tr>\n"
            # indicate the id of the sentence
            if token.sent.text != list(original_doc.sents)[sent_id].text and not any(modifications[sent_id]):
                # If the sentence is not indicated as containing modifications, but the sentence is different
                # nonetheless from the sentence in the original file (eg. words were deleted but not replaced)
                current_sent += '<td><b style="color:red;">{}</b></td>\n'.format(sent_id)
            else:
                # If the sentence contains modification, put it in bold and blue, otherwise, not in bold
                current_sent += "<td>{}</td>\n".format('<b style="color:blue;">{}</b>'.format(sent_id)
                                                       if any(modifications[sent_id]) else sent_id)
            # start the second cell, for the tokens of the sentence
            current_sent += "<td>\n"
        # Add the tokens of the sentence
        if is_modification:
            # Check if token is part of a mention
            if token._.coref_spans:
                if last_mention_span and last_mention_span != token._.coref_spans[0]:
                    # There is an open mention_Span, but it's not the same as the current mention
                    # Close previous mention
                    current_sent += "</b></span>"
                    # initiate new mention span
                    last_mention_span = None
                if not last_mention_span:
                    # No mention previously set, or it has been reseted
                    # This is a new mention
                    last_mention_span = token._.coref_spans[0]
                    # add open the mention
                    current_sent += '<span chain="{}"><b style="color:tomato;">'.format(
                        [span.text for chain in token._.coref_chains for span in chain])
                # Add the token
                current_sent += f'<span coref_nodes="{nodes}">'
                current_sent += f"{token.text} "
                current_sent += f'</span>'
            else:
                if last_mention_span:
                    # close and reset last_mention_span if token is not a mention
                    last_mention_span = None  # Reset the mention
                    current_sent += "</b></span>"
                # If token is not a mention, just add the token as simple modification
                current_sent += f'<span coref_nodes="{nodes}">' \
                                f'<b style="color:#7FFF00;">{token.text}</b> ' \
                                f'</span>'
        else:
            # Check if previous token was a modification mention and close the mention
            if last_mention_span:
                last_mention_span = None  # Reset the mention
                current_sent += "</b></span>"
            # simply add the token as such if no modification was applied
            current_sent += f'<span coref_nodes="{nodes}">'
            current_sent += f"{token.text} "
            current_sent += f'</span>'
    # Close the table
    html_corefed += "</table>"
    html_rep += html_corefed
    return html_rep

def gen_orig_doc_html_rpz(doc:Doc) -> str:
    html_rep = f"<h2>{doc.user_data.get('doc_key')} : texte original</h2>\n"
    html_rep += '<table border=1 style="border-collapse: collapse; border: 1px solid black;">\n'
    html_rep += '<tr>\n<th>ID</th>\n<th>Phrases</th>\n</tr>'
    for i, sent in enumerate(doc.sents):
        html_rep += "<tr>\n"
        html_rep += f"<td>{i}</td>\n"
        html_rep += "<td>\n"
        # add inner table
        html_rep += '<table border=1 style="border-collapse: collapse; border: 1px solid black;">'

        # Add token index
        html_rep += "<tr>"
        for tok in sent:
            html_rep += f"<td>{tok.i}</td>"
        # Close line in inner table
        html_rep += "</tr>"
        # Add token text
        html_rep += "<tr>"
        for tok in sent:
            html_rep += f"<td>{tok.text}</td>"
        # Close line in inner table
        html_rep += "</tr>"
        # Add token pos
        html_rep += "<tr>"
        for tok in sent:
            html_rep += f"<td>{tok.pos_}</td>"
        # Close line in inner table
        html_rep += "</tr>"
        # Add token dep
        html_rep += "<tr>"
        for tok in sent:
            html_rep += f"<td>{tok.dep_}</td>"
        # Close line in inner table
        html_rep += "</tr>"
        ## Add token morph
        html_rep += "<tr>"
        for tok in sent:
            html_rep += f"<td>{tok.morph}</td>"
        # Close line in inner table
        html_rep += "</tr>"

        # close inner table
        html_rep += "</table>"
        html_rep += "</td>\n"
        html_rep += "</tr>\n"
    html_rep += "</table>"
    return html_rep

def gen_data_frame(resolver: CorefResolver, corefed_doc: Doc, original_doc: Doc):
    doc_key = []
    sent_i = []
    modified_sentence = []
    orig_sentence = []
    chain_new_doc = []
    chain_old_doc = []
    grammar = []
    coherence = []
    pertinence = []
    error_type = []
    comments = []
    visited_nodes = []
    last_modif_visited_nodes = None
    for i, new_token in enumerate(corefed_doc):
        if resolver.target_is_modification[i]:
            if last_modif_visited_nodes == resolver.stats_visited_nodes[i]:
                continue
            last_modif_visited_nodes = resolver.stats_visited_nodes[i]
            # Add data
            doc_key.append(original_doc.user_data.get("doc_key"))
            sent_i.append(new_token._.sent_i)
            modified_sentence.append(" ".join(tok.text for tok in new_token.sent))
            orig_sentence.append(list(original_doc.sents)[new_token._.sent_i].text)
            chain_new_doc.append(np.NAN if not new_token._.coref_chains else [span.text for span in new_token._.coref_chains[0]])
            old_doc_chain = np.NAN if not new_token._.coref_chains else original_doc._.get_coref_chains()
            chain_old_doc.append(old_doc_chain)
            grammar.append(np.NAN)
            coherence.append(np.NAN)
            pertinence.append(np.NAN)
            error_type.append(np.NAN)
            comments.append(np.NAN)
            nodes = [node for node in last_modif_visited_nodes]
            visited_nodes.append(nodes or np.NAN)
    return pd.DataFrame({"ID_doc" : doc_key,
                         "ID_phrase": sent_i,
                         "Phrase_modif": modified_sentence,
                         "Phrase_orig": orig_sentence,
                         "Chaine_nouvelle": chain_new_doc,
                         "Chaine_ancienne": chain_old_doc,
                         "Noeuds": visited_nodes,
                         "Grammatical": grammar,
                         "Coherence": coherence,
                         "Pertinence": pertinence,
                         "Type_erreurs": error_type,
                         "Commentaires": comments})

if __name__ == "__main__":
    os.chdir("/mnt/sharedhome/dorian/Documents/Documents/Etudes/2018-2019-TAL/z-corpora/RTBF_leSOIR2021/rtbf_corefed_jsonlines_predictions")
    output = "../html_reps"
    nlp = spacy.load("fr_dep_news_trf")
    # create a DataFrame to keep data in
    df = pd.DataFrame()
    # Iterate over all corefed files (must be in current directory
    for file in tqdm(os.listdir()):
        # Open the file and read with json
        with open(file, "r") as json_file:
            print("Opening File")
            doc_dict = json.load(json_file)
        # Create a Doc from the json file
        print("Creating the Doc")
        doc = parser.dict2doc(doc_dict, nlp.vocab)
        # Process the doc with all the elements of the pipeline
        print("Processing Doc with Pipeline")
        for _, pipe in nlp.pipeline:
            pipe(doc)
        # The pipeline should not change the number of sentences
        assert len(doc_dict.get("sentences")) == len(list(doc.sents))
        # Generate the coreferenced doc
        print("Resolve coreference")
        resolver = CorefResolver(doc, target_is_summary=False, resolution_window=0, replace_first_mention=False)
        corefed_doc = resolver()
        # Only make html representation for documents that have modifications
        print("Prepare html file")
        if any(resolver.target_is_modification):
            html_rep = create_html_representation(resolver, corefed_doc, doc)
            html_rep += gen_orig_doc_html_rpz(doc)
            file_name = doc.user_data.get("doc_key").split(':')[-1]
            file_name = file_name.removesuffix(".txt")
            print("write the html file")
            with open(os.path.join(output, file_name + ".html"), "w") as html_file:
                html_file.write(html_rep)
            # Add to the pandas file
            df = pd.concat([df, gen_data_frame(resolver, corefed_doc, doc)], ignore_index=True)
    df.to_excel(os.path.join(output, "modification.xlsx"))
