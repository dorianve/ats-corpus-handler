function create_form(){
  // This script generates Google Forms from a json file of document.
  // Has to be executed in Google Apps Scripts

  //json_data will be 1 orig doc per row
  var json_data = JSON.parse(DriveApp.getFileById("1gefKDsdtN56lxBc4TjUHGRE-zjh25fm8").getBlob().getDataAsString())

  // json_data is treated as a queue
  while (json_data.length){
    // While there is some data in json data, create docs
    // Now, determine which doc go with in which forms
    // Prepare 3 docs per form
    Logger.log("started processing the Data (1st while loop)")
    var docs_current_form = []
    
    while(docs_current_form.length < 1 && json_data.length != 0){
      // Add a doc to the list of docs to include in the form
      docs_current_form.push(json_data.shift())
    } 
    Logger.log(docs_current_form.length + " docs were added to the queue to make the first form")


    // Now the list of docs to include in the form has been updated
    // Prepare first summary
    // docs_current_form is shift (dequeue) when making a form for 1 doc
    while(docs_current_form.length){
      // prepare name of the form by contatenating the names of the original docs that compose the form
      Logger.log("Preparing a form")
      var file_name = ""
      for(var i = 0, size = docs_current_form.length; i < size ; i++){
        file_name = file_name.concat(docs_current_form[i].doc_key.concat("-"))
      }
      Logger.log("Form will be named " + file_name)

      // creating the Form object
      var form = FormApp.create(file_name);
      form.setTitle("Résumé Automatique de Texte et résolution de coréférence")
          .setDescription(
        "Bonjour, merci d’avoir accepté de remplir ce formulaire dans le cadre de mon mémoire sur le résumé automatique de texte associé à un outil de résolution de coréférence.\n\n\
1) Qu’est-ce que le résumé automatique de texte (RAT) ?\n\
    Ici je m’intéresse au résumé automatique extractif, il s’agit simplement d’extraire des phrases d’un texte original pour former un « résumé ».\n\n\
2) Qu'est-ce que la coréférence\n\
    Par exemple dans la phrase« Il le lui a donné », « il », « le » et « lui » font référence à d’autres mots dans le texte. Dans cet exemple, « il » est, par exemple coréférent de « Benoît », « le » de « le cake » et « lui » de « Kevin ». Ainsi, dans un texte, les coréférents font référence à la même entité.\n\n\
Dans ce questionnaire, il vous sera demandé de lire un ou plusieurs textes et d’ensuite évaluer des résumés automatiquement générés depuis ce(s) texte(s) selon différents critères (la cohérence, la qualité de l'information extraite et la qualité générale). Pas de panique, les critères seront défini plus amplement en temps voulu.\
\n\n\
Puisqu'il s'agit de lire des textes parfois un peu long, je suggère fortement de zoomer sur la page lors de la lecture ('ctrl' + 'molette de souris', ou 'ctrl' + '+' sur ordinateur)\n\n\Légende :"
        )
      .setProgressBar(true)
  
      form.addTextItem()
          .setTitle("Entrez votre nom et prénom ou un pseudonyme")
          .setHelpText("IMPORTANT : Si vous remplissez plusieurs formulaires, assurez-vous de toujours utiliser le même nom ou pseudo\n\n\
Le but est de pouvoir grouper vos réponses si vous décidez de remplir plusieurs formulaires. Évitez donc de ne mettre que votre prénom, pour ne pas risquer d'être confondu avec un·e autre participant·e.")
          .setRequired(true)
    
      // Make 1p of the form per doc
      Logger.log("Processing each pages of the form (1p per original doc)")
      while (docs_current_form.length){
        Logger.log("docs_current_form is of length " + docs_current_form.length)
        var current_doc = docs_current_form.shift();
        Logger.log("Current doc is " + current_doc.doc_key)

        form.addPageBreakItem()
            .setTitle("Évaluation de résumés")
            .setHelpText("Un texte d'actualité (datant de 2008-2009) va vous être présenté, ainsi que deux résumés automatiquement générés, constitués de phrases extraites du texte.\n\
\n\n\
Il vous est demandé d'évaluer ces deux résumés en fonction de la cohérence, de la qualité de l'information sélectionnée et de la qualité générale.\n\n\
1) Qualité de l'information sélectionnée (contenu) :\n\
Un résumé qui a une bonne qualité d'information est un résumé qui a su sélectionner le contenu le plus pertinent d'un \
texte original. Les phrases extraites sont les plus informatives.\n\n\
\
2) Cohérence :\n Un résumé est cohérent si ce qui est écrit a du sens dans le monde réel (« Le chat était un chien » \
est un exemple incohérent) et le sens dans le résumé est le même que dans son texte d'origine (par exemple, si la phrase \
« Il dort. » est placé après « je vois le chat » dans le résumé, alors que dans le résumé elle vient juste après « \
Je vois le meunier », le sens a changé et le résumé n'est pas cohérent avec le texte).\n\n\
\
3) Qualité générale : \n\
Selon vous, le résumé présenté est-il de bonne qualité dans son ensemble ?\
\n\n\
Attention : pour des raisons techniques, tous les articles contractés \
« du », « des », « au », « aux » ont été remplacés par leur version non contractée « de le », « des les », « à le », « à les », \
veuillez faire abstraction de ce traitement et ne pas considérer ces cas comme des erreurs. \n\
Par ailleurs, tous les mots et caractères de ponctuation sont séparés par un espace, ceci ne doit pas non plus\
être considéré comme une erreur.\n\n\
Le texte ci-dessus sera rappelé pour chaque document, pour que vous puissiez retourner voir les exemples au besoin.\n\n\
Si les textes s'affichent trop petit sur votre écran, n'hésitez pas à zoomer sur la page.")

        form.addSectionHeaderItem()
          .setTitle("Texte complet à lire pour comprendre le contexte général")
          .setHelpText(current_doc['text'])

        var summaries = ""
        var likert_rows = []
        var grammaticality_warning = "\n------\nRappel : "
        grammaticality_warning = grammaticality_warning.concat("pour des raisons techniques, tous les articles contractés \
« du », « des », « au », « aux » ont été remplacés par leur version non contractée « de le », « des les », « à le », « à les », \
veuillez faire abstraction de ce traitement et ne pas considérer ces cas comme des erreurs. \n\
Par ailleurs, tous les mots et caractères de ponctuation sont séparés par un espace, ceci ne doit pas non plus \
être considéré comme une erreur.")
        // Create the representations of the summaries as txt
        for(var i = 0, size = current_doc.generated_summaries.length; i < size ; i++){
          summaries = summaries.concat("\n--------------------\n")
          summaries = summaries.concat("[ Résumé ".concat(i + 1).concat(" ]\n"))
          summaries = summaries.concat(current_doc.generated_summaries[i].text)
          // And prepare the likert rows as well
          likert_rows.push("Résumé ".concat(i +1))
        }

        // Contenu
        form.addGridItem()
          .setTitle("Qualité de l'information sélectionnée (contenu)")
          .setHelpText("Les résumé suivants ont-ils bien sélectionné le contenu à inclure ?\n".concat(summaries).concat(grammaticality_warning).concat("\n\nUn résumé ayant une bonne sélection de contenu a su identifier les phrases qui contiennent le plus d’informations pertinentes du texte \n\n\
Les résumés ont-ils bien sélectionné le contenu à inclure ?\n\
0 = le contenu sélectionné ne représente pas bien du tout le texte original\n\
5 = le contenu sélectionné représente très bien le texte original\n"))
          .setRows(likert_rows)
        .setColumns([0, 1, 2, 3, 4, 5])
      .setRequired(true)

        // Cohérence 
        form.addGridItem()
          .setTitle("Cohérence")
          .setHelpText("Les résumé suivants sont-ils cohérents ?\nLes résumés présentés ci-dessous sont les mêmes que ceux présentés à la question précédente.".concat(summaries).concat(grammaticality_warning).concat("\n\nUn résumé est cohérent si ce qui est écrit a du sens dans le monde réel (« Le chat était un chien » \
est un exemple incohérent) et le sens d'une phrase d'un résumé est le même que dans son texte d'origine. \n\n\
Comment évalueriez-vous la cohérence des résumés ?\n\
0 = le contenu sélectionné ne représente pas bien du tout le texte original\n\
5 = le contenu sélectionné représente très bien le texte original\n"))
          .setRows(likert_rows)
        .setColumns([0, 1, 2, 3, 4, 5])
      .setRequired(true)

      // Qualité générale  
        form.addGridItem()
          .setTitle("Qualité général")
          .setHelpText("Comment décrieriez-vous la qualité générale de ces résumés ?\nLes résumés présentés ci-dessous sont les mêmes que ceux présentés aux questions précédentes.".concat(summaries).concat(grammaticality_warning).concat("\n\nUn résumé ayant une bonne sélection de contenu a su identifier les phrases qui contiennent le plus d’informations pertinentes du texte \n\n\
D'après vous, quelle est la qualité générale des résumés présenté ?\n\
0 = le résumé n'est vraiment pas de bonne qualité\n\
5 = le résumé est de très bonne qualité\n"))
          .setRows(likert_rows)
        .setColumns([0, 1, 2, 3, 4, 5])
      .setRequired(true)

      form.addParagraphTextItem()
        .setTitle("Un autre commentaire sur les résumés ? (optionnel)")
        .setHelpText("Si vous avez des observations à partager concernant les résumés, n'hésitez pas à le faire ici")
      };
      Logger.log("All inner pages of the form were created")

      Logger.log("Adding thanks page")
      form.setConfirmationMessage("Merci d'avoir répondu à ce formulaire.\n\
N'hésitez par à remplir d'autres formulaires similaires ici : https://drive.google.com/drive/folders/1I1sXaMZjpqy1EhA0brxEtKvF9IaqELe5?usp=sharing \n\
Cela sera grandement apprécié ;) ."
  )
 
  // Create a spreadsheet to collect the anscwers
  Logger.log("Create and add a spreadsheet to store the result of the current form")
  var spreasheet_answers = SpreadsheetApp.create(file_name)
  form.setDestination(
    FormApp.DestinationType.SPREADSHEET, spreasheet_answers.getId()
  )

  Logger.log("Store and move the form to appropriate location")
  // Move the form to already existing Folder "formulaires"
    // The folder has to exist, you have to adapt the if
  var dest_folder = DriveApp.getFolderById("1I1sXaMZjpqy1EhA0brxEtKvF9IaqELe5")  
  DriveApp.getFileById(form.getId()).moveTo(dest_folder)

  // Move the spreadsheet as well
  dest_folder = DriveApp.getFolderById("14EQ641W-z6tRZXNaqA8ns4w2Dptk-9yg")  // Again, the folder must exist and match the ID
  DriveApp.getFileById(spreasheet_answers.getId()).moveTo(dest_folder)

  Logger.log('Published URL: ' + form.getPublishedUrl());
    };
  };
}


