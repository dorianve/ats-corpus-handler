"""Transforms data structure of original RPM2 corpus to work well with atsch,
RPM2 is a corpus originally intended for multi summarization and described  in

    Loupy, Claude de, Marie Guégan, Christelle Ayache, Somara Seng, and Juan-Manuel Torres Moreno. 2010.
    “A French Human Reference Corpus for Multi-Document Summarization and Sentence Compression.”
    In Proceedings of the Seventh International Conference on Language Resources and Evaluation (LREC’10).
    Valletta, Malta: European Language Resources Association (ELRA).
    http://www.lrec-conf.org/proceedings/lrec2010/pdf/919_Paper.pdf.

400 news articles (of 2009):
    - 20 Topics,
        - 2 sets of 10 original docs per topic (named "C1" and "C2") (10 * 2 * 20 = 400 original docs)
            - C1 set: meant for classic ATS
            - C2 set: meant for **incremental** update summarization (knowing 1st set) (1 month after C1)
        - 4 reference summaries per cluster (20 * 2 * 4  = 160 reference summaries)
            - each summary has between 90 to 100 space separeted words
            - C2 = update summaries contain new information only (based on what the writer retained of C1)

Original organisation of data:
-d/ documents
    -f: T01_C1_01.txt
    -f: T01_C1_02.txt
    ...
-d/ reference
    -f: T01_C1_A1.txt
    -f: T01_C1_A2.txt
    ...
Where T is the Topic, C is the cluster, A is the annotator

We will keep that structure, but adapt it as to form
    - original doc : T001_C001_D001.json
    - reference summary: T001_C001_RF001.json
    - generated summary: T001_C001_D001_GS001.json
    Where T is the topic, C the cluster, D the document, RF the reference summary, GS the generated summary

We will not perform update summaries, therefore cluster 2 will not be retained.
Auhtorship of the reference summaries is kept inside the json files.
"""
import argparse

import numpy

from atsch import StatFactory
import atsch.parser as parser
import atsch.scripts.trf2vec_pipeline
import json
import sys
import os
import fr_dep_news_trf
import multiprocessing as mp
import numpy as np
from typing import Generator, Any
import re
from spacy.tokens import Doc
from pathlib import Path
from tqdm import tqdm
from spacy.language import Language
from spacy.tokens import Token

mp.set_start_method('fork') # Set multi process to use 'fork', otherwise moght not work because of pytorch bug


@Language.component('one_doc_one_sent')
def sent_boundaries(doc: Doc) -> Doc:
    """RPM2 is presentencized as 1 sentence per line. Doc should use the presentences boundaries

    Args:
        doc: A doc corresponding to 1 single sentence

    Returns:
        the doc corresponding to one single sentence, with is_sent_start set.
    """
    doc[0].is_sent_start = True
    for tok in doc[1:-1]:
        tok.is_sent_start = False
    return doc


@Language.component('to_uncontracted_articles')
def to_uncontracted_articles(doc: Doc) -> Doc:
    tokens = parser.tokens2list(doc)
    spaces = parser.spaces2list(doc)
    uncontracted_doc, is_uncontracted, aligned = parser.contracted2full(tokens, align=[spaces])
    doc = Doc(doc.vocab, words=uncontracted_doc, spaces=aligned[0], user_data=doc.user_data)
    doc.user_data["is_uncontracted_article"] = is_uncontracted or []
    return doc


def init_parser():
    """Parser for the arguments

    Returns:

    """
    parser = argparse.ArgumentParser(description='Process RPM2 corpus into tokenized & sentencized json files')
    parser.add_argument('-i', '--input',
                        help='Folder with the data inside',
                        required=True)
    parser.add_argument('-t', '--type',
                        help="The type of document your are processing",
                        choices=['original', 'reference'],
                        required=True)
    parser.add_argument('-o', '--output',
                        help='Folder where the processed files will be stored',
                        default='preprocessed_corpus')
    return vars(parser.parse_args())


def get_files(input_dir: str, exclude: str = None) -> Generator[tuple[str, dict[Any]], None, None]:
    """Get files that

    Args:
        input_dir: The directory in which the files are to be taken from
        exclude: A pattern that, if present in a file name, excludes it

    Yields: A 2d tuple, where 0 is the file read as a string and 1 a dictionnary of some context (doc_key and author)

    """
    # Todo : change doc_key to atsch standard naming system
    for path, _, files in os.walk(input_dir):
        for name in files:
            # break if exclude is met
            if exclude and re.findall(exclude, name):
                print("--- {} marked for exclusion".format(name))
                continue
            with open(os.path.join(path, name), 'r', encoding='utf-8') as file:
                author = "".join(re.findall("A[0-9]+", name))
                yield file.read(), {'doc_key': name, 'author': author}


def main():
    args = init_parser()  # init arguments from cli
    input_dir = args["input"]
    docs_type = args["type"]
    output_dir = args["output"]
    json_dir = os.path.join(output_dir, docs_type)
    pandas_dir = os.path.join(output_dir, "stats")
    numpy_dir = os.path.join(output_dir, "vectors")  # Where the trf vectors will be stored

    # Create the output folders
    Path(json_dir).mkdir(parents=True, exist_ok=True)
    Path(pandas_dir).mkdir(parents=True, exist_ok=True)
    Path(numpy_dir).mkdir(parents=True, exist_ok=True)

    # nbr_files = len([name for name in os.listdir('.') if os.path.isfile(name)])

    print("Preparing spaCy Language Model")
    nlp = fr_dep_news_trf.load()
    # print("Adding trf verctors to pipeline")
    print("nlp is loaded")
    nlp.disable_pipes(["parser", "attribute_ruler"])
    # disable at least the parser, otherwise it will mess up with sentence boundaries

    print("adding pipes to nlp object")
    nlp.add_pipe("one_doc_one_sent", first=True)
    nlp.add_pipe("to_uncontracted_articles", first=True)
    nlp.add_pipe("tensor2attr", last=True)

    print("Instanciating Stat Dataframe")
    stat_fact = StatFactory()
    print("Data Frame created")

    print("Processing files")

    for file, context in get_files(input_dir, exclude="C2"):
        disabled_pipes = nlp.select_pipes(enable=["to_uncontracted_articles", "one_doc_one_sent"])
        print(nlp.pipeline)
        # disable at least the parser, otherwise sentence starts are overwritten

        print(context["doc_key"])
        sents = file.split("\n")
        sents = [sent.rstrip() for sent in sents]
        sents = [sent for sent in sents if sent]  # Only add sentence if it is not empty.
        title = ""
        if docs_type == "original":
            title = sents[0]
        docs = list(nlp.pipe(sents))

        contraction_status = []
        # Retrieve the contraction status of the articles
        # and then delete "is_uncontracted_article" to avoid warning
        for doc in docs:
            contraction_status.extend(doc.user_data["is_uncontracted_article"])
            doc.user_data.pop("is_uncontracted_article")

        print("making a big doc from sentences")
        doc = Doc.from_docs(docs)

        doc.user_data["is_uncontracted_article"] = contraction_status
        doc.user_data["title"] = title
        doc.user_data["doc_key"] = "ge:" + context["doc_key"]
        doc.user_data["author"] = context["author"]
        doc.user_data["doc_type"] = docs_type

        disabled_pipes.restore()
        disabled_pipes = nlp.select_pipes(disable=["to_uncontracted_articles", "one_doc_one_sent"])
        print(nlp.pipeline)
        # Applying the rest of the pipeline
        for pipe_name, pipe in nlp.pipeline:
            doc = pipe(doc)

        disabled_pipes.restore()  # Restore disabled pipes for the next run of the loop.

        # Use atsch StatFactory to collect data about this document and add it to the stat DataFrame
        stat_fact.add_doc(doc, nlp, context["doc_key"], docs_type)

        # Save vector representation to disk
        numpy.save(os.path.join(numpy_dir, context["doc_key"] + "_doc"), doc.vector)
        numpy.save(os.path.join(numpy_dir, context["doc_key"] + "_tokens"), [tok.vector for tok in doc])

        # delete the trf information, so that it is not kept in the json file.
        doc.user_data.pop(('._.', 'trf_data', None, None))

        # Use atsch parser to generate a nice json file and store in in the correct output file.
        with open(os.path.join(json_dir, context["doc_key"] + ".json"), 'w') as json_file:
            json.dump(parser.doc2dict(doc, other=list(doc.user_data.items())), json_file)


    # Save the stat dataframe on the disk
    print("Writing stats to file")
    stat_fact.data.to_feather(os.path.join(pandas_dir, docs_type + ".ftr"))
    print("Stats written")


if __name__ == '__main__':
    sys.exit(main())
