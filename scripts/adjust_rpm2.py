import os
import json
import pandas as pd

# 1 = add coref chains to corresponding docs
def add_coref():
    with open("rpm2_corefed.jsonlines", 'r') as cofr_json:
        for line in cofr_json:
            cofred_doc = json.loads(line)
            file_name = cofred_doc['doc_key'].removeprefix("ge:") + ".json"
            with open(os.path.join("original", file_name), "r") as jsonfile:
                doc = json.load(jsonfile)
            doc["coref_chains"] = cofred_doc["predicted_clusters"]
            with open(os.path.join("original", file_name), "w") as jsonfile:
                json.dump(doc, jsonfile)


# 3 Change the doc key (remove prefix and suffix), change the filename, strip \uffef
def process(path):
    for filename in os.listdir(path):
        with open(os.path.join(path, filename), "r") as jsonfile:
            print("trying to load {}".format(os.path.join(path, filename)))
            doc = json.load(jsonfile)
            print("the file is loaded")
        # remove \ufeff char
        doc["sentences"][0][0] = doc["sentences"][0][0].removeprefix('\ufeff')
        # and from title
        doc["title"] = doc["title"].removeprefix("\ufeff")
        # also, remove ":ge" and "txt" from doc key
        doc["doc_key"] = doc['doc_key'].removeprefix("ge:")
        doc["doc_key"] = doc['doc_key'].removesuffix(".txt")
        # make own key for them, cluster and doc number
        doc['doc_theme'] = int(doc["doc_key"].split("_")[0].strip('T'))
        doc['doc_cluster'] = int(doc["doc_key"].split("_")[1].strip('C'))
        if doc["doc_type"] == "original":
            doc['doc_number'] = int(doc["doc_key"].split("_")[2])
            new_file_name = "T{:0>2}_C{}_D{:0>2}.json".format(doc['doc_theme'], doc['doc_cluster'], doc['doc_number'])
        if doc["doc_type"] == "reference":
            doc['doc_ref_sum_number'] = int(doc["doc_key"].split("_")[2].strip('A'))
            new_file_name = "T{:0>2}_C{}_RS{:0>2}.json".format(doc['doc_theme'], doc['doc_cluster'],
                                                               doc['doc_ref_sum_number'])
        # write changes to a new file
        with open(os.path.join(path, new_file_name), "w") as jsonfile:
            jsonfile.write(json.dumps(doc))
        # finally, remove old file
        os.remove(os.path.join(path, filename))

def update_keys(path):
	for filename in os.listdir(path):
		with open(os.path.join(path, filename), 'r') as json_file:
			doc = json.load(json_file)
		doc["doc_key"] = filename.removesuffix(".json")
		with open(os.path.join(path, filename), "w") as json_file:
			json.dump(doc, json_file)

def update_dataframe(path):
	df = pd.read_feather(path)
	for i, name in enumerate(df["doc_name"]):
		name = name.removesuffix(".txt").split("_")
		name[-1] = "D" + name[-1]
		df.loc[[i], ["doc_name"]] = "_".join(name)
