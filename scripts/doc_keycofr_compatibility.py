import json
import os


if __name__ == '__main__':
    input_dir = os.getcwd()
    for path, _, files in os.walk(input_dir):
        for name in files:
            with open(os.path.join(path, name), "r") as infile:
                document = json.load(infile)
                document["doc_key"] = "ge:" + document["doc_key"]
            with open(os.path.join(path, name), "w") as outfile:
                json.dump(document, outfile)
