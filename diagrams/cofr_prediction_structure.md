Atsch is being developped for my master thesis on ATS, with help of a coreference resolution tool for Frenche,
[COFR](https://github.com/boberle/cofr)

Files are in a jsonlines format (one document per line, json format).

- "sentences" contains the sentencized text [["tokens", "of", "sentence", "1"], ["tokens", "of", "2nd", "sent"]]
- "predicted_clusters" is a list of sublists. Each sublist is a cluster i.e a chain of reference.
    Each item in the cluster is a 2 elements list corresponding to a mention in the flatened text, where l[0] is the
    index in the tokenized text where the mention starts and l[1] where it ends.

In french, there are several structures to take into account while replacing pronouns
by their real world entity in the coreference chaine such as: 

- Citation "[...] **a-t-elle** indiqué" -> "a indiqué **Ingrid Betancourt**"
- Question "**A-t-il** eu froid ?" -> "**Jean** a-t-il eu froid ?"
- Possession 
  - "**Son** gros manteau jaune et vert est chaud." -> "**Le** gros manteau jaune et vert **de Jean** est chaud"
  - **[**[Son]1** domicile]5** , **[dont]** -> "Le domicile du trader, dont" (# T02_C1_02.txt)
- Possessif suivant "de" : "De **son** côté..." -> "*De le* côté **de Jean**" -> "*Du*  coté de **Jean**"
- Entité-article indéfini "**Un gros chat** s'avance. Il ronronne" -> "Un gros chat s'avance. ***Le*** **gros chat** ronronne"
- Référence précédente "**Ces derniers** étaient parvenu à faire croire [...] " -> "**Les guérilleros des Forces armées révolutionnaires de Colombie** étaient parvenu à faire croire [...]"
- Première personne, discour indirect "**j**'ai parlé avec [...]" ->  leave unchanged. "Nous sommes monté à bord"
- moi, toi, lui, nous, vous, leur 
  - "il lui a offert des fleurs" -> "Jean a offert des fleurs bleues à Jeanne".
  - "figure-toi que ..." -> "Figure-toi, Vincent, que" ?? 
- l' -> "Ce guérillero **l**'avait humilié" -> "Ce guérillero avait humilié **Betancourt**"



Autres cas empêchant l'autonomie des phrases

- Déterminants démonstratifs (ce, cette, ...) 
  - "Cette libéréation est survenue ..."  -> "La libération est survenue"
  - "Dans [une vidéo diffusée à [le mois de [novembre]]] , **[cette femme de 46 ans]1** était apparue amaigrie" -> "Dans une vidéo [...] , **[Betancour,~~cette~~ femme de 46 ans,]1** était apparue amaigrie" voir "Dans une vidéo [...] , **[Betancour]1** était apparue amaigrie"
- Mots liens : "Mais [**[ses]1** six années de détention] ont [...]" -> "~~Mais~~ Les sic année de détention de Betancourt ont [...]" (# T01_C1_05.txt)



Erreurs de détection des chaînes, ou problèmes vouées à réduire la qualité du résumé:

- Erreur de détection de chaîne de référence introduisant une erreur si résolution: 
  
  - "De **[son]25** coté , le général Padilla] a , dans [...]" -> "**Du** coté de **[le ministre de la défense Santos]25**, le général Padilla a [...]" (# T01_C1_02.txt)
  
  - "[les Colombiens]7 tentaient d' infiltrer [les guérilleros]7" -> évidemment, "les Colombiens" et "guérilleros" s'opposent en réalité.  (# T01_C1_03.txt)
  
  - "[Fabrice Delloye]10, [l' ex-mari d'[Ingrid Betancourt]2]10" est différent de  "[M. Uribe]10" (président Colombien à l'époque), pourtant indiqué dans la même chaîne (# T01_C1_03.txt)

- COFR est sensible à la casse : 
  
  - "**[les FARC]6**" n'est pas dans la même chaine que "**[la guérilla de les Farc]4**" (# T01_C1_04.txt)

- Les mention enchevêtrée, comme "**[**[sa]1** santé]6**]", pourraient également poser certains problèmes

- Parfois, il y a interpélation des journalistes (donc sans entité réelle dans le texte) "Pouvez **[-vous]1** imaginer dans [quel état] **[elle]1** était ? ". "Vous" s'addresse à l'interviewer, mais est interprété comme référent à  "Betancourt", tout comme "elle"

- Les titres, ont une structure particulière (souvent phrases non verbales sans article initial)
  
  - "Croissance minimale [...] Elle sera de 1%" -> "Croissance sera de 1% [...]", il manque un déterminant devant "Croissance" (# T03_C1_08.txt)

- Mauvaise segmentation de certaine mention. Dans l'exemple suivant, un guillement ouvrant fait partie de la mention, mais pas le fermant :  
  
  - "**[fichier Edvige '' inacceptable]1** '' Le Parti communiste [...]" (# T05_C1_01.txt). 





Il y a néanmoins, évidemment, aussi des cas où la résolution de coréférence devrait fonctionner sans problème, si les phrases concernées sont sélectionnées par le résumeur extractif:

- "Outre de **[les prix d' achat]4** [...] , **[ils]3** doivent aujourd'hui compter avec de [les crédits de plus en plus chers]" -> "[...], **les candidats** doivent [...]" (# T03_C1_02.txt)
- "Interrogée sur l' éventuelle mise en place d' un " fichier positif " , **[elle]2** a indiqué que c'était " une de les pistes examinées" -> "[...] Christine Lagarde a indiqué, ..." (# T03_C1_08.txt)



Compte tenu de l'assez grande autonomie des phrases dans le corpus, et le fait que seront remplacés uniquement les pronoms des phrases ne contenant pas en leur sein un référent nominal, il est probable que, même sur des texte ayant étés traités pour la coréférence  le résumé automatique ne contienne pas de phrases modifiées pour résoudre la coréférence. La proportion de ce genre de cas fera partie de l'analyse.



Ajouter partie évaluation coréférence seule dans les formulaires ?

- après lecture du texte original
  
  - "Les chaines de référence suivantes semblent-elles cohérentes, au vu de ce que vous avez pu lire ?" (ne contient pas d'erreur, contient une erreur, contient plusieurs erreurs, contient beaucoup d'erreurs)
    
    - options : 
      
      - montrer toutes les chaines du texte, avec pronoms inclus
      
      - montrer toute les chaines du texte, sans "stop word"
      
      - montrer uniquement les chaines ayant été utilisées dans le résumé automatiques
      
      - montrer uniquement les mentions
      
      - montrer les mentions avec quelques mots de contexte