# Save everything with Pickle format
#		pros : 
#			- easy to implement
#		cons :
#			- One Big file has to be loaded in memory all at once (inefficient)
#			- needs to save Spacy Vectors somewhere else anyway
# 
# 0_50.ndjson format
#		pros :
#			- can stream each document
#			- you don't load all the corpus at once, but still many texts
#				(you can still just open the file in a text editor)
#			- reduces the number of single files, so that explorer or cd doesn't 
#				crash when you manually visit the folders
#			- if in any case scenario, the number is the document ID, then you
#				knwo that reference summaries in 0_50.ndjson are for doc ids from 0 to 50
# 		cons : 
#			- makes it harder to just navigate there and know what documents 
#				or summaries you are dealing with
#			- since you can end up with many files, you've got to make sure
#				your python scritp does not forget any file.
#			- when loading data to python, you have to load the whole 0-50.ndjson to get 
#			the text in strings that you need, if, for some reason, you just want to
# 			load document 50. 
# 			!!! If not planned properly, you might end up reading the same lines several 
#			times to populate GeneratedSummaries and Reference Summaries !!! (linecache might help)
#			
#
# One file per original document format ?
#		pros :
#			- readability
#		cons :
#			- hard to navigate big corpora in a file explorer


dir:Nom_du_corpus/
	file:README # explains the structure of a corpus and how to open it with ATS Corpus Handler + how to use .ftr files
	file:config.ndjson #contains infos about: Name, datetime, number of documents, size of summaries, eval + ATSCH version
	-dir:/original/ # compulsary
		file:0_50.ndjson #contains : text, id, refsum_ids, gensum_ids
		file:51_100.ndjson
	-dir:/reference/ # if not present, no automatic evaluation available
		file:0_50.ndjson
		file:51_100.ndjson
	-dir:/generated/ # automatically created if not present
		file:0_50.ndjson
		file:51_100.ndjson
	-dir:/evaluation/ # automatically created if not present
		-dir:/automatic/
			file:average.ndjson
			file:0_50.ndjson
			file:51_100.ndjson
		-dir:/manual/
			file:average.ndjson
			file:0_50.ndjson
			file:51_100.ndjson
	-dir:/stats # automatically created if not present
		evaluation.ftr # an automatically generated Feather file populated with all the evaluation results.
		og_texts_meta.ftr # use this file to explore your corpus, have a look at the distribution, check for outliers
						# contains infos on the number of sentences, tokens and types and the date and type of 
						# document + compression rate, for every original texts, reference summaries and generated 
						# summaries when added.
						# (consider making a separate file for every type of document, in case of huge corpus).
						# USE FEATHER INSTEAD (compatible with Pandas and R, dead easy to convert to csv if really needed!)
						# https://towardsdatascience.com/the-best-format-to-save-pandas-data-414dca023e0d
		ref_summaries_meta.ftr
		gen_summaries_meta.ftr
	-dir:/spacy_docs/ # One .spacy file per doc. Spacy docs contains: Tokens, Document, Sentences, Vectors. 
					# Could also be called "cache", so that user knows it can be deleted safely and regenerated auto ?
		-dir:/NAMEOFVECTORMODEL
			file:docID.spacy
			file:refID_docID.spacy
			file:refID_docID.spacy
			file:genID_docID.spacy
			file:genID_docID.spacy
		-dir:/scripts #Where scripts can be put to be added in the pipeline(s) for
			# - adding a document (original, reference or generated doc)
			# - evaluating a generated doc
			# - (loading a document ?)
