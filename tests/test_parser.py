import json
import unittest
import spacy
import atsch.parser as parser
from spacy.tokens import Doc


class TestParser(unittest.TestCase):
    nlp = spacy.blank('fr')
    text = ['Ceci', 'est', 'une', 'première', 'phrase', '.', 'Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']
    spaces = [True, True, True, True, False, True, False, True, True, True, True, False]
    sents = [True, False, False, False, False, False, True, False, False, False, False, False]
    pos = ['PRON', 'AUX', 'DET', 'ADJ', 'NOUN', 'PUNCT', 'ADV', 'PUNCT', 'VERB', 'DET', 'ADJ', 'NOUN']
    lemmas = ['ceci', 'être', 'un', 'premier', 'phrase', '.', 'ensuite', ',', 'voici', 'un', 'deuxième', 'phrase']
    doc = Doc(nlp.vocab, words=text, spaces=spaces, sent_starts=sents, pos=pos, lemmas=lemmas)

    def test_is_flat_list(self):
        flat = [1,2,3,4,5,6]
        non_flat = [[1,2,3,4,5,6]]
        self.assertEqual(True, parser.is_flat_list(flat))
        self.assertEqual(False, parser.is_flat_list(non_flat))

    def test_sents2list(self):
        tokens = [['Ceci', 'est', 'une', 'première', 'phrase', '.'],
                  ['Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']]
        self.assertEqual(parser.sents2list(self.doc), tokens)

    def test_tokens2list(self):
        tokens = ['Ceci', 'est', 'une', 'première', 'phrase', '.',
                  'Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']
        self.assertEqual(parser.tokens2list(self.doc), tokens)

    def test_spaces2list(self):
        spaces = [True, True, True, True, False, True,
                  False, True, True, True, True, False]
        self.assertEqual(parser.spaces2list(self.doc), spaces)

    def test_pos2list(self):
        pos = ['PRON', 'AUX', 'DET', 'ADJ', 'NOUN', 'PUNCT',
               'ADV', 'PUNCT', 'VERB', 'DET', 'ADJ', 'NOUN']
        self.assertEqual(parser.pos2list(self.doc), pos)

    def test_lemmas2list(self):
        lemmas = ['ceci', 'être', 'un', 'premier', 'phrase', '.', 'ensuite', ',', 'voici', 'un', 'deuxième', 'phrase']
        self.assertEqual(parser.lemmas2list(self.doc), lemmas)

    def test_sentstart2list(self):
        sent_starts = [True, False, False, False, False, False, True, False, False, False, False, False]
        self.assertEqual(parser.sentstart2list(self.doc), sent_starts)

    def test_nested2sentstart(self):
        sent_starts = [True, False, False, False, False, False, True, False, False, False, False, False]
        nest_list = [['Ceci', 'est', 'une', 'première', 'phrase', '.'],
                    ['Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']]
        self.assertEqual(parser.nested2sentstart(nest_list), sent_starts)

    def test_nested2flat(self):
        nest_list = [['Ceci', 'est', 'une', 'première', 'phrase', '.'],
                    ['Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']]
        flat_list = ['Ceci', 'est', 'une', 'première', 'phrase', '.',
                     'Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']
        self.assertEqual(parser.nested2flat(nest_list), flat_list)

    def test_flat2nested(self):
        flat_list = ['Ceci', 'est', 'une', 'première', 'phrase', '.',
                     'Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']
        nest_list = [['Ceci', 'est', 'une', 'première', 'phrase', '.'],
                    ['Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']]
        sent_starts = [True, False, False, False, False, False, True, False, False, False, False, False]
        self.assertEqual(parser.flat2nested(flat_list, sent_starts), nest_list)

    def test_contracted2full(self):
        eg_contracted = ["du", "chat", "des", "chiens", "au", "basket", "aux", "canards"]
        eg_uncontracted = ["de", "le", "chat", "de", "les", "chiens", "à", "le", "basket", "à", "les", "canards"]
        eg_to_align = [[1, 2, 3, 4, 5, 6, 7, 8],
                            ["a", "b", "c", "d", "e", "f", "g", "h"]]
        eg_are_aligned = [[1, 1, 2, 3, 3, 4, 5, 5, 6, 7, 7, 8],
                          ["a", "a", "b", "c", "c", "d", "e", "e", "f", "g", "g", "h"]]

        eg2_contracted = ['Le', 'cousin', 'de', 'mère', 'a', 'cru', 'voir', 'la', 'patte',
                          'du', 'chat', 'au', 'marché', 'du', 'village', '.']
        eg2_spaces = [True, True, True, True, True, True, True, True, True, True, True, True, True, True, False, False]
        eg2_pos = ['DET', 'NOUN', 'ADP', 'NOUN', 'AUX', 'VERB', 'VERB', 'DET', 'NOUN', 'ADP',
                        'NOUN', 'ADP', 'NOUN', 'ADP', 'NOUN', 'PUNCT']
        eg2_uncontracted = ['Le', 'cousin', 'de', 'mère', 'a', 'cru', 'voir', 'la', 'patte',
                            'de', 'le', 'chat', 'à', 'le', 'marché', 'de', 'le', 'village', '.']
        eg2_aligned_spaces = [True, True, True, True, True, True, True, True, True,
                              True, True, True, True, True, True, True, True, False, False]
        eg2_aligned_pos = ['DET', 'NOUN', 'ADP', 'NOUN', 'AUX', 'VERB', 'VERB', 'DET', 'NOUN',
                           'ADP', 'ADP', 'NOUN', 'ADP', 'ADP', 'NOUN', 'ADP', 'ADP', 'NOUN', 'PUNCT']
        # Without alignment
        uncontracted, boolist, _ = parser.contracted2full(eg_contracted)
        self.assertIsInstance(uncontracted, list)
        self.assertIsInstance(boolist, list)
        self.assertIsInstance(boolist[0], bool)
        self.assertEqual(uncontracted, eg_uncontracted)
        self.assertEqual(len(boolist), len(eg_uncontracted))
        # With alignment
        uncontracted, boolist, are_aligned = parser.contracted2full(eg_contracted, eg_to_align)
        self.assertEqual(eg_uncontracted, uncontracted)
        self.assertEqual(eg_are_aligned, are_aligned)
        # eg2 without alignment
        uncontracted, boolist, _ = parser.contracted2full(eg2_contracted)
        self.assertEqual(uncontracted, eg2_uncontracted)
        self.assertEqual(len(boolist), len(eg2_uncontracted))
        # eg2 with alignment
        uncontracted, boolist, are_aligned = parser.contracted2full(eg2_contracted, [eg2_spaces, eg2_pos])
        self.assertEqual(uncontracted, eg2_uncontracted)
        self.assertEqual(len(boolist), len(eg2_uncontracted))
        self.assertEqual(eg2_aligned_spaces, are_aligned[0])
        self.assertEqual(eg2_aligned_pos, are_aligned[1])


    def test_full2contracted(self):
        eg_contracted = ["du", "tennis", "des", "armoires", "au", "aux", "canards"]
        eg_uncontracted = ["de", "le", "tennis", "de", "les", "armoires", "à", "le", "à", "les", "canards"]
        boolist = [True, False, False, True, False, False, True, False, True, False, False]
        eg_to_align = [[1, 1, 2, 3, 3, 4, 5, 5, 7, 7, 8],
                       ["a", "a", "b", "c", "c", "d", "e", "e", "g", "g", "h"]]
        eg_are_aligned = [[1, 2, 3, 4, 5, 7, 8],
                      ["a", "b", "c", "d", "e", "g", "h"]]
        # Without alignment
        contracted, _ = parser.full2contracted(eg_uncontracted, boolist)
        self.assertIsInstance(contracted, list)
        self.assertEqual(eg_contracted, contracted)
        # With alignment
        contracted, are_aligned = parser.full2contracted(eg_uncontracted, boolist, eg_to_align)
        self.assertEqual(eg_contracted, contracted)
        self.assertEqual(eg_are_aligned, are_aligned)

    def test_clone_shape_list(self):
        l1 = ["h", "e", "l", "l", "o"]
        l1_shape = ["_", "_", "_", "_", "_"]
        l2 = [["h", "e", "lll", "l", "ooo"],
              ["h", "eeeee", "l", "l", "o"]]
        l2_shape = [["_", "_", "_", "_", "_"],
                    ["_", "_", "_", "_", "_"]]
        l3 = [["h", "e", "lll", "l", ["h", "e", "l", "l", "o"], "ooo"],
              ["h", ["h", "e", "l", "l", "o"], "eeeee", "l", "l", "o"]]
        l3_shape = [["_", "_", "_", "_",["_", "_", "_", "_", "_"], "_"],
                    ["_", ["_", "_", "_", "_", "_"], "_", "_", "_", "_"]]
        self.assertEqual(l1_shape, parser.clone_shape_list(l1))
        self.assertEqual(l2_shape, parser.clone_shape_list(l2))
        self.assertEqual(l3_shape, parser.clone_shape_list(l3))

    def test_doc2dict(self):
        exp_dict = {
            "doc_key": "document_test",
            "doc_type": "DocumentTest",
            "clusters": [],
            "sentences": [['Ceci', 'est', 'une', 'première', 'phrase', '.'],
                          ['Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']],
            "pos": ['PRON', 'AUX', 'DET', 'ADJ', 'NOUN', 'PUNCT',
                    'ADV', 'PUNCT', 'VERB', 'DET', 'ADJ', 'NOUN'],
            "spaces": [True, True, True, True, False, True,
                       False, True, True, True, True, False],
            "speakers": [['_', '_', '_', '_', '_', '_'], ['_', '_', '_', '_', '_', '_']],
            "other1": "smth else",
            "other2": 123,
            "other3": [1,2,3,4],
            "author": ''
        }
        other = [("other1", "smth else"), ("other2", 123), ("other3", [1,2,3,4])]
        self.doc.user_data = {} # make sure there is nothing there
        parser_dict = parser.doc2dict(self.doc, doc_key="document_test", doc_type="DocumentTest", other=other)
        self.assertEqual(exp_dict, parser_dict)

        # Check if data is correctly taken from doc.user_data
        self.doc.user_data["author"] = "a"
        self.doc.user_data["doc_key"] = "document_test_01"
        self.doc.user_data["doc_type"] = "document test"
        parser_dict = parser.doc2dict(self.doc)
        self.assertEqual("a", parser_dict["author"])
        self.assertEqual("document_test_01", parser_dict["doc_key"])
        self.assertEqual("document test", parser_dict["doc_type"])
        self.doc.user_data = {}  # make sure to empty user_data, for the other tests

        # Check when there is nothing in doc.user_data
        parser_dict = parser.doc2dict(self.doc)
        self.assertEqual("", parser_dict["author"])
        self.assertEqual("", parser_dict["doc_key"])
        self.assertEqual("", parser_dict["doc_type"])

    def test_dict2doc(self):
        with open("eg_data/T01_C1_01.json", "r") as json_file:
            dic_doc = json.load(json_file)
        doc = parser.dict2doc(dic_doc, self.nlp.vocab)
        pos = ["NOUN", "ADP", "PROPN", "PROPN", "PUNCT", "PUNCT", "PROPN", "PUNCT", "VERB", "AUX", "VERB", "DET", "NOUN", "DET", "NOUN", "VERB", "ADP", "DET", "NOUN", "ADP", "DET", "NOUN", "VERB", "SCONJ", "PRON", "ADV", "AUX", "ADV", "VERB", "DET", "NOUN", "ADJ", "ADJ", "ADP", "PROPN", "PUNCT", "PROPN", "PUNCT", "ADV", "ADP", "DET", "NOUN", "ADP", "DET", "NOUN", "PUNCT", "DET", "PRON", "ADP", "DET", "NUM", "NOUN", "ADP", "DET", "NOUN", "VERB", "DET", "NUM", "NOUN", "ADV", "ADP", "DET", "NOUN", "ADJ", "ADP", "NOUN", "ADP", "PROPN", "PROPN", "CCONJ", "ADP", "NUM", "ADJ", "NOUN", "AUX", "VERB", "ADV", "AUX", "VERB", "DET", "ADJ", "NOUN", "PUNCT", "ADP", "DET", "NOUN", "VERB", "ADP", "DET", "NOUN", "PUNCT", "DET", "NOUN", "PUNCT", "VERB", "SCONJ", "ADP", "DET", "NOUN", "PRON", "ADV", "AUX", "VERB", "DET", "NOUN", "PUNCT", "CCONJ", "SCONJ", "ADJ", "DET", "NOUN", "VERB", "SCONJ", "DET", "NOUN", "ADP", "DET", "NUM", "NOUN", "AUX", "DET", "NOUN", "ADP", "DET", "NOUN", "ADP", "NOUN", "ADJ", "VERB", "PUNCT", "ADV", "ADP", "DET", "NOUN", "ADP", "DET", "NOUN", "ADJ", "PUNCT", "PUNCT", "AUX", "VERB", "DET", "NOUN", "PUNCT", "DET", "NOUN", "ADP", "NOUN", "ADP", "DET", "NOUN", "PUNCT", "VERB", "ADP", "DET", "NOUN", "ADJ", "ADP", "DET", "PUNCT", "NOUN", "ADP", "NOUN", "ADJ", "ADJ", "PUNCT", "PUNCT", "AUX", "VERB", "ADP", "VERB", "DET", "NUM", "ADJ", "NOUN", "ADP", "DET", "NOUN", "VERB", "ADP", "VERB", "DET", "NOUN", "PUNCT", "PROPN", "PROPN", "CCONJ", "PROPN", "PROPN", "PUNCT", "ADV", "PUNCT", "PROPN", "PROPN", "PUNCT", "PUNCT", "PUNCT", "DET", "NOUN", "ADP", "DET", "NUM", "NOUN", "ADP", "NOUN", "PUNCT", "DET", "NOUN", "NUM", "NOUN", "PUNCT", "AUX", "AUX", "DET", "NOUN", "ADJ", "ADP", "DET", "NOUN", "ADJ", "ADP", "PROPN", "CCONJ", "ADP", "PROPN", "PRON", "AUX", "VERB", "DET", "NOUN", "ADJ", "CCONJ", "DET", "NOUN", "PRON", "PRON", "AUX", "AUX", "VERB", "PUNCT", "PUNCT", "AUX", "VERB", "DET", "NOUN", "ADP", "DET", "NOUN", "VERB", "ADP", "DET", "NUM", "NOUN", "PUNCT", "DET", "NUM", "NOUN", "PUNCT", "PRON", "AUX", "ADV", "VERB", "AUX", "VERB", "ADP", "DET", "NOUN", "ADP", "DET", "NOUN", "ADJ", "PUNCT", "AUX", "ADV", "VERB", "DET", "NOUN", "ADP", "NOUN", "ADP", "NOUN", "ADP", "DET", "PROPN", "PUNCT", "PRON", "PRON", "VERB", "ADP", "DET", "NOUN", "ADP", "DET", "NUM", "NOUN", "ADJ", "PROPN", "PROPN", "PUNCT", "PROPN", "PROPN", "CCONJ", "PROPN", "PROPN", "PUNCT", "VERB", "ADP", "NUM", "NOUN", "CCONJ", "NUM", "NOUN", "PUNCT", "DET", "NOUN", "ADJ", "ADP", "DET", "NOUN", "PROPN", "PROPN", "PROPN", "AUX", "VERB", "NOUN", "NUM", "NOUN", "PUNCT", "SCONJ", "NOUN", "ADP", "VERB", "DET", "NOUN", "ADP", "NOUN", "PUNCT", "DET", "NOUN", "VERB", "DET", "ADJ", "NOUN", "PUNCT", "PUNCT"]
        self.assertEqual([tok.pos_ for tok in doc], pos)
        self.assertEqual(len(list(doc.sents)), len(dic_doc.get('sentences', [])))
        self.assertNotEqual(doc.user_data.get("title"), None)

    def test_other(self):
        print("WARNING: parser not fully implemented, fill in unittesting when more advanced in progra")


if __name__ == '__main__':
    unittest.main()
