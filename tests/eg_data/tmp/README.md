This mini corpus used for basic testing are examples taken from RPM2 corpus for multi document summarization :



Loupy, Claude de, Marie Guégan, Christelle Ayache, Somara Seng, and Juan-Manuel Torres Moreno. 2010. “A French Human Reference Corpus for Multi-Document Summarization and Sentence Compression.” In *Proceedings of the Seventh International Conference on Language Resources and Evaluation (LREC’10)*. Valletta, Malta: European Language Resources Association (ELRA). http://www.lrec-conf.org/proceedings/lrec2010/pdf/919_Paper.pdf.
