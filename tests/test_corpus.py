import json
import unittest
import spacy

import atsch
import atsch.parser as parser
from atsch.summarizers.leadn_baseline import LeadN
from atsch.summarizers.random_baseline import Random
from spacy.tokens import Doc
import os
import shutil
from pathlib import Path
from atsch.summarizers.centroid import CentroidSummarizer
from atsch.summarizers.centroid_corefed_selection import CentroidSummarizerCorefedSelection


class TestCorpus(unittest.TestCase):

    # copy corpus examples to tmp
    os.chdir("eg_data")
    try:
        shutil.rmtree('tmp')  # try to remove tmp folder if it exists, to start anew.
    except:
        pass
    shutil.copytree("corpus_example", "tmp")

    # current working dir = tmp
    os.chdir("tmp")

    corpus = atsch.Corpus()
    nlp = spacy.blank("fr")

    def test_get_docs(self):
        docs = self.corpus.get_list_docs()
        self.assertIsInstance(docs, list)
        self.assertEqual(4, len(docs))

    def test_get_ref_sums(self):
        docs = self.corpus.get_list_ref_sums()
        self.assertIsInstance(docs, list)
        self.assertEqual(4, len(docs))

    def test_summarize_1_baseline(self):
        raise NotImplementedError

    def test_summarize_baseline_lead3_dry(self):
        summarizer = LeadN(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_lead3",
                              to_disk=False,
                              evaluate=False,
                              gen_stat=False
                              )

    def test_summarize_baseline_lead3_to_disk(self):
        summarizer = LeadN(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_lead3",
                              to_disk=True,
                              evaluate=False,
                              gen_stat=False
                              )

    def test_summarize_baseline_lead3_evaluate(self):
        summarizer = LeadN(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_lead3",
                              to_disk=False,
                              evaluate=True,
                              gen_stat=False
                              )

    def test_summarize_baseline_lead3_evaluate_to_disk(self):
        summarizer = LeadN(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_lead3",
                              to_disk=True,
                              evaluate=True,
                              gen_stat=False
                              )

    def test_summarize_baseline_lead3_gen_stats(self):
        summarizer = LeadN(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_lead3",
                              to_disk=False,
                              evaluate=False,
                              gen_stat=True
                              )

    def test_summarize_baseline_lead3_gen_stats_to_disk(self):
        summarizer = LeadN(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_lead3",
                              to_disk=True,
                              evaluate=False,
                              gen_stat=True
                              )

    def test_summarize_baseline_lead3_all_true(self):
        summarizer = LeadN(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_lead3",
                              to_disk=True,
                              evaluate=False,
                              gen_stat=True
                              )

    def test_baseline_random(self):
        summarizer = Random(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_random3",
                              to_disk=True,
                              evaluate=False,
                              gen_stat=False
                              )

    def test_baseline_random_evaluate(self):
        summarizer = Random(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_random3",
                              to_disk=False,
                              evaluate=True,
                              gen_stat=False
                              )

    def test_baseline_random_gen_stats(self):
        summarizer = Random(n=3)
        self.corpus.summarize(summarizer=summarizer,
                              nlp=self.nlp,
                              summarization_id="test_baseline_random3",
                              to_disk=False,
                              evaluate=False,
                              gen_stat=True
                              )

    def test_baseline_random_gen_coref_coref_dry(self):
        summarizer = Random(n=5)
        nlp = spacy.load("fr_dep_news_trf")
        nlp.add_pipe("tensor2attr")
        self.corpus.summarize(summarizer=summarizer,
                              nlp=nlp,
                              to_disk=True,
                              evaluate=True,
                              gen_stat=True,
                              resolve_coref=True,
                              resolution_window=0,
                              replace_first_mention=False,
                              no_coref_dont_keep=True
                              )

    def test_baseline_oracle(self):
        raise NotImplementedError

    def test_evaluate_corpus(self):
        raise NotImplementedError

    def test_get_stats(self):
        raise NotImplementedError

    def test_summarize_centroid(self):
        summarizer = CentroidSummarizer(n=5)
        nlp = spacy.load("fr_dep_news_trf")
        nlp.add_pipe("tensor2attr")
        nlp.select_pipes(enable=["transformer", "tensor2attr"])
        self.corpus.summarize(summarizer=summarizer,
                              nlp=nlp,
                              summarization_id="test_simple_centroid",
                              to_disk=True,
                              evaluate=False,
                              gen_stat=False,
                              )

    def test_summarize_centroid_all(self):
        summarizer = CentroidSummarizer(n=5, tf_idf_threshold=0)
        nlp = spacy.load("fr_dep_news_trf")
        nlp.add_pipe("tensor2attr")
        nlp.select_pipes(enable=["transformer", "tensor2attr"])
        self.corpus.summarize(summarizer=summarizer,
                              nlp=nlp,
                              summarization_id="test_simple_centroid",
                              to_disk=True,
                              evaluate=True,
                              gen_stat=True
                              )

    def test_summarize_centroid_coref_selection(self):
        summarizer = CentroidSummarizerCorefedSelection(n=5)
        nlp = spacy.load("fr_dep_news_trf")
        nlp.add_pipe("tensor2attr")
        nlp.select_pipes(enable=["transformer", "tensor2attr"])
        self.corpus.summarize(summarizer=summarizer,
                              nlp=nlp,
                              summarization_id="test_centroid_selection_corefed",
                              to_disk=True,
                              evaluate=True,
                              gen_stat=True
                              )

    def test_summarize_one_centroid_simple(self):
        nlp = spacy.load("fr_dep_news_trf")
        nlp.add_pipe("tensor2attr")
        summarizer = CentroidSummarizer(n=5)
        with open("../T01_C1_D01.json", "r") as json_file:
            doc = parser.dict2doc(json.load(json_file), nlp.vocab)
        for _, pipe in nlp.pipeline:
            pipe(doc)
        self.corpus.summarize_one(doc,
                                  summarizer=summarizer,
                                  summarization_id="centroid_simple_test",
                                  to_disk=True,
                                  evaluate=False,
                                  gen_stat=False)

    def test_gen_form_data(self):
        prev_wd = os.getcwd()
        os.chdir("/home/dorian/memoire/Corpus/txt/Corpus_RPM2_resumes/preprocessed_corpus")
        corpus = atsch.Corpus()
        form_data, form_meta = corpus.gen_forms_data(["Centroid_simple-20210808-13:35:33",
                                                      "Centroid_selection_with_coreference-20210808-13:54:06"],
                                                     target_number=40)
        len(form_data)
        os.chdir(prev_wd)



if __name__ == '__main__':
    # run the tests
    unittest.main()

    # corpus.summarize(summarizer, nlp, to_disk=False, evaluate=False, gen_stat=False)
    # corpus.summarize(summarizer, nlp, to_disk=False, evaluate=True, gen_stat=False)
    # corpus.summarize(summarizer, nlp, to_disk=False, evaluate=False, gen_stat=True)
    # corpus.summarize(summarizer, nlp, to_disk=True, evaluate=True, gen_stat=True)