import unittest
from atsch import StatFactory
import atsch.scripts.trf2vec_pipeline
import pandas as pd
import fr_dep_news_trf
from spacy.tokens.doc import Doc


class TestStatFactory(unittest.TestCase):
    nlp = fr_dep_news_trf.load()
    nlp.add_pipe("tensor2attr")

    # pre-sentencized and tokenized doc
    text = ['Ceci', 'est', 'une', 'première', 'phrase', '.', 'Ensuite', ',', 'voici', 'une', 'deuxième', 'phrase']
    spaces = [True, True, True, True, False, True, False, True, True, True, True, False]
    sents = [True, False, False, False, False, False, True, False, False, False, False, False]
    doc1 = Doc(nlp.vocab, words=text, spaces=spaces, sent_starts=sents)

    # Seconde doc with automatique tokenization
    doc2 = nlp("Voici un deuxième exemple en français.")

    # process the doc1 with the rest of the pretrained pipeline
    for pipe_name, pipe in nlp.pipeline:
        doc1 = pipe(doc1)

    try:
        stat_fact = StatFactory()
        stat_fact.add_doc(doc1,
                          nlp,
                          "01",
                          "Generated Summary")
        stat_fact.add_doc(doc2,
                          nlp,
                          "02",
                          "Generated Summary")
    except:
        stat_fact = None

    def test_create_statfact(self):
        stat_fact = StatFactory()
        self.assertIsInstance(stat_fact.data, pd.DataFrame, 'StatFactory.data not a DataFrame')
        self.assertEqual(len(stat_fact.data), 0, "The DataFrame is not size 0")

    def test_add_docs_size(self):
        stat_fact = StatFactory()
        stat_fact.add_doc(self.doc1,
                          self.nlp,
                          "01",
                          "Generated Summary",
                          )
        self.assertEqual(len(stat_fact.data), 1, "The DataFrame is not size 1")
        stat_fact.add_doc(self.doc2,
                          self.nlp,
                          "02",
                          "Generated Summary")
        self.assertEqual(len(stat_fact.data), 2, "The DataFrame is not size 2")
        return stat_fact

    def test_text_sha256sum(self):
        doc1_sha = "4e909331d9b86198cbd94ab8bc5bd8784a5874bff0cd2b97d7e00e279f6b88f0"
        self.assertEqual(self.stat_fact.data["text_sha256sum"][0],
                         doc1_sha,
                         "The SHA256SUM is incorrect")

    def test_spacy_model(self):
        model_meta = {'lang': 'fr',
                      'name': 'dep_news_trf',
                      'version': '3.0.0',
                      'spacy_version': '>=3.0.0,<3.1.0',
                      'author': 'Explosion'}
        self.assertIsInstance(self.stat_fact.data["spacy_model"][0], dict)
        self.assertEqual(self.stat_fact.data["spacy_model"][0], model_meta)
        self.assertEqual(self.stat_fact.data["spacy_model"][1], model_meta)

    def test_vector_dimesions(self):
        self.assertEqual(self.stat_fact.data["vector_dimensions"][0], 768)

    def test_nbr_chars_w_spaces(self):
        self.assertEqual(self.stat_fact.data["nbr_chars_w_spaces"][0], 64)

    def test_nbr_chars_wo_spaces(self):
        self.assertEqual(self.stat_fact.data["nbr_chars_wo_spaces"][0], 55)

    def test_nbr_tokens_w_punct(self):
        self.assertEqual(self.stat_fact.data["nbr_tokens_w_punct"][0], 12)

    def test_nbr_tokens_wo_punct(self):
        self.assertEqual(self.stat_fact.data["nbr_tokens_wo_punct"][0], 10)

    def test_nbr_types(self):
        self.assertEqual(self.stat_fact.data["nbr_types_wo_punct"][0], 8)

    def test_nbr_lemmas(self):
        self.assertEqual(self.stat_fact.data["nbr_lemmas_wo_punct"][0], 8)

    def test_nbr_sentences(self):
        self.assertEqual(self.stat_fact.data["nbr_sentences"][0], 2)

    def test_nbr_token_wo_punct_per_sent(self):
        self.assertEqual(self.stat_fact.data["nbr_token_wo_punct_per_sent"][0], [5, 5])

    def test_avg_nbr_token_per_sent(self):
        self.assertEqual(self.stat_fact.data["avg_nbr_token_per_sent"][0], 5)

    def test_std_nbr_token_per_sent(self):
        self.assertEqual(self.stat_fact.data["std_nbr_token_per_sent"][0], 0)

    def test_nbr_chars_per_sent(self):
        self.assertEqual(self.stat_fact.data["nbr_chars_per_sent_spaces_punct"][0], [29, 34])

    def test_avg_nbr_chars_per_sent(self):
        self.assertEqual(self.stat_fact.data["avg_nbr_chars_per_sent_spaces_punct"][0], 31.5)

    def test_std_nbr_chars_per_sent(self):
        self.assertEqual(self.stat_fact.data["std_nbr_chars_per_sent_spaces_punct"][0], 2.5)


if __name__ == '__main__':
    unittest.main()
