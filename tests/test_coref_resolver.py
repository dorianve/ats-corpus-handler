import unittest
import spacy
from spacy.tokens import Doc, Span
import atsch.parser as parser
from atsch.coref_resolver import CorefResolver
import json


class TestCoref(unittest.TestCase):
    # Simple example
    nlp_trf = spacy.load("fr_dep_news_trf")
    nlp = spacy.blank('fr')
    doc_text = [["Ceci", "est", "un", "exemple", "."], ["Il", "est", "parlant", "!"]]
    spaces = [True, True, True, False, True, True, True, True, False]
    doc = Doc(nlp.vocab, words=parser.nested2flat(doc_text),
              spaces=spaces, sent_starts=parser.nested2sentstart(doc_text))
    doc.user_data["coref_chains"] = [[[0, 0], [2, 3], [5, 5]]]

    def test_cofr2spans(self):
        expected = [["Ceci", "un exemple", "Il"]]

        self.assertIsInstance(self.doc._.get_coref_chains(), list)
        for chain in self.doc._.get_coref_chains():
            for span in chain:
                self.assertIsInstance(span, Span)
        self.assertEqual(1, len(self.doc._.get_coref_chains()))
        self.assertEqual(3, len(self.doc._.get_coref_chains()[0]))
        self.assertEqual(expected, [[span.text for span in chain] for chain in self.doc._.get_coref_chains()],
                         msg="Chains 2 Chains Spans failed")

    def test_corefed_rpz(self):
        expected = ["Ceci", "un", "exemple", "Il", "est", "Ceci", "un", "exemple", "Il", ".",
                    "Ceci", "un", "exemple", "Il", "est", "parlant", "!"]

        self.assertIsInstance(self.doc._.get_coref_doc_rep(), Doc)
        self.assertEqual(expected, [tok.text for tok in self.doc._.get_coref_doc_rep()])

    def test_nested_coref_spans(self):
        # Example with nested coreference chains
        doc2_text = [
            ["Kevin", ",", "je", "t'", "ai", "vu", "avec", "le", "corbeau", "de", "ta", "tante", ",", "Joseline", "."],
            ["Il", "avait", "de", "belles", "plumes", "noires", "qui", "étaient", "mouillées", "."]
        ]
        spaces = [False, True, True, True, True, True, True, True, True, True, True, False, True, False, True,
                  True, True, True, True, True, True, True, True, False, False]
        doc2_cofr = [
            [[0, 0], [3, 3], [10, 10]],  # Kevin
            [[10, 11], [13, 13]],  # Joseline
            [[7, 8], [15, 15]],  # Le corbeau
            [[17, 20], [21, 21]]  # Les plumes
        ]
        doc2_span_chains = [
            ["Kevin", "t'", "ta"],
            ["ta tante", "Joseline"],
            ["le corbeau", "Il"],
            ["de belles plumes noires", "qui"]
        ]
        doc2_sent_starts = parser.nested2sentstart(doc2_text)
        doc2 = Doc(self.nlp.vocab, words=parser.nested2flat(doc2_text),
                   sent_starts=doc2_sent_starts, spaces=spaces)
        doc2.user_data["coref_chains"] = doc2_cofr
        self.assertEqual(doc2_span_chains, [[span.text for span in chain] for chain in doc2._.get_coref_chains()])

    def test_dict_doc_auto_extensions(self):
        """ Test wether an automatically deserialized doc has all parameters automatically
        set for coreference resolution"""
        with open("eg_data/T01_C1_D01.json", "r") as json_file:
            dic_doc = json.load(json_file)
            doc = parser.dict2doc(dic_doc, self.nlp.vocab)
        # Test Doc level custom extensions
        self.assertGreaterEqual(len(doc._.get_coref_chains()), 1)
        self.assertIsInstance(doc._.get_coref_doc_rep(), Doc)
        # Test Span level custom extensions
        span1 = doc._.get_coref_chains()[0][0]
        span2 = doc._.get_coref_chains()[0][1]
        span3 = doc._.get_coref_chains()[1][1]
        self.assertEqual(span1._.mention_id, (0, 0))
        self.assertEqual(span2._.mention_id, (0, 1))
        self.assertEqual(span3._.mention_id, (1, 1))

    def test_token_extensions(self):
        # Token._.coref_spans
        tok0_spans = [span.text for span in self.doc[0]._.coref_spans]
        # Token._.sent_i
        self.assertEqual(tok0_spans, ["Ceci"])
        self.assertEqual(self.doc[0]._.sent_i, 0)
        self.assertEqual(self.doc[6]._.sent_i, 1)
        # Token._.is_uncontracted_article
        with open("eg_data/T01_C1_D01.json", "r") as json_file:
            dic_doc = json.load(json_file)
            doc = parser.dict2doc(dic_doc, self.nlp.vocab)
        self.assertEqual([tok._.is_uncontracted_article for tok in doc], dic_doc.get("is_uncontracted_article"))
        # Token._.mention_id, test with nested case
        #   Example with nested coreference chains
        doc2_text = [
            ["Kevin", ",", "je", "t'", "ai", "vu", "avec", "le", "corbeau", "de", "ta", "tante", ",", "Joseline", "."],
            ["Il", "avait", "de", "belles", "plumes", "noires", "qui", "étaient", "mouillées", "."]
        ]
        spaces = [False, True, True, True, True, True, True, True, True, True, True, False, True, False, True,
                  True, True, True, True, True, True, True, True, False, False]
        #  The list here is not ordered by apparition, to test if Token._.mention_id is ordered by length of Span
        doc2_cofr = [
            [[10, 11], [13, 13]],  # Joseline
            [[0, 0], [3, 3], [10, 10]],  # Kevin
            [[7, 8], [15, 15]],  # Le corbeau
            [[17, 20], [21, 21]]  # Les plumes
        ]
        doc2_sent_starts = parser.nested2sentstart(doc2_text)
        doc2 = Doc(self.nlp.vocab, words=parser.nested2flat(doc2_text),
                   sent_starts=doc2_sent_starts, spaces=spaces)
        doc2.user_data["coref_chains"] = doc2_cofr
        self.assertEqual([(1, 2), (0, 0)], doc2[10]._.mention_id)
        # Token._.coref_chains, we can access the chains with this extension, ordered by length of Span
        expected = [
            ["Kevin", "t'", "ta"],
            ["ta tante", "Joseline"],
        ]
        actual = [[span.text for span in chain] for chain in doc2[10]._.coref_chains]
        self.assertEqual(expected, actual)

    def test_rule_a_empty_coref(self):
        doc_text = ["voici", "un", "exemple", "bateau"]
        doc_sents = [True, False, False, False]
        nlp = spacy.blank('fr')
        doc = Doc(nlp.vocab, words=doc_text, sent_starts=doc_sents)
        doc._.nlp = nlp
        doc.user_data["coref_chains"] = []
        resolver = CorefResolver(doc, target_is_summary=False, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(doc_text, [token.text for token in corefed_doc])

    def test_rule_a_no_noun(self):
        doc_text = [["Ceci", "qui", "que", "."],["Phrase", "interne"], ["Voici", "un", "exemple", "sans", "mention", "nominale"]]
        doc_coref = [[[0,0],[1,1],[6,6]]]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        resolver = CorefResolver(doc, target_is_summary=False, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        # We expect the same result as input, because there are no nominal mentions
        self.assertEqual(doc_text, [[token.text for token in sent] for sent in corefed_doc.sents])

    def test_rule_abis_summary_window1(self):
        doc_text = [["Ceci", "est", "un", "avertissement", "."], ["Un", "exemple", "est", "nécessaire", "."]]
        doc_coref = [[[2, 3], [5,6]]]
        expected = ["Un", "avertissement", "est", "nécessaire", "."]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        doc._.summary = [list(doc.sents)[-1]]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [tok.text for tok in corefed_doc])
        self.assertIn("Abis", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_e_summary_no_window(self):
        doc_text = [["Ceci", "est", "un", "avertissement", "."], ["Il", "est", "nécessaire", "."]]
        doc_coref = [[[2, 3], [5,5]]]
        expected = ["Un", "avertissement", "est", "nécessaire", "."]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        doc._.summary = [list(doc.sents)[-1]]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=-1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [tok.text for tok in corefed_doc])
        self.assertIn("E", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_b_01(self):
        doc_text = [["Les", "vendeurs", "."], ["phrase", "entre","."], ["Ces", "derniers", "étaient", "sympas", "."]]
        doc_coref = [[[0,1],[6,7]]]
        expected = [["Les", "vendeurs", "."], ["phrase", "entre","."], ["Les", "vendeurs", "étaient", "sympas", "."]]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        # resolve coreference
        resolver = CorefResolver(doc, target_is_summary=False, resolution_window=1, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [[token.text for token in sent] for sent in corefed_doc.sents])
        self.assertIn("B", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_b_02(self):
        doc_text = [["Les", "vendeurs", "."], ["phrase", "entre", "."], ["Ces", "derniers"]]
        doc_coref = [[[0, 1], [6, 7]]]
        expected = [["Les", "vendeurs", "."], ["phrase", "entre", "."], ["Les", "vendeurs"]]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        # resolve coreference
        resolver = CorefResolver(doc, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [[token.text for token in sent] for sent in corefed_doc.sents])
        self.assertIn("B", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_b_0_window(self):
        doc_text = [["Les", "vendeurs", "."], ["Ces", "autruches", ",", "ces", "derniers"]]
        doc_coref = [[[0, 1], [3, 4], [6,7]]]
        expected = [["Les", "vendeurs", "."], ["Les", "vendeurs", ",", "ces", "derniers"]]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        # resolve coreference
        resolver = CorefResolver(doc, target_is_summary=False, resolution_window=0, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [[token.text for token in sent] for sent in corefed_doc.sents])
        self.assertIn("B", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_b_no_window(self):
        doc_text = [["Les", "vendeurs", "."], ["Ces", "autruches", ",", "ces", "derniers"]]
        doc_coref = [[[0, 1], [3, 4], [6,7]]]
        expected = [["Les", "vendeurs", "."], ["Les", "vendeurs", ",", "les", "vendeurs"]]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        # resolve coreference
        resolver = CorefResolver(doc, target_is_summary=False, resolution_window=-1, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [[token.text for token in sent] for sent in corefed_doc.sents])
        self.assertIn("B", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_c_simple(self):
        doc_text = [["Henri"],["Son", "chat"]]
        doc_coref = [[[0, 0], [1, 1]]]
        expected = ["Le", "chat", "de", "Henri"]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        doc._.summary = [list(doc.sents)[-1]]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("C", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_c_longer(self):
        doc_text = [["Henri"],
                    ["Son", "grand", "manteau", "à", "motifs", "jaunes", ",",  "verts", "et", "bleus", "est",  "moche", "."]]
        doc_coref = [[[0, 0], [1, 1]]]
        expected = ["Le", "grand", "manteau", "à", "motifs", "jaunes", ",",  "verts", "et", "bleus", "de", "Henri", "est",  "moche", "."]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        doc._.summary = [list(doc.sents)[-1]]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("C", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_c_de(self):
        doc_text = [["Henri"],
                    ["De", "son", "domicile", ",", "on", "n'", "a", "pas", "une", "belle", "vue"]]
        doc_coref = [[[0, 0], [2, 2]]]
        expected = ["De", "le", "domicile", "de", "Henri", ",", "on", "n'", "a", "pas", "une", "belle", "vue"]
        doc = Doc(self.nlp_trf.vocab, words=parser.nested2flat(doc_text), sent_starts=parser.nested2sentstart(doc_text))
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        doc._.summary = [list(doc.sents)[-1]]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("C", set(parser.nested2flat(resolver.stats_visited_nodes)))
        self.assertTrue(corefed_doc[0]._.is_uncontracted_article)

    def test_rule_d_past(self):
        doc_text = [["Jean", "Benoit"],
                    ["«", "j'", "aime", "faire", "des", "tests", "»", "a", "-t", "-il", "indiqué", "."]]
        spaces= [True, True, True, False, True, True, True, True, True, False, False, True, False, False]
        # Note : spacing is important, otherwise parser is not sure what is the function of "-il" in the
        #  sentence and mark it as dep ("unclassified dependent" instead of "nsubj")
        doc_coref = [[[0, 1], [11, 11]]]
        expected = ["«", "j'", "aime", "faire", "des", "tests", "»", "a", "indiqué", "Jean", "Benoit", "."]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  spaces=spaces)
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        doc._.summary = [list(doc.sents)[-1]]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=0, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("D", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_d_present(self):
        doc_text = [["Jean", "Benoit"],
                    ["«", "j'", "aime", "faire", "des", "tests", "»", "indique", "-t", "-il", "."]]
        spaces = [True, True, True, False, True, True, True, True, True, False, False, False, False]
        doc_coref = [[[0, 1], [11, 11]]]
        expected = ["«", "j'", "aime", "faire", "des", "tests", "»", "indique", "Jean", "Benoit", "."]
        pos = ['PROPN', 'PROPN', 'PUNCT', 'PRON', 'VERB', 'VERB', 'DET', 'NOUN', 'PUNCT', 'VERB', 'PART', 'PRON', 'PUNCT']
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  spaces=spaces,
                  pos=pos
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        # Spacy has trouble identifying the correct "nsubj" in the above sentence (thinks it's "-t" instead of
        #  "-il"), so help just a bit, to make sure the rules work fine with the correct parsing
        doc[10].dep_ = "dep"
        doc[11].dep_ = "nsubj"
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("D", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_e(self):
        doc_text = [["Jean", "Benoit"],
                    ["Quand", "il", "viendra", ",", "dites", "«", "bonjour", "»", "."]]
        doc_coref = [[[0, 1], [3, 3]]]
        expected = ["Quand","Jean", "Benoit", "viendra", ",", "dites", "«", "bonjour", "»", "."]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("E", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_ebis_estce(self):
        doc_text = [["Jean", "Benoit"],
                    ["Est", "-ce", "qu'", "il", "viendra", "?"]]
        doc_coref = [[[0, 1], [5, 5]]]
        expected = ["Est", "-ce", "que", "Jean", "Benoit", "viendra", "?"]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("Ebis", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_ebis(self):
        doc_text = [["Jean", "Benoit"],
                    ["Je", "pense", "qu'", "il", "viendra", "."]]
        doc_coref = [[[0, 1], [5, 5]]]
        expected = ["Je", "pense", "que", "Jean", "Benoit", "viendra", "."]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("Ebis", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_f_no_t(self):
        doc_text = [["Jean", "Benoit"],
                    ["Est", "-il", "blond", "?"]]
        doc_coref = [[[0, 1], [3, 3]]]
        spaces = [True, False, False, True, True, False]
        # In the future, cass will be changed
        expected = ["Jean", "Benoit", "Est", "-il", "blond", "?"]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  spaces=spaces
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        doc[2].pos_ = "VERB"
        doc[3].dep_ = 'nsubj'
        for tok in doc:
            print(f"{tok.i}\t{tok.text}\t{tok.pos_}\t{tok.dep_}\t{tok.morph}")
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        for tok, nodes in zip(corefed_doc, resolver.stats_visited_nodes):
            print(f"{tok.i}\t{tok.text}\t{tok.pos_}\t{tok.dep_}\t{tok.morph}\t{nodes}")

        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("F", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_f_t_euphonique(self):
        doc_text = [["Jean", "Benoit"],
                    ["A", "-t", "-il", "eu", "froid", "?"]]
        doc_coref = [[[0, 1], [4, 4]]]
        # In the future, cass will be adapted
        expected = ["Jean", "Benoit", "A", "-t", "-il", "eu", "froid", "?"]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("F", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_g(self):
        doc_text = [["Jean", "Benoit"],
                    ["Le", "criminel", "l'", "avait", "menacé"]]
        doc_coref = [[[0, 1], [4, 4]]]
        # In the future, cass will be adapted
        expected = ["Le", "criminel", "avait", "menacé", "Jean", "Benoit"]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("G", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_h(self):
        doc_text = [["Jean", "Benoit"],
                    ["Il", "lui", "a", "offert", "des", "fleurs", "."]]
        doc_coref = [[[0, 1], [3, 3]]]
        # In the future, cass will be adapted
        expected = ["Il", "a", "offert", "des", "fleurs", "à", "Jean", "Benoit", "."]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        print("ORIGINAL")
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("H", set(parser.nested2flat(resolver.stats_visited_nodes)))

    def test_rule_h_uncontracted(self):
        doc_text = [["les", "gens"],
                    ["Il", "leur", "a", "offert", "du", "pain", "."]]
        doc_coref = [[[0, 1], [3, 3]]]
        # In the future, cass will be adapted
        expected = ["Il", "a", "offert", "du", "pain", "à", "les", "gens", "."]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("H", set(parser.nested2flat(resolver.stats_visited_nodes)))
        self.assertTrue(corefed_doc[5]._.is_uncontracted_article)

    def test_rule_i(self):
        doc_text = [["les", "gens"],
                    ["Je", "le", "leur", "ai", "donné", "."]]
        doc_coref = [[[0, 1], [4, 4]]]
        # In the future, cass will be adapted
        expected = ["Je", "l'", "ai", "donné", "à", "les", "gens", "."]
        doc = Doc(self.nlp_trf.vocab,
                  words=parser.nested2flat(doc_text),
                  sent_starts=parser.nested2sentstart(doc_text),
                  )
        doc.user_data["coref_chains"] = doc_coref
        doc._.nlp = self.nlp_trf
        # Process the whole doc
        doc._.summary = [list(doc.sents)[-1]]
        for _, pipe in self.nlp_trf.pipeline:
            pipe(doc)
        # tagging issue with spacy, correct that to check rule
        doc[4].dep_ = "iobj"
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=1, replace_first_mention=True)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for sent in corefed_doc.sents for token in sent])
        self.assertIn("I", set(parser.nested2flat(resolver.stats_visited_nodes)))
        self.assertTrue(corefed_doc[4]._.is_uncontracted_article)

    def test_additive_rules_BCD(self):
        words = ['les', 'voisins', 'les', 'inconnus', 'Jean', '.', '«', 'Ces', 'derniers', 'leur', 'ont', 'vendu', 'sa', 'nintendo', 'et', 'je', 'pense', "qu'", 'il', 'ne', 'les', 'pardonera', 'pas', '»', 'a', '-t', '-il', 'ajouté', '.', 'Mais', 'a', '-t', '-il', 'raison', '?', 'Je', 'pense', 'que', 'oui', ',', 'ils', "l'", 'ont', 'humilié', 'et', 'lui', 'ont', 'mis', 'un', 'coup', 'au', 'moral', '.']
        heads = [1, 1, 3, 1, 1, 5, 11, 8, 11, 11, 11, 11, 13, 11, 16, 16, 11, 21, 21, 21, 21, 16, 21, 11, 27, 26, 27, 27, 27, 30, 30, 32, 30, 30, 30, 36, 36, 43, 43, 43, 43, 43, 43, 36, 47, 47, 47, 43, 49, 47, 51, 47, 36]
        deps = ['det', 'ROOT', 'det', 'ROOT', 'nmod', 'ROOT', 'punct', 'det', 'nsubj', 'iobj', 'aux:tense', 'ROOT', 'det', 'obj', 'cc', 'nsubj', 'conj', 'mark', 'nsubj', 'advmod', 'obj', 'ccomp', 'advmod', 'punct', 'aux:tense', 'dep', 'nsubj', 'ROOT', 'punct', 'cc', 'ROOT', 'dep', 'nsubj', 'dep', 'punct', 'nsubj', 'ROOT', 'mark', 'advmod', 'punct', 'nsubj', 'obj', 'aux:tense', 'ccomp', 'cc', 'iobj', 'aux:tense', 'conj', 'det', 'obj', 'case', 'obl:arg', 'punct']
        lemmas = ['le', 'voisin', 'le', 'inconnu', 'Jean', '.', '«', 'ce', 'dernier', 'leur', 'avoir', 'vendre', 'son', 'nintendo', 'et', 'je', 'pense', 'que', 'il', 'ne', 'le', 'pardoner', 'pas', '»', 'avoir', '-t', 'il', 'ajouter', '.', 'mais', 'avoir', '-t', 'il', 'raison', '?', 'je', 'pense', 'que', 'oui', ',', 'il', "l'", 'avoir', 'humilier', 'et', 'lui', 'avoir', 'mettre', 'un', 'coup', 'au', 'moral', '.']
        pos = ['DET', 'NOUN', 'DET', 'NOUN', 'PROPN', 'PUNCT', 'PUNCT', 'DET', 'ADJ', 'PRON', 'AUX', 'VERB', 'DET', 'NOUN', 'CCONJ', 'PRON', 'VERB', 'SCONJ', 'PRON', 'ADV', 'PRON', 'VERB', 'ADV', 'PUNCT', 'AUX', 'PART', 'PRON', 'VERB', 'PUNCT', 'CCONJ', 'VERB', 'PART', 'PRON', 'NOUN', 'PUNCT', 'PRON', 'VERB', 'SCONJ', 'ADV', 'PUNCT', 'PRON', 'PRON', 'AUX', 'VERB', 'CCONJ', 'PRON', 'AUX', 'VERB', 'DET', 'NOUN', 'ADP', 'NOUN', 'PUNCT']
        spaces = [True, True, True, True, False, True, True, True, True, True, True, True, True, True, True, True, True, False, True, True, True, True, True, True, False, False, True, False, True, True, False, False, True, True, True, True, True, True, False, True, True, False, True, True, True, True, True, True, True, True, True, False, False]
        morphs = ['Definite=Def|Number=Plur|PronType=Art', 'Gender=Masc|Number=Plur', 'Definite=Def|Number=Plur|PronType=Art', 'Gender=Masc|Number=Plur', 'Gender=Fem|Number=Sing', '', '', 'Number=Plur|PronType=Dem', 'Gender=Masc|NumType=Ord|Number=Plur', 'Number=Plur|Person=3', 'Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin', 'Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part', 'Gender=Fem|Number=Sing|Poss=Yes', 'Gender=Fem|Number=Sing', '', 'Number=Sing|Person=1', 'Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin', '', 'Gender=Masc|Number=Sing|Person=3', 'Polarity=Neg', 'Gender=Masc|Number=Plur|Person=3', 'Mood=Ind|Number=Sing|Person=3|Tense=Fut|VerbForm=Fin', 'Polarity=Neg', '', 'Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin', '', 'Gender=Masc|Number=Sing|Person=3', 'Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part', '', '', 'Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin', '', 'Gender=Masc|Number=Sing|Person=3', 'Gender=Fem|Number=Sing', '', 'Number=Sing|Person=1', 'Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin', '', '', '', 'Gender=Masc|Number=Plur|Person=3', 'Number=Sing|Person=3', 'Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin', 'Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part', '', 'Number=Sing|Person=3', 'Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin', 'Gender=Masc|Tense=Past|VerbForm=Part', 'Definite=Ind|Gender=Masc|Number=Sing|PronType=Art', 'Gender=Masc|Number=Sing', 'Definite=Def|Gender=Masc|Number=Sing|PronType=Art', 'Gender=Masc|Number=Sing', '']
        doc = Doc(self.nlp_trf.vocab,
                  words=words,
                  heads=heads,
                  deps=deps,
                  lemmas=lemmas,
                  pos=pos,
                  spaces=spaces,
                  morphs=morphs
                  )
        doc.user_data["coref_chains"] = [
            [[0,1], [7,8], [20,20], [40, 40]], # les voisins
            [[2,3], [9,9]],  # les inconnus
            [[4,4], [12,12], [18,18], [26, 26], [32,32], [41,41], [45,45]]  # Jean
        ]
        doc._.summary = list(doc.sents)[2:]
        resolver = CorefResolver(doc=doc, target_is_summary=True, resolution_window=-1, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        print("ORIGINAL DOC :\n")
        print(doc)
        print("\n\nCOREFED DOC:\n")
        print("\n")
        print(' '.join(tok.text for tok in corefed_doc))


    def test_simple(self):
        doc = self.nlp_trf("Les voisins, Kevin. Ils l'ont humilié")
        doc.user_data["coref_chains"] = [
            [[0,1], [5,5]],  # Les voisins
            [[3,3], [6,6]]# Kevin
        ]
        expected = ["Les", "voisins", "ont", "humilié", "Kevin"]
        doc._.summary = [list(doc.sents)[-1]]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=-1, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for token in corefed_doc])

    def test_simple_non_start(self):
        doc = self.nlp_trf("Les voisins, Kevin. Bonjour, ils l'ont humilié")
        doc.user_data["coref_chains"] = [
            [[0,1], [7,7]],  # Les voisins
            [[3,3], [8,8]]  # Kevin
        ]
        doc._.summary = [list(doc.sents)[-1]]
        expected = ["Bonjour", ",", "les", "voisins", "ont", "humilié", "Kevin"]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=-1, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [token.text for token in corefed_doc])

    def test_simple_non_start_1rule(self):
        doc = self.nlp_trf("Les voisins, Kevin. Bonjour, ils l'ont humilié")
        doc.user_data["coref_chains"] = [
            [[0,1], [7,7]],  # Les voisins
            [[3,3]]  # Kevin
        ]
        expected = ["Bonjour", ",", "les", "voisins", "l'", "ont", "humilié"]
        doc._.summary = [list(doc.sents)[-1]]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=-1, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        self.assertEqual(expected, [tok.text for tok in corefed_doc])


    def test_chains_alignment(self):
        words = ['les', 'voisins', 'les', 'inconnus', 'Jean', '.', '«', 'Ces', 'derniers', 'leur', 'ont', 'vendu', 'sa', 'nintendo', 'et', 'je', 'pense', "qu'", 'il', 'ne', 'les', 'pardonera', 'pas', '»', 'a', '-t', '-il', 'ajouté', '.', 'Mais', 'a', '-t', '-il', 'raison', '?', 'Je', 'pense', 'que', 'oui', ',', 'ils', "l'", 'ont', 'humilié', 'et', 'lui', 'ont', 'mis', 'un', 'coup', 'au', 'moral', '.']
        heads = [1, 1, 3, 1, 1, 5, 11, 8, 11, 11, 11, 11, 13, 11, 16, 16, 11, 21, 21, 21, 21, 16, 21, 11, 27, 26, 27, 27, 27, 30, 30, 32, 30, 30, 30, 36, 36, 43, 43, 43, 43, 43, 43, 36, 47, 47, 47, 43, 49, 47, 51, 47, 36]
        deps = ['det', 'ROOT', 'det', 'dep', 'nmod', 'ROOT', 'punct', 'det', 'nsubj', 'iobj', 'aux:tense', 'ROOT', 'det', 'obj', 'cc', 'nsubj', 'conj', 'mark', 'nsubj', 'advmod', 'obj', 'ccomp', 'advmod', 'punct', 'aux:tense', 'dep', 'nsubj', 'ROOT', 'punct', 'cc', 'ROOT', 'dep', 'nsubj', 'dep', 'punct', 'nsubj', 'ROOT', 'mark', 'advmod', 'punct', 'nsubj', 'obj', 'aux:tense', 'ccomp', 'cc', 'iobj', 'aux:tense', 'conj', 'det', 'obj', 'case', 'obl:arg', 'punct']
        lemmas = ['le', 'voisin', 'le', 'inconnu', 'Jean', '.', '«', 'ce', 'dernier', 'leur', 'avoir', 'vendre', 'son', 'nintendo', 'et', 'je', 'pense', 'que', 'il', 'ne', 'le', 'pardoner', 'pas', '»', 'avoir', '-t', 'il', 'ajouter', '.', 'mais', 'avoir', '-t', 'il', 'raison', '?', 'je', 'pense', 'que', 'oui', ',', 'il', "l'", 'avoir', 'humilier', 'et', 'lui', 'avoir', 'mettre', 'un', 'coup', 'au', 'moral', '.']
        pos = ['DET', 'NOUN', 'DET', 'ADJ', 'PROPN', 'PUNCT', 'PUNCT', 'DET', 'ADJ', 'PRON', 'AUX', 'VERB', 'DET', 'NOUN', 'CCONJ', 'PRON', 'VERB', 'SCONJ', 'PRON', 'ADV', 'PRON', 'VERB', 'ADV', 'PUNCT', 'AUX', 'PART', 'PRON', 'VERB', 'PUNCT', 'CCONJ', 'VERB', 'PART', 'PRON', 'NOUN', 'PUNCT', 'PRON', 'VERB', 'SCONJ', 'ADV', 'PUNCT', 'PRON', 'PRON', 'AUX', 'VERB', 'CCONJ', 'PRON', 'AUX', 'VERB', 'DET', 'NOUN', 'ADP', 'NOUN', 'PUNCT']
        spaces = [True, True, True, True, False, True, True, True, True, True, True, True, True, True, True, True, True, False, True, True, True, True, True, True, False, False, True, False, True, True, False, False, True, True, True, True, True, True, False, True, True, False, True, True, True, True, True, True, True, True, True, False, False]
        morphs = ['Definite=Def|Number=Plur|PronType=Art', 'Gender=Masc|Number=Plur', 'Definite=Def|Number=Plur|PronType=Art', 'Gender=Masc|Number=Plur', 'Gender=Fem|Number=Sing', '', '', 'Number=Plur|PronType=Dem', 'Gender=Masc|NumType=Ord|Number=Plur', 'Number=Plur|Person=3', 'Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin', 'Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part', 'Gender=Fem|Number=Sing|Poss=Yes', 'Gender=Fem|Number=Sing', '', 'Number=Sing|Person=1', 'Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin', '', 'Gender=Masc|Number=Sing|Person=3', 'Polarity=Neg', 'Gender=Masc|Number=Plur|Person=3', 'Mood=Ind|Number=Sing|Person=3|Tense=Fut|VerbForm=Fin', 'Polarity=Neg', '', 'Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin', '', 'Gender=Masc|Number=Sing|Person=3', 'Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part', '', '', 'Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin', '', 'Gender=Masc|Number=Sing|Person=3', 'Gender=Fem|Number=Sing', '', 'Number=Sing|Person=1', 'Mood=Ind|Number=Sing|Person=1|Tense=Pres|VerbForm=Fin', '', '', '', 'Gender=Masc|Number=Plur|Person=3', 'Number=Sing|Person=3', 'Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin', 'Gender=Masc|Number=Sing|Tense=Past|VerbForm=Part', '', 'Number=Sing|Person=3', 'Mood=Ind|Number=Plur|Person=3|Tense=Pres|VerbForm=Fin', 'Gender=Masc|Tense=Past|VerbForm=Part', 'Definite=Ind|Gender=Masc|Number=Sing|PronType=Art', 'Gender=Masc|Number=Sing', 'Definite=Def|Gender=Masc|Number=Sing|PronType=Art', 'Gender=Masc|Number=Sing', '']
        doc = Doc(self.nlp_trf.vocab,
                  words=words,
                  heads=heads,
                  deps=deps,
                  lemmas=lemmas,
                  pos=pos,
                  spaces=spaces,
                  morphs=morphs
                  )
        doc.user_data["coref_chains"] = [
            [[0,1], [7,8], [20,20], [40, 40]], # les voisins
            [[2,3], [9,9]],  # les inconnus
            [[4,4], [12,12], [18,18], [26, 26], [32,32], [41,41], [45,45]]  # Jean
        ]
        doc._.summary = list(doc.sents)[2:]
        resolver = CorefResolver(doc, target_is_summary=True, resolution_window=-1, replace_first_mention=False)
        corefed_doc = resolver.resolve_coreference()
        print("This is the original chains")
        for chain in doc._.get_coref_chains():
            print(chain)
        print("This is the chain remaining in the target document")
        for chain in corefed_doc._.get_coref_chains():
            print(chain)
        print("Here comes the output document")
        print(" ".join(tok.text for tok in corefed_doc))


if __name__ == '__main__':
    unittest.main()
